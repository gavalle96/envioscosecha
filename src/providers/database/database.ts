import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


import { SQLiteObject, SQLite } from '@ionic-native/sqlite';
import { Storage } from '@ionic/storage';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {
  DATA_VERSION:number = 7; //Increment this number whit each version that you alter your db structure 
  public db: SQLiteObject = null;
  constructor(public http: HttpClient,
    private storage: Storage
    , private sqlite: SQLite) {
    console.log('Hello DatabaseProvider Provider');
    this.formatDate();
  }

  setDatabase(db: SQLiteObject) {
    if (this.db === null) {
        this.db = db;
    }
}
setDB(){
  return this.storage.get('db').then((val) => {
    console.log(val);

    return this.sqlite.create({
      name: val+'.db',//'dbcatalogos.db',
      location: 'default'
    });
  }).then((db: SQLiteObject) => {
        this.setDatabase(db);
  });
}

createTables() {
  /**
   * Tablas de Catalogo de Chequero
   */
  this.db.executeSql("CREATE TABLE IF NOT EXISTS version(idversion INTEGER)", []);
  this.db.executeSql("CREATE TABLE IF NOT EXISTS haciendas(idhacienda INTEGER, bkhacienda TEXT, dshacienda TEXT, proveedor TEXT)", []);
  this.db.executeSql("CREATE TABLE IF NOT EXISTS lotes(idlote INTEGER, idhacienda INTEGER, bklote TEXT, dslote TEXT, poligono TEXT)", []);
  this.db.executeSql("CREATE TABLE IF NOT EXISTS trabajadores(idtrabajador INTEGER, codtrabajador TEXT, nomtrabajador TEXT,nombre TEXT, profesion TEXT, generico INTEGER)", []);
  this.db.executeSql("CREATE TABLE IF NOT EXISTS liberaciones(codliberacion TEXT, idlote INTEGER, idhacienda INTEGER, idbloque INTEGER, bloque TEXT, fechahora TEXT, manual INTEGER, idordencosecha INTEGER, idzafra INTEGER)", []);
  this.db.executeSql("CREATE TABLE IF NOT EXISTS equipos(idequipo INTEGER, codequipo TEXT, nombreequipo TEXT, flota INTEGER, tipo INTEGER)", []);
  this.db.executeSql("CREATE TABLE IF NOT EXISTS bloques( idbloque  INTEGER,codbloque TEXT, dsbloque TEXT)", []);
  this.db.executeSql("CREATE TABLE IF NOT EXISTS envios"+


  "(codliberacion TEXT, idlote INTEGER, idbloque INTEGER, idcamion INTEGER, idconductor INTEGER"+
  ",idcosechadora INTEGER,idchofer INTEGER,idchequero INTEGER,idsupalce INTEGER"+
  ",ayud1 INTEGER, ayud2 INTEGER,ayud3 INTEGER, ayud4 INTEGER, ayud5 INTEGER, ayud6 INTEGER"+
  ",acom1 INTEGER, acom2 INTEGER"+//",acom3 INTEGER"+
  ",desp1 INTEGER, desp2 INTEGER, desp3 INTEGER, desp4 INTEGER, desp5 INTEGER"+
  ",envio TEXT,fhcorte TEXT,tractor1 INTEGER,tractor2 INTEGER,tractor3 INTEGER"+
  ", tractorista1 INTEGER,tractorista2 INTEGER,tractorista3 INTEGER,fechaini TEXT,fechafin TEXT, lat TEXT, long TEXT, barrida INTEGER, enviado INTEGER"+
  ", idgondola INTEGER, tipoenvio TEXT, idordencosecha INTEGER, escaneado INTEGER DEFAULT 0, enviorel TEXT)", []);
  this.db.executeSql("CREATE TABLE IF NOT EXISTS rozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, envio TEXT, idtrabajador INTEGER, unadas INTEGER)", []);
  return this.db.executeSql("CREATE TABLE IF NOT EXISTS temprozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, idtrabajador INTEGER, unadas INTEGER)", []); // tabla temporal de rozadores en pantalla. Se borra al cambiar aplicativo o al guardar envìo manual

}



onStart() {
  let storedversion = this.DATA_VERSION; //Inicializa con el valor de la nueva versión
  this.recuperarCatalogosPorNombreTabla('version').then(data => 
    {
      console.log("Datos de la version ");
      console.log(data);
      
      if(data.length > 0){ 
        console.log("Version de la base ",data[0]['idversion']);//Si existe el registro de versión, obtiene el valor
        storedversion = data[0]['idversion'];
        return Promise.resolve();
      }
      else{ //Si el registro no existe, se inserta (es la primera instalación)
        return this.InsertaVersion();
      }
    }).
    then(() => {
      if(this.DATA_VERSION > storedversion  ){/*version number that is stored in the db*/
        this.migrate( storedversion/*version number that is stored in the db*/).then( _ => {
          this.db.executeSql("UPDATE version SET idversion = ?", [this.DATA_VERSION]);/*set the version number that is stored in the db to DATA_VERSION*/
         /* start the app like normal */
         console.log("update de version realizada")
        })
        .catch(err =>{
          console.log(err);
        });
       } else {/* start the app like normal */
        console.log("stored version: "+storedversion)
        console.log("nueva version:"+this.DATA_VERSION);
        console.log("Necesita actualizar ? ",this.DATA_VERSION > storedversion);
        console.log("inicia la aplicacion normalmente");
        this.eliminarHistoria();
      }
    });
    
    // console.log(this.db);

}

migrate (oldversion:number) {
  // return this.db.executeSql("UPDATE version SET idversion = ?", [this.DATA_VERSION]); //ELIMINAR ESTA LINEA EN LA VERSIÓN 2
  let sql =[];
  console.log("migrando de la versión "+oldversion+" a la "+this.DATA_VERSION);
    switch (oldversion) {
      case 1:
        sql.push("ALTER TABLE trabajadores ADD COLUMN nombre TEXT;")//DBVERSION 2
        sql.push("ALTER TABLE envios ADD COLUMN ayud3 INTEGER;");//DBVERSION 3
        sql.push("ALTER TABLE envios ADD COLUMN ayud4 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN ayud5 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN ayud6 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp1 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp2 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp3 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp4 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp5 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN idgondola INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN tipoenvio TEXT;");//DBVERSION 4
        sql.push("UPDATE envios SET tipoenvio = 'mecanizada';");//DBVERSION 4
        sql.push("CREATE TABLE IF NOT EXISTS rozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, envio TEXT, idtrabajador INTEGER, unadas INTEGER)");
        sql.push("CREATE TABLE IF NOT EXISTS temprozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, idtrabajador INTEGER, unadas INTEGER)");
        this.storage.set("aplicativo","mecanizada");
        break;
      case 2:
        sql.push("ALTER TABLE envios ADD COLUMN ayud3 INTEGER;");//DBVERSION 3
        sql.push("ALTER TABLE envios ADD COLUMN ayud4 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN ayud5 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN ayud6 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp1 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp2 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp3 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp4 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp5 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN idgondola INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN tipoenvio TEXT;");//DBVERSION 4
        sql.push("UPDATE envios SET tipoenvio = 'mecanizada';");//DBVERSION 4
        sql.push("CREATE TABLE IF NOT EXISTS rozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, envio TEXT, idtrabajador INTEGER, unadas INTEGER)");
        sql.push("CREATE TABLE IF NOT EXISTS temprozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, idtrabajador INTEGER, unadas INTEGER)");
        this.storage.set("aplicativo","mecanizada");
      break;
      case 3:
      sql.push("ALTER TABLE envios ADD COLUMN ayud4 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN ayud5 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN ayud6 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp1 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp2 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp3 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp4 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp5 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN idgondola INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN tipoenvio TEXT;");//DBVERSION 4
      sql.push("UPDATE envios SET tipoenvio = 'mecanizada';");//DBVERSION 4
      sql.push("CREATE TABLE IF NOT EXISTS rozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, envio TEXT, idtrabajador INTEGER, unadas INTEGER)");
      sql.push("CREATE TABLE IF NOT EXISTS temprozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, idtrabajador INTEGER, unadas INTEGER)");
      this.storage.set("aplicativo","mecanizada");
      break;
      case 4:
      sql.push("ALTER TABLE envios ADD COLUMN ayud4 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN ayud5 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN ayud6 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp1 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp2 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp3 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp4 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp5 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN idgondola INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN tipoenvio TEXT;");//DBVERSION 4
      sql.push("UPDATE envios SET tipoenvio = 'mecanizada';");//DBVERSION 4
      sql.push("CREATE TABLE IF NOT EXISTS rozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, envio TEXT, idtrabajador INTEGER, unadas INTEGER)");
      sql.push("CREATE TABLE IF NOT EXISTS temprozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, idtrabajador INTEGER, unadas INTEGER)");
      this.storage.set("aplicativo","mecanizada");
      sql.push("ALTER TABLE envios ADD COLUMN idordencosecha INTEGER;");//DBVERSION 5
      sql.push("ALTER TABLE liberaciones ADD COLUMN idordencosecha INTEGER;");//DBVERSION 5
      break;
      case 5:
      sql.push("ALTER TABLE envios ADD COLUMN ayud4 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN ayud5 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN ayud6 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp1 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp2 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp3 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp4 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN desp5 INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN idgondola INTEGER;");//DBVERSION 4
      sql.push("ALTER TABLE envios ADD COLUMN tipoenvio TEXT;");//DBVERSION 4
      sql.push("UPDATE envios SET tipoenvio = 'mecanizada';");//DBVERSION 4
      sql.push("CREATE TABLE IF NOT EXISTS rozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, envio TEXT, idtrabajador INTEGER, unadas INTEGER)");
      sql.push("CREATE TABLE IF NOT EXISTS temprozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, idtrabajador INTEGER, unadas INTEGER)");
      this.storage.set("aplicativo","mecanizada");
      sql.push("ALTER TABLE envios ADD COLUMN idordencosecha INTEGER;");//DBVERSION 5
      sql.push("ALTER TABLE liberaciones ADD COLUMN idordencosecha INTEGER;");//DBVERSION 5
      sql.push("ALTER TABLE envios ADD COLUMN    INTEGER DEFAULT 0;");//DBVERSION 6
      break;
      case 6:
        sql.push("ALTER TABLE envios ADD COLUMN ayud4 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN ayud5 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN ayud6 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp1 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp2 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp3 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp4 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN desp5 INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN idgondola INTEGER;");//DBVERSION 4
        sql.push("ALTER TABLE envios ADD COLUMN tipoenvio TEXT;");//DBVERSION 4
        sql.push("UPDATE envios SET tipoenvio = 'mecanizada';");//DBVERSION 4
        sql.push("CREATE TABLE IF NOT EXISTS rozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, envio TEXT, idtrabajador INTEGER, unadas INTEGER)");
        sql.push("CREATE TABLE IF NOT EXISTS temprozadores( id INTEGER PRIMARY KEY AUTOINCREMENT, idtrabajador INTEGER, unadas INTEGER)");
        this.storage.set("aplicativo","mecanizada");
        sql.push("ALTER TABLE envios ADD COLUMN idordencosecha INTEGER;");//DBVERSION 5
        sql.push("ALTER TABLE liberaciones ADD COLUMN idordencosecha INTEGER;");//DBVERSION 5
        sql.push("ALTER TABLE envios ADD COLUMN escaneado INTEGER DEFAULT 0;");//DBVERSION 6
        sql.push("ALTER TABLE liberaciones ADD COLUMN idzafra INTEGER;");//DBVERSION 7
        sql.push("ALTER TABLE envios ADD COLUMN enviorel TEXT;");//DBVERSION 7
        break;
      default:
      //ayud2 INTEGER,
        break;
    }
    console.log(sql);
    sql.forEach(element => {
      this.db.executeSql(element,[]);
    });
    return Promise.resolve();
    
 //Migrate the data, oldversion tells you from which version.
 /*
   Con cada versión que se aumente, deben acumularse los cambios
   ejemplo
   DATA_VERSION = 2 //nueva versión
    switch(oldversion)
      case 1:
        ***CAMBIOS DE LA VERSIÓN 2***
        break;

    DATA_VERSION = 3 //nueva versión
    switch(oldversion)
      case 1:
        ***CAMBIOS DE LA VERSIÓN 2***
        ***CAMBIOS DE LA VERSIÓN 3***
        break;
      case 2:
        ***CAMBIOS DE LA VERSIÓN 3***
        break;
 */
}

 /**
     * Funcion para Recuperar Catalogos por Nombre de Tabla
     * @param nombreTabla String ('tiposparo')
     */
    recuperarCatalogosPorNombreTabla(nombreTabla: string) {
      let sql = 'SELECT * FROM ' + nombreTabla + '';
      switch (nombreTabla) {
        case 'haciendas':
          sql += ' order by bkhacienda ';
          break;
              
        default:
          break;
      }
      return this.db.executeSql(sql, [])
          .then(response => {
              let filas = [];
              for (let index = 0; index < response.rows.length; index++) {
                  filas.push(response.rows.item(index));
              }
              return Promise.resolve(filas);
          })
          .catch(error => Promise.reject(error));
  }

  getLotesByHda(IdHacienda: number) {
    let sql = 'SELECT * FROM lotes where idhacienda = '+IdHacienda+' order by bklote';
    return this.db.executeSql(sql, [])
        .then(response => {
            let filas = [];
            for (let index = 0; index < response.rows.length; index++) {
                filas.push(response.rows.item(index));
            }
            return Promise.resolve(filas);
        })
        .catch(error => Promise.reject(error));
}

getCatalogo(tabla: string) {
  let sql = 'SELECT * FROM '+tabla;
  return this.db.executeSql(sql, [])
      .then(response => {
          let filas = [];
          for (let index = 0; index < response.rows.length; index++) {
              filas.push(response.rows.item(index));
          }
          return Promise.resolve(filas);
      })
      .catch(error => Promise.reject(error));
}

getHaciendasConLiberaciones() {
  let manual = "mecanizada";
  return this.storage.get("aplicativo").then(val => {
    if(val != null && val != undefined){
      manual =val;
    }
  let sql = 'SELECT h.idhacienda, h.bkhacienda, h.dshacienda FROM haciendas h '+
  'INNER JOIN lotes l on h.idhacienda = l.idhacienda '+
  'INNER JOIN liberaciones lib on l.idlote = lib.idlote and lib.manual = '+ (manual == "mecanizada" ? 0 : (manual=="coop" ? 'lib.manual' :1))+
  ' GROUP BY h.idhacienda, h.bkhacienda, h.dshacienda';
  return this.db.executeSql(sql, [])
  })
      .then(response => {
          let filas = [];
           console.log("liberaciones-haciendas",response);
          for (let index = 0; index < response.rows.length; index++) {
              filas.push(response.rows.item(index));
          }
          return Promise.resolve(filas);
      })
      .catch(error => Promise.reject(error));
}

getLotesConLiberaciones(idhacienda: number = 0) {

  let manual = "mecanizada";
  return this.storage.get("aplicativo").then(val => {
    if(val != null && val != undefined){
      manual =val;
    }

  let sql = 'SELECT l.idlote, l.bklote, l.dslote, l.idhacienda FROM haciendas h '+
  'INNER JOIN lotes l on h.idhacienda = l.idhacienda ';
  if(idhacienda >0){
    sql += ' and h.idhacienda = '+ idhacienda+' ';
  }

  sql += 'INNER JOIN liberaciones lib on l.idlote = lib.idlote and lib.manual= '+ (manual == "mecanizada" ? 0 : (manual=="coop" ? 'lib.manual' :1))+
  
  ' GROUP BY l.idlote, l.bklote, l.dslote, l.idhacienda '+
  'ORDER BY l.bklote';
  return this.db.executeSql(sql, [])
})
      .then(response => {
          let filas = [];
          for (let index = 0; index < response.rows.length; index++) {
              filas.push(response.rows.item(index));
          }
          // console.log(filas);
          return Promise.resolve(filas);
      })
      .catch(error => Promise.reject(error));
}

getSingleTrabajadorCodigo(codtrabajador: string){
  let filtro = [{campo: "codtrabajador", valor:codtrabajador}]
  this.getCatalogoFiltrado("trabajadores",filtro).then(data => {
    if(data.length > 0){
      return data[0];
    }
  }).catch(err => {
    console.log(err);
    return {};
  });
}

getSingleTrabajadorId(idtrabajador: number){
  let filtro = [{campo: "idtrabajador", valor:idtrabajador}]
  this.getCatalogoFiltrado("trabajadores",filtro).then(data => {
    if(data.length > 0){
      return data[0];
    }
  }).catch(err => {
    console.log(err);
    return {};
  });
}

getEnvios(tipo="mecanizada"){
  let sql = "SELECT distinct e.envio, e.fhcorte, e.codliberacion"+
  ", h.idhacienda, h.bkhacienda, h.dshacienda, l.idlote, l.bklote, l.dslote"+
  ", e.enviado, e.fechaini, e.fechafin, e.lat, e.long, e.barrida, e.tipoenvio"+
  //TRIPULACION
  ", e.idbloque, b.codbloque, b.dsbloque" +
  ", e.idchequero, ch.codtrabajador codchequero, ch.nomtrabajador chequero"+
  ", e.idchofer, oc.codtrabajador codopcos, oc.nomtrabajador opcos"+
  ", e.tractorista1 idtractorista1, t1.codtrabajador codtractorista1, t1.nomtrabajador tractorista1, e.tractorista2 idtractorista2, t2.codtrabajador codtractorista2, t2.nomtrabajador tractorista2, e.tractorista3 idtractorista3, t3.codtrabajador codtractorista3, t3.nomtrabajador tractorista3"+
  ", e.idsupalce, sa.codtrabajador codsupalce, sa.nomtrabajador supalce"+
  ", e.ayud1, ay1.codtrabajador codayudante1, ay1.nomtrabajador ayudante1, e.ayud2, ay2.codtrabajador codayudante2, ay2.nomtrabajador ayudante2, e.ayud3, ay3.codtrabajador codayudante3, ay3.nomtrabajador ayudante3"+
  ", e.ayud4, ay4.codtrabajador codayudante4, ay4.nomtrabajador ayudante4, e.ayud5, ay5.codtrabajador codayudante5, ay5.nomtrabajador ayudante5,e.ayud6, ay6.codtrabajador codayudante6, ay6.nomtrabajador ayudante6"+
  ", e.desp1 , desp1.codtrabajador coddespuntador1, desp1.nomtrabajador despuntador1, e.desp2, desp2.codtrabajador coddespuntador2, desp2.nomtrabajador despuntador2"+
  ", e.desp3, desp3.codtrabajador coddespuntador3, desp3.nomtrabajador despuntador3"+
  ", e.desp4, desp4.codtrabajador coddespuntador4, desp4.nomtrabajador despuntador4"+
  ", e.desp5, desp5.codtrabajador coddespuntador5, desp5.nomtrabajador despuntador5"+
  ", e.acom1, ac1.codtrabajador codacomodador1, ac1.nomtrabajador acomodador1, e.acom2, ac2.codtrabajador codacomodador2, ac2.nomtrabajador acomodador2"+//", ac3.nomtrabajador acomodador3"+
  //MAQUINARIA
  ", co.codequipo codcosechadora, co.nombreequipo cosechadora"+
  ", tr1.codequipo codtractor1, tr1.nombreequipo tractor1, tr2.codequipo codtractor2, tr2.nombreequipo tractor2, tr3.codequipo codtractor3, tr3.nombreequipo tractor3"+
  ", v.codequipo codvehiculo, v.nombreequipo vehiculo, cnd.codtrabajador codconductor, cnd.nomtrabajador conductor, e.idgondola, g.codequipo codgondola, g.nombreequipo gondola"+
  " FROM envios e"+
  " INNER JOIN lotes l on e.idlote = l.idlote"+
  " INNER JOIN haciendas h on l.idhacienda = h.idhacienda"+
  " LEFT JOIN trabajadores ch on e.idchequero = ch.idtrabajador"+
  " LEFT JOIN trabajadores oc on e.idchofer = oc.idtrabajador"+
  " LEFT JOIN trabajadores t1 on e.tractorista1 = t1.idtrabajador"+
  " LEFT JOIN trabajadores t2 on e.tractorista2 = t2.idtrabajador"+
  " LEFT JOIN trabajadores t3 on e.tractorista3 = t3.idtrabajador"+
  " LEFT JOIN trabajadores sa on e.idsupalce= sa.idtrabajador"+
  " LEFT JOIN trabajadores ay1 on e.ayud1 = ay1.idtrabajador"+
  " LEFT JOIN trabajadores ay2 on e.ayud2 = ay2.idtrabajador"+
  " LEFT JOIN trabajadores ay3 on e.ayud3 = ay3.idtrabajador"+
  " LEFT JOIN trabajadores ay4 on e.ayud4 = ay4.idtrabajador"+
  " LEFT JOIN trabajadores ay5 on e.ayud5 = ay5.idtrabajador"+
  " LEFT JOIN trabajadores ay6 on e.ayud6 = ay6.idtrabajador"+

  " LEFT JOIN trabajadores desp1 on e.desp1 = desp1.idtrabajador"+
  " LEFT JOIN trabajadores desp2 on e.desp2 = desp2.idtrabajador"+
  " LEFT JOIN trabajadores desp3 on e.desp3 = desp3.idtrabajador"+
  " LEFT JOIN trabajadores desp4 on e.desp4 = desp4.idtrabajador"+
  " LEFT JOIN trabajadores desp5 on e.desp5 = desp5.idtrabajador"+
  
  " LEFT JOIN trabajadores ac1 on e.acom1 = ac1.idtrabajador"+
  " LEFT JOIN trabajadores ac2 on e.acom2 = ac2.idtrabajador"+
 // " LEFT JOIN trabajadores ac3 on e.acom3 = ac3.idtrabajador"+
  " LEFT JOIN equipos co on e.idcosechadora = co.idequipo"+
  " LEFT JOIN bloques b on e.idbloque = b.idbloque" +
  " LEFT JOIN equipos tr1 on e.tractor1 = tr1.idequipo"+
  " LEFT JOIN equipos tr2 on e.tractor2 = tr2.idequipo"+
  " LEFT JOIN equipos tr3 on e.tractor3 = tr3.idequipo"+
  " LEFT JOIN equipos v on e.idcamion = v.idequipo"+
  " LEFT JOIN trabajadores cnd on e.idconductor = cnd.idtrabajador"+
  " LEFT JOIN equipos g on e.idgondola = g.idequipo"+
  " where e.tipoenvio = '"+tipo+"'"+
  " ORDER BY e.fechafin DESC"
  ;
//  console.log(sql);

  return this.db.executeSql(sql, [])
      .then(response => {
          let filas = [];
          for (let index = 0; index < response.rows.length; index++) {
              filas.push(response.rows.item(index));
          }
          // console.log(filas);
          return Promise.resolve(filas);
      })
      .catch(error => Promise.reject(error));
}

marcarEnviados(envios){
  let sql = "UPDATE envios SET enviado = 1 where envio in ('',";
  if(envios != null){
    if(envios.length > 0){
      envios.forEach(item => {
        sql+= "'"+item.envio+"',";
      });
      sql = sql.slice(0, (sql.length-1));
    }
  }
  sql += ");"
  this.db.executeSql(sql,[]);
}
  getEnviosNoEnviados(){
  let sql = "SELECT distinct ROW_NUMBER() OVER(ORDER BY e.envio) AS Id, e.envio, e.fhcorte, e.codliberacion, lib.idordencosecha, lib.idzafra"+
  ", h.idhacienda, h.bkhacienda, h.dshacienda, l.idlote, l.bklote, l.dslote"+
  ", e.enviado, e.fechaini, e.fechafin, e.lat, e.long, e.barrida, e.tipoenvio, e.escaneado, e.enviorel"+
  //TRIPULACION
  ", e.idbloque, b.codbloque, b.dsbloque" +
  ", ch.codtrabajador codchequero, ch.nomtrabajador chequero"+
  ", oc.codtrabajador codopcos, oc.nomtrabajador opcos"+
  ", t1.codtrabajador codtractorista1, t1.nomtrabajador tractorista1, t2.codtrabajador codtractorista2, t2.nomtrabajador tractorista2, t3.codtrabajador codtractorista3, t3.nomtrabajador tractorista3"+
  ", sa.codtrabajador codsupalce, sa.nomtrabajador supalce"+
  ", ay1.codtrabajador codayudante1, ay1.nomtrabajador ayudante1, ay2.codtrabajador codayudante2, ay2.nomtrabajador ayudante2, ay3.codtrabajador codayudante3, ay3.nomtrabajador ayudante3"+
  ", ay4.codtrabajador codayudante4, ay4.nomtrabajador ayudante4, ay5.codtrabajador codayudante5, ay5.nomtrabajador ayudante5, ay6.codtrabajador codayudante6, ay6.nomtrabajador ayudante6"+
  ", desp1.codtrabajador coddespuntador1, desp1.nomtrabajador despuntador1, desp2.codtrabajador coddespuntador2, desp2.nomtrabajador despuntador2"+
  ", desp3.codtrabajador coddespuntador3, desp3.nomtrabajador despuntador3"+
  ", desp4.codtrabajador coddespuntador4, desp4.nomtrabajador despuntador4"+
  ", desp5.codtrabajador coddespuntador5, desp5.nomtrabajador despuntador5"+
  ", ac1.codtrabajador codacomodador1, ac1.nomtrabajador acomodador1, ac2.codtrabajador codacomodador2, ac2.nomtrabajador acomodador2"+//", ac3.nomtrabajador acomodador3"+
  //MAQUINARIA
  ", co.codequipo codcosechadora, co.nombreequipo cosechadora"+
  ", tr1.codequipo codtractor1, tr1.nombreequipo tractor1, tr2.codequipo codtractor2, tr2.nombreequipo tractor2, tr3.codequipo codtractor3, tr3.nombreequipo tractor3"+
  ", v.codequipo codvehiculo, v.nombreequipo vehiculo, cnd.codtrabajador codconductor, cnd.nomtrabajador conductor, e.idgondola, g.codequipo codgondola, g.nombreequipo gondola"+
  " FROM envios e"+
  " INNER JOIN lotes l on e.idlote = l.idlote"+
  " INNER JOIN haciendas h on l.idhacienda = h.idhacienda"+
  " INNER JOIN liberaciones lib ON e.codliberacion = lib.codliberacion"+
  " LEFT JOIN trabajadores ch on e.idchequero = ch.idtrabajador"+
  " LEFT JOIN trabajadores oc on e.idchofer = oc.idtrabajador"+
  " LEFT JOIN trabajadores t1 on e.tractorista1 = t1.idtrabajador"+
  " LEFT JOIN trabajadores t2 on e.tractorista2 = t2.idtrabajador"+
  " LEFT JOIN trabajadores t3 on e.tractorista3 = t3.idtrabajador"+
  " LEFT JOIN trabajadores sa on e.idsupalce= sa.idtrabajador"+
  " LEFT JOIN trabajadores ay1 on e.ayud1 = ay1.idtrabajador"+
  " LEFT JOIN trabajadores ay2 on e.ayud2 = ay2.idtrabajador"+
  " LEFT JOIN trabajadores ay3 on e.ayud3 = ay3.idtrabajador"+
  " LEFT JOIN trabajadores ay4 on e.ayud4 = ay4.idtrabajador"+
  " LEFT JOIN trabajadores ay5 on e.ayud5 = ay5.idtrabajador"+
  " LEFT JOIN trabajadores ay6 on e.ayud6 = ay6.idtrabajador"+

  " LEFT JOIN trabajadores desp1 on e.desp1 = desp1.idtrabajador"+
  " LEFT JOIN trabajadores desp2 on e.desp2 = desp2.idtrabajador"+
  " LEFT JOIN trabajadores desp3 on e.desp3 = desp3.idtrabajador"+
  " LEFT JOIN trabajadores desp4 on e.desp4 = desp4.idtrabajador"+
  " LEFT JOIN trabajadores desp5 on e.desp5 = desp5.idtrabajador"+
  
  " LEFT JOIN trabajadores ac1 on e.acom1 = ac1.idtrabajador"+
  " LEFT JOIN trabajadores ac2 on e.acom2 = ac2.idtrabajador"+
 // " LEFT JOIN trabajadores ac3 on e.acom3 = ac3.idtrabajador"+
  " LEFT JOIN equipos co on e.idcosechadora = co.idequipo"+
  " LEFT JOIN bloques b on e.idbloque = b.idbloque" +
  " LEFT JOIN equipos tr1 on e.tractor1 = tr1.idequipo"+
  " LEFT JOIN equipos tr2 on e.tractor2 = tr2.idequipo"+
  " LEFT JOIN equipos tr3 on e.tractor3 = tr3.idequipo"+
  " LEFT JOIN equipos v on e.idcamion = v.idequipo"+
  " LEFT JOIN trabajadores cnd on e.idconductor = cnd.idtrabajador"+
  " LEFT JOIN equipos g on e.idgondola = g.idequipo"+
  " WHERE e.enviado = 0"
  " ORDER BY e.fechafin DESC"
  ;
  // console.log(sql);

  return this.db.executeSql(sql, [])
      .then(response => {
          let filas = [];
          for (let index = 0; index < response.rows.length; index++) {
              filas.push(response.rows.item(index));
          }
          // console.log(filas);
          return Promise.resolve(filas);
      })
      .catch(error => Promise.reject(error));
}

getSingleEnvio(numEnvio){
  let sql = "SELECT e.envio, e.fhcorte, e.codliberacion"+
  ", h.idhacienda, h.bkhacienda, h.dshacienda, l.idlote, l.bklote, l.dslote"+
  ", e.enviado, e.fechaini, e.fechafin, e.lat, e.long, e.barrida, e.tipoenvio"+
  //TRIPULACION
  ", e.idbloque, b.codbloque, b.dsbloque" +
  ", ch.codtrabajador codchequero, ch.nomtrabajador chequero"+
  ", oc.codtrabajador codopcos, oc.nomtrabajador opcos"+
  ", t1.codtrabajador codtractorista1, t1.nomtrabajador tractorista1, t2.codtrabajador codtractorista2, t2.nomtrabajador tractorista2, t3.codtrabajador codtractorista3, t3.nomtrabajador tractorista3"+
  ", sa.codtrabajador codsupalce, sa.nomtrabajador supalce"+
  ", ay1.codtrabajador codayudante1, ay1.nomtrabajador ayudante1, ay2.codtrabajador codayudante2, ay2.nomtrabajador ayudante2, ay3.codtrabajador codayudante3, ay3.nomtrabajador ayudante3"+
  ", ay4.codtrabajador codayudante4, ay4.nomtrabajador ayudante4, ay5.codtrabajador codayudante5, ay5.nomtrabajador ayudante5, ay6.codtrabajador codayudante6, ay6.nomtrabajador ayudante6"+
  ", desp1.codtrabajador coddespuntador1, desp1.nomtrabajador despuntador1, desp2.codtrabajador coddespuntador2, desp2.nomtrabajador despuntador2"+
  ", desp3.codtrabajador coddespuntador3, desp3.nomtrabajador despuntador3"+
  ", desp4.codtrabajador coddespuntador4, desp4.nomtrabajador despuntador4"+
  ", desp5.codtrabajador coddespuntador5, desp5.nomtrabajador despuntador5"+
  ", ac1.codtrabajador codacomodador1, ac1.nomtrabajador acomodador1, ac2.codtrabajador codacomodador2, ac2.nomtrabajador acomodador2"+//", ac3.nomtrabajador acomodador3"+
  //MAQUINARIA
  ", co.codequipo codcosechadora, co.nombreequipo cosechadora"+
  ", tr1.codequipo codtractor1, tr1.nombreequipo tractor1, tr2.codequipo codtractor2, tr2.nombreequipo tractor2, tr3.codequipo codtractor3, tr3.nombreequipo tractor3"+
  ", v.codequipo codvehiculo, v.nombreequipo vehiculo, cnd.codtrabajador codconductor, cnd.nomtrabajador conductor, e.idgondola, g.codequipo codgondola, g.nombreequipo gondola"+
  " FROM envios e"+
  " INNER JOIN lotes l on e.idlote = l.idlote"+
  " INNER JOIN haciendas h on l.idhacienda = h.idhacienda"+
  " LEFT JOIN trabajadores ch on e.idchequero = ch.idtrabajador"+
  " LEFT JOIN trabajadores oc on e.idchofer = oc.idtrabajador"+
  " LEFT JOIN trabajadores t1 on e.tractorista1 = t1.idtrabajador"+
  " LEFT JOIN trabajadores t2 on e.tractorista2 = t2.idtrabajador"+
  " LEFT JOIN trabajadores t3 on e.tractorista3 = t3.idtrabajador"+
  " LEFT JOIN trabajadores sa on e.idsupalce= sa.idtrabajador"+
  " LEFT JOIN trabajadores ay1 on e.ayud1 = ay1.idtrabajador"+
  " LEFT JOIN trabajadores ay2 on e.ayud2 = ay2.idtrabajador"+
  " LEFT JOIN trabajadores ay3 on e.ayud3 = ay3.idtrabajador"+
  " LEFT JOIN trabajadores ay4 on e.ayud4 = ay4.idtrabajador"+
  " LEFT JOIN trabajadores ay5 on e.ayud5 = ay5.idtrabajador"+
  " LEFT JOIN trabajadores ay6 on e.ayud6 = ay6.idtrabajador"+

  " LEFT JOIN trabajadores desp1 on e.desp1 = desp1.idtrabajador"+
  " LEFT JOIN trabajadores desp2 on e.desp2 = desp2.idtrabajador"+
  " LEFT JOIN trabajadores desp3 on e.desp3 = desp3.idtrabajador"+
  " LEFT JOIN trabajadores desp4 on e.desp4 = desp4.idtrabajador"+
  " LEFT JOIN trabajadores desp5 on e.desp5 = desp5.idtrabajador"+
  
  " LEFT JOIN trabajadores ac1 on e.acom1 = ac1.idtrabajador"+
  " LEFT JOIN trabajadores ac2 on e.acom2 = ac2.idtrabajador"+
 // " LEFT JOIN trabajadores ac3 on e.acom3 = ac3.idtrabajador"+
  " LEFT JOIN equipos co on e.idcosechadora = co.idequipo"+
  " LEFT JOIN bloques b on e.idbloque = b.idbloque" +
  " LEFT JOIN equipos tr1 on e.tractor1 = tr1.idequipo"+
  " LEFT JOIN equipos tr2 on e.tractor2 = tr2.idequipo"+
  " LEFT JOIN equipos tr3 on e.tractor3 = tr3.idequipo"+
  " LEFT JOIN equipos v on e.idcamion = v.idequipo"+
  " LEFT JOIN trabajadores cnd on e.idconductor = cnd.idtrabajador"+
  " LEFT JOIN equipos g on e.idgondola = g.idequipo"+
  " WHERE e.envio = '"+numEnvio+"'"+
  " ORDER BY e.fechafin DESC"
  ;
 console.log(sql);

  return this.db.executeSql(sql, [])
      .then(response => {
          let filas = [];
          
          for (let index = 0; index < response.rows.length; index++) {
              filas.push(response.rows.item(index));
          }
          console.log(filas);
          return Promise.resolve(filas);
      })
      .catch(error => Promise.reject(error));
}

getRozadoresEnvio(envio){
  let sql = "SELECT b.id, c.idtrabajador, c.codtrabajador, c.nomtrabajador, b.unadas, d.idordencosecha, d.manual"+
  " FROM envios a"+
  " INNER JOIN rozadores b ON a.envio = b.envio and a.envio = '"+envio+"'"+
  " INNER JOIN trabajadores c ON b.idtrabajador = c.idtrabajador "+
  " inner JOIN liberaciones d ON a.codliberacion = d.codliberacion"
  " ORDER BY b.id asc";
  
  return this.db.executeSql(sql, [])
      .then(response => {
          let filas = [];
          
          
          for (let index = 0; index < response.rows.length; index++) {
              filas.push(response.rows.item(index));
          }
          filas = filas.sort((a,b) => a.id - b.id);

          console.log("ROZADORES DE ENVIO");
          console.log(filas);
          return Promise.resolve(filas);
      })
      .catch(error => Promise.reject(error));
}



getRozadoresTemp(){
  let sql = "SELECT b.id, c.idtrabajador, c.nomtrabajador, b.unadas"+
  " FROM  temprozadores b "+
  " INNER JOIN trabajadores c ON b.idtrabajador = c.idtrabajador"+
  " ORDER BY b.id";

  return this.db.executeSql(sql, [])
      .then(response => {
          let filas = [];
          
          for (let index = 0; index < response.rows.length; index++) {
              filas.push(response.rows.item(index));
          }
          return Promise.resolve(filas);
      })
      .catch(error => Promise.reject(error));
}

getEquiposGondola(){
  let sql = "SELECT a.idequipo, a.codequipo, a.nombreequipo"+
  " FROM  equipos a WHERE tipo in (4)";

  return this.db.executeSql(sql, [])
      .then(response => {
          let filas = [];
          
          for (let index = 0; index < response.rows.length; index++) {
              filas.push(response.rows.item(index));
          }
          return Promise.resolve(filas);
      })
      .catch(error => Promise.reject(error));
}

getCargadorasCosechadoras(){
  let sql = "SELECT a.idequipo, a.codequipo, a.nombreequipo"+
  " FROM  equipos a WHERE tipo in (1,5)";

  return this.db.executeSql(sql, [])
      .then(response => {
          let filas = [];
          
          for (let index = 0; index < response.rows.length; index++) {
              filas.push(response.rows.item(index));
          }
          return Promise.resolve(filas);
      })
      .catch(error => Promise.reject(error));
}

getCatalogoFiltrado(tabla: string, filtro: Array<{campo: string, valor: any}> = [], orden: Array<{campo: string}> = []) {
  // console.log(this.db);
  let sql = "SELECT * FROM "+tabla+" ";

  if(filtro.length > 0){
    sql += "WHERE ";

    let cont = 0;

    filtro.forEach(item => {
      if(cont > 0){
        sql += "AND ";
      }
      if(typeof(item.valor) == "string"){
        sql +=  item.campo+" = '"+item.valor+"' ";
      }
      else if(typeof(item.valor) == "number"){
        sql += item.campo+" = "+item.valor+" ";
      }

      cont++;
    });

  }

  if(orden.length > 0){
    sql += "ORDER BY "
    let i = 0;

    orden.forEach(item => {
      if(i > 0){
        sql += ", ";
      }
      sql += item.campo;
      i++;
    });
  }

  //console.log(sql);
  return this.db.executeSql(sql, [])
      .then(response => {
          let filas = [];
          for (let index = 0; index < response.rows.length; index++) {
              filas.push(response.rows.item(index));
          }
          return Promise.resolve(filas);
      })
      .catch(error => Promise.reject(error));
}


  /**
   * Funcion para Insertar Catalogos por Transaccion
   * @param sql String [] se espera una array de sentencias SQL=>INSERT
   */
  insertarCatalogosPorTransaccion(sql:any) {
      return this.db.sqlBatch(sql);
  }


  insertRozadorTemp(obj){
    let sql = "INSERT INTO temprozadores(idtrabajador, unadas) VALUES("+obj.idtrabajador+","+obj.unadas+");"
    return this.db.executeSql(sql,[]);
  }
  prepareInsertRozador(obj){
    let sql = "INSERT INTO rozadores(envio,idtrabajador, unadas) VALUES("+obj.envio+","+obj.idtrabajador+","+obj.unadas+");"
    return sql;
  }
  insertRozador(obj){
    let sql = this.prepareInsertRozador(obj);
    return this.db.executeSql(sql,[])
    
  }

  updateRozTemp(obj){
    let sql = "UPDATE temprozadores SET unadas = "+obj.unadas+" where id = "+obj.id;
    return this.db.executeSql(sql,[])
  }

  deleteRozTemp(obj){
    let sql = "DELETE FROM temprozadores where id = "+obj.id;
    return this.db.executeSql(sql,[])
  }

  limpiarRozTemp(){
    let sql = "DELETE FROM temprozadores;";
    return this.db.executeSql(sql,[])
  }



  
  //#region LLenar tablas auxiliares

 
  

   /**
    * Recibe la entidad completa de envio que será descompuesta internamente para guardar
    * @param item Tipo Envio
    */
  EnvioCreate(item: any) {
    let sql = "INSERT INTO envios(codliberacion, idlote, idbloque, idcamion, idconductor, idcosechadora, idchofer,idchequero,idsupalce"+
    ",ayud1,ayud2, ayud3,ayud4,ayud5,ayud6,desp1,desp2,desp3,desp4,desp5,acom1,acom2"+//",acom3"+
    ",envio,fhcorte,tractor1,tractor2,tractor3"+ //fhcorte almacena fecha y hora de corte
    ", tractorista1,tractorista2,tractorista3,fechaini,fechafin, lat, long, barrida, enviado, idgondola, tipoenvio, escaneado, enviorel)"+

    " VALUES('"+item.codliberacion+"',"+item.idlote+", "+item.idbloque+"," +
    item.idcamion+", "+item.idconductor+","+ item.idcosechadora+
    ", "+item.idchofer+", "+item.idchequero+","+item.idsupalce+
    ","+item.ayud1+","+item.ayud2+","+item.ayud3+","+item.ayud4+","+item.ayud5+","+item.ayud6+","+item.desp1+","+item.desp2+","+item.desp3+","+item.desp4+","+item.desp5+","+item.acom1+","+item.acom2+//","+item.acom3+
    
    ",'"+item.envio+"','"+item.fhcorte+"',"+//item.hcorte+"',"+
    item.tractor1+","+item.tractor2+","+item.tractor3+
    ","+item.tractorista1+","+item.tractorista2+","+item.tractorista3+
    ",'"+item.fechaini+"','"+item.fechafin+"','"+item.lat+"','"+ item.long+"',"+(item.barrida == true ? 1 : 0)+",0,"+item.idgondola+",'"+item.tipoenvio+"', "+item.escaneado+",'"+item.enviorel+"'"+")";
     console.log(sql);
    return this.db.executeSql(sql, []);
}
 

  InsertaVersion(){
    return this.db.executeSql('INSERT INTO version(idversion) values(?)', [this.DATA_VERSION]);
  }




  //#endregion


  /**
   * Funcion para Limpiar Catalogos por Nombre de Tabla
   * @param nombreTabla String ('tiposparo'
   */
  limpiarCatalogosPorNombreTabla(nombreTabla) {
      let sql = 'DELETE FROM ' + nombreTabla + '';
      return this.db.executeSql(sql, []);
  }

  //MANEJO DE POLIGONOS
  
  convertirPoligono(poligono:string){
    let retorno = [];
    //quita POLYGON y los parentesis
    poligono = poligono.slice(10,(poligono.length-2));

    //quito los espacios despues de las comas

    poligono = poligono.replace(/, /g,",");

    //console.log(poligono);
    //separamos cada punto en un array
    let arrpol = poligono.split(",");
    for(let i = 0; i < arrpol.length; i++){
      //separa latitud y longitud y lo reordena
      let item =  arrpol[i];
      //console.log(item);
      let arrlatlng = item.split(" ");
      let latlng = {lat: arrlatlng[1], lng: arrlatlng[0]}; //ordena primero lat y luego long
      
      //agrega el valor al nuevo array
      retorno.push(latlng);
    }


    // console.log(poligono)
    return retorno;
  }

  puntoEnPoligono( position: {lat:number,lng:number}, idlote){
    let retorno = false;
    let verificar = false; //Verificar poligono
    
    return this.storage.get("checkpoligono")
    .then(data =>{
      if(data !== undefined && data !== null){
        verificar = data;
      }
    
    return this.getCatalogoFiltrado("lotes",[{campo:"IdLote",valor:idlote}])
      })
      .then(data => {
      
      //console.log(data);
      if(data !== undefined && data !== null){
        if(data.length > 0){

          //Solo verificar si así esta parametrizado y si el lote tiene poligono
          if(data[0].poligono == "" || !verificar){
            retorno = true;
          }
          else{
            
            retorno = this.inside([position.lng,position.lat],data[0].poligono); //Poly.containsLocation(myPosition,this.convertirPoligono(data[0].poligono));
          }


        }
      }
      return retorno;

    })
    .catch(err => {
      console.log(err);
      return false
    });
    

    
  }


  toCoordArrays(poligono: string){

    let retorno = [];

    //quita POLYGON y los parentesis
    poligono = poligono.slice(10,(poligono.length-2));
    
    //quito los espacios despues de las comas
    
    poligono = poligono.replace(/, /g,",");
    
    //separamos cada punto en un array    
    let arrpol = poligono.split(",");
    for(let i = 0; i < arrpol.length; i++){
      
      let item =  arrpol[i];      
      //separa latitud y longitud del punto evaluado
      let arrlatlng = item.split(" ");

      //se crea el array de dos valores [longitud, latitud]
      let latlng = [];
      latlng.push(parseFloat(arrlatlng[0])); 
      latlng.push(parseFloat(arrlatlng[1])); 
      
      //agrega el valor al retorno
      retorno.push(latlng);
    }
    
    return retorno;
    }
    
    inside(point, poligono:string) {
        let vs = this.toCoordArrays(poligono);
        var x = point[0], y = point[1];
    
        var inside = false;
        for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
            var xi = vs[i][0], yi = vs[i][1];
            var xj = vs[j][0], yj = vs[j][1];
    
            var intersect = ((yi > y) != (yj > y))
                && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) inside = !inside;
        }
    
        return inside;
    };

    modificarEnvio(envio){

      let sql = "UPDATE envios SET "+
      "enviado = 0, "+
      "idchequero = "+envio.idchequero+", "+
      "idsupalce = "+envio.idsupalce+", "+
      "idchofer = "+envio.idchofer+", "+
      "tractorista1 = "+envio.tractorista1+", "+
      "tractorista2 = "+envio.tractorista2+", "+
      "tractorista3 = "+envio.tractorista3+", "+
      "ayud1 = "+envio.ayud1+", "+
      "ayud2 = "+envio.ayud2+", "+
      "ayud3 = "+envio.ayud3+", "+
      "ayud4 = "+envio.ayud4+", "+
      "ayud5 = "+envio.ayud5+", "+
      "ayud6 = "+envio.ayud6+", "+
      "desp1 = "+envio.desp1+", "+
      "desp2 = "+envio.desp2+", "+
      "desp3 = "+envio.desp3+", "+
      "desp4 = "+envio.desp4+", "+
      "desp5 = "+envio.desp5+", "+
      "acom1 = "+envio.acom1+", "+
      "acom2 = "+envio.acom2+", "+
      "idconductor = "+envio.idconductor+", "+
      "idcamion = "+envio.idcamion+", "+
      "idgondola = "+envio.idgondola+", "+
      "idcosechadora = "+envio.idcosechadora+", "+
      "tractor1 = "+envio.tractor1+", "+
      "tractor2 = "+envio.tractor2+", "+
      "tractor3 = "+envio.tractor3+", "+
      "escaneado = "+envio.escaneado+", "+
      "fhcorte = '"+envio.fhcorte+"' "+
      "WHERE envio = "+envio.envio;
      console.log("SCRIPT UPDATE");
      console.log(sql);
      return this.db.executeSql(sql,[])
      .catch(err=>{
        console.log("ERROR EN UPDATE");
        console.log(err);
        return Promise.reject(err);
      });
    }

    eliminarHistoria(){
      let limite = new Date();
      limite.setDate(limite.getDate() - 21);

      this.db.executeSql("DELETE FROM rozadores "+
        "WHERE envio IN ( "+
        "SELECT envio FROM envios WHERE fechafin < '"+limite.toISOString()+"')",[])
        .then(()=>{
          return this.db.executeSql("DELETE FROM envios WHERE fechafin < '"+limite.toISOString()+"'",[]);
        })
        .catch(err=> console.log(err));
        
      

    }

    formatDate(){
    Date.prototype.toISOString = function() {
      var tzo = -this.getTimezoneOffset(),
          dif = tzo >= 0 ? '+' : '-',
          pad = function(num) {
              var norm = Math.floor(Math.abs(num));
              return (norm < 10 ? '0' : '') + norm;
          };
      return this.getFullYear() +
          '-' + pad(this.getMonth() + 1) +
          '-' + pad(this.getDate()) +
          'T' + pad(this.getHours()) +
          ':' + pad(this.getMinutes()) +
          ':' + pad(this.getSeconds()) +
          dif + pad(tzo / 60) +
          ':' + pad(tzo % 60);
  }
  }

    consultarCamionEnEnviosRecientes(idcamion,fecha){//fecha-> en formato ISO a partir de la cual buscar hacia adelante

      let sql = "SELECT idcamion FROM envios WHERE fechafin > '"+fecha+"' and idcamion = "+idcamion;
      return this.db.executeSql(sql, [])
      .then(response => {
          let filas = [];
          for (let index = 0; index < response.rows.length; index++) {
              filas.push(response.rows.item(index));
          }
          return Promise.resolve(filas);
      })
      .catch(error => Promise.reject(error));
    }
}

