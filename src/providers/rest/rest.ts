
import { Injectable, Injector } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http';
import { DatabaseProvider } from '../database/database';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Uid } from '@ionic-native/uid';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { PrinterProvider } from '../printer/printer';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
  private appUsr: string = "AgricolaMobile";
  private appPwd: string = "C4ss4Agr1col_Mobile0";
/**
 * nombres de métodos de recuperación de datos
 */
  private urlHdasLotes: string = "/api/DatosMaestros/GetDatosHaciendasLotes";
  private urlTrabajadores: string = "/api/DatosMaestros/GetDatosTrabajadores?App=1";
  private urlEquipos: string = "/api/DatosMaestros/GetDatosEquipos";
  private urlLiberaciones: string = "/api/DatosMaestros/GetDatosLiberaciones";
  private urlBloques: string = "/api/DatosMaestros/GetDatosBloques";
  private urlEnvios: string = "/api/Envios/GenerarEnvio";
  private urlEtapa: string = "/api/Etapas/CambioEtapaVehiculo";
  private urlLog: string  = "/api/Cosecha/Log";
  private urlTiempo: string  = "/api/Cosecha/Parametros";

  private token: string = "";
  constructor(public http: HTTP,  private storage: Storage
    , private dbservice: DatabaseProvider
    , private backgroundMode: BackgroundMode
    , private uid: Uid, private androidPermissions: AndroidPermissions
    , private printer: PrinterProvider
    , private injector: Injector) {
      this.dbservice = injector.get(DatabaseProvider);
    console.log('Hello RestProvider Provider');
    this.http.setSSLCertMode('nocheck');


    // Run when the device is ready
    
  }

  async getImei() {
    const { hasPermission } = await this.androidPermissions.checkPermission(
      this.androidPermissions.PERMISSION.READ_PHONE_STATE
    );
   
    if (!hasPermission) {
      const result = await this.androidPermissions.requestPermission(
        this.androidPermissions.PERMISSION.READ_PHONE_STATE
      );
   
      if (!result.hasPermission) {
        throw new Error('Permisos requeridos');
      }
   
      // ok, a user gave us permission, we can get him identifiers after restart app
      return;
    }
   
     return this.uid.IMEI
   }

  autenticar(usr: string, pwd:string){
    usr=usr.trim();
    return this.storage.get('server').then((val) => {
      let apiUrl = val+'/api/Sesiones/GetToken?appUsr='+this.appUsr+'&appPwd='+this.appPwd+'&usrLogin='+usr+'&usrPwd='+pwd;
    console.log(apiUrl);
    return this.http.get(apiUrl, {}, {})
    .then(data => {
      //console.log(data.data);
      return JSON.parse(data.data);
    })
    .catch(error => {
      console.log(error);
      return [];
    });
    });
  }

  testSesion(token){
    let apiUrl ="";
    return this.storage.get('server').then((val) => {
      apiUrl = val+'/api/Sesiones/Test';

      
      return this.http.get(apiUrl, {}, {'Content-Type': 'application/json', 'token': token})
    }
    )
    //console.log(apiUrl);
    
    .then(data => {
      console.log(data);
      return JSON.parse(data.data);
    })
    .catch(error => {
      console.log(error);
      Promise.reject(error);
    });
  }


  sendEnvios(){
    let imei = "";
    let usr = "";

    let envios = [];
    this.storage.get("token").then( stoken =>{
    this.token = stoken;
    return this.storage.get("imei")
    }).then(data =>{
        imei = data;
        return this.storage.get("usuario");
    }) 
    .then(data =>{
      usr = data;
      return this.dbservice.getEnviosNoEnviados();
    })   
    .then(data =>{
      let body = [];
      if(data!= null){
        console.log("NO ENVIADOS");
        console.log(data);
        envios= data;
        let promRelleno = Promise.resolve();
        
        let obj = {};
        let rozadores = [];
        console.log(data);
        data.forEach(item => {
          // SE ARMA CADENA DE ENVIO
          
          promRelleno = promRelleno.then(()=>{
            rozadores = [];
           return this.dbservice.getRozadoresEnvio(item.envio)
            .then(data => {
              rozadores = data;
            

            obj = {};
            obj["BkEnvio"] = item.envio;
            obj["IdDispositivo"] = imei;
            obj["Usuario"] = usr;
            obj["Cadena"] = this.printer.getCadena2D(item,1, rozadores, item.manual);
            obj["Vehiculo"] = item.codvehiculo;
            obj["Latitud"] = item.lat == null || item.lat == "null" ? "0" : item.lat;
            obj["Longitud"] = item.long == null || item.long == "null" ? "0" : item.long;
            obj["Kiosko"] = false;
            obj["AlzadoraCosechadora"] = item.codcosechadora;
            obj["Liberacion"] = item.codliberacion;
            obj["Barrida"] = item.barrida;
            obj["Manual"] = item.tipoenvio == "mecanizada" ? false : true;
            obj["IdOrdenCosecha"] = item.idordencosecha;
            obj["CamionEscaneado"] = item.escaneado > 0;
            obj["IdZafra"] = item.idzafra;
            obj["Id"] = item.Id;
            obj["BkEnvioRel"] = item.enviorel == 'null' || item.enviorel == null ? null : item.enviorel;
            body.push(obj);

            //CAMBIO DE ESTADO A VEHICULOS DE ENVIOS DE HACE 10 MINUTOS O MENOS

            let today = new Date();
            let fechaEnvio = new Date(item.fechafin);
            var diffMs = (fechaEnvio.getMilliseconds() - today.getMilliseconds()); // milliseconds between now & FechaEnvio
            var diffMins = Math.floor(((diffMs % 86400000) % 3600000) / 60000); // minutes
            
            if(diffMins <= 10){
              this.etapaCargadoHaciaIng(item.codvehiculo);
            }
            return Promise.resolve();
           })
          });

        });
        // console.log(this.token);
        // console.log(body);
        promRelleno.then(()=> {
          if(envios.length > 0){
            //obtenemos la IP DEL SERVER
            this.storage.get('server').then((val) => {
                let apiUrl = val+this.urlEnvios;
                // console.log("BODY REQUEST, datos: "+data.length);
                // console.log(body);
                //SE HACE UN POST AL SERVER
                this.http.setDataSerializer("json");
                return this.http.post(apiUrl, body, {'Content-Type': 'application/json'})
              .then(data => {
                // console.log("BODY ENVIADO");
                // console.log(body);
                // console.log(data);
                return JSON.parse(data.data);
              })
              .then(data =>{
                if(data.Key == true){
                  this.dbservice.marcarEnviados(envios);
                }
                this.getDatos(this.urlTiempo+"?Id=87")//Obtiene el parametro de tiempo para utilizar el camion nuevamente
                .then(param=>{
                  console.log("PARAMETRO",param);
                  this.storage.set("tiempo",param);
                })
              })
              .catch(error => {
                console.log(error);
                return Promise.reject(error);
              });
            });
          }
        })
        
    }
    })
    .catch(err =>{
      console.log(err)
    }
      )
  }

  

  showDate(ISOFormat: string){
    let date = new Date(ISOFormat);
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let dt = date.getDate();

    let sdt:string = ""+dt;
    let smonth:string = ""+month;
    if (dt < 10) {
      sdt = '0' + dt;
    }
    if (month < 10) {
      smonth = '0' + month;
    }

    //console.log(sdt+'/' + smonth + '/'+year);
    return sdt+'/' + smonth + '/'+year;
  }

  showTime(ISOFormat: string){
    let date = new Date(ISOFormat);
    let hh = date.getHours();
    let mm = date.getMinutes();
    let ss = date.getSeconds();
    let shh =""+hh;
    let smm = ""+mm;
    let sss = ""+ss;
    if (hh < 10) {
      shh = '0' + hh;
    }
    if (mm < 10) {
      smm = '0' + mm;
    }
    if (ss < 10) {
      sss = '0' + ss;
    }

    //console.log(hh+":"+mm);
    return shh+":"+smm+":"+sss;
  }
  
  syncData(opc: string[]){
    
    var promesa =  Promise.resolve();
    promesa = promesa.then(() => {return this.getToken()});
    if(opc.indexOf("haciendas") > -1){
      promesa = promesa.then(() => {return  this.actualizarHaciendas()});
    }
    if(opc.indexOf("trabajadores") > -1){
      promesa = promesa.then(() => {return this.actualizarTrabajadores()});
    }

    if(opc.indexOf("equipos") > -1){
      promesa = promesa.then(() => {return this.actualizarEquipos()});
    }

    if(opc.indexOf("liberaciones") > -1){
      promesa = promesa.then(() => {return this.actualizarLiberaciones()});
    }

    if(opc.indexOf("bloque") > -1){
      promesa = promesa.then(() => {return this.actualizarBloques()});
    }

    
    return promesa; 
    
    
  }
  

  actualizarHaciendas(){
    //ACTUALIZA LAS HACIENDAS
    let datos;
    return this.getDatos(this.urlHdasLotes).then( retorno => {
      // console.log(haciendas);
      datos = retorno;
      console.log(datos["<haciendas>k__BackingField"]);
      let datosarr = datos["<haciendas>k__BackingField"];

      //console.log(datosarr);
      datosarr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj["<IdHacienda>k__BackingField"]).indexOf(obj["<IdHacienda>k__BackingField"]) === pos;
      });
       console.log(datosarr);

      let sqlhdas = this.prepararSql(datosarr,'haciendas');
      //console.log(sqlhdas);
      this.dbservice.limpiarCatalogosPorNombreTabla('haciendas').then(() =>{
        
        this.dbservice.insertarCatalogosPorTransaccion(sqlhdas).catch(err => {console.log(err)});
      }).then(()=> {
        console.log(datos["<lotes>k__BackingField"]);
        let sqllotes = this.prepararSql(datos["<lotes>k__BackingField"],'lotes');
        this.dbservice.limpiarCatalogosPorNombreTabla('lotes').then(()=>{
          this.dbservice.insertarCatalogosPorTransaccion(sqllotes).catch(err => {console.log(err)});
        });
        
      });
      
    }).catch((err)=>{
      console.log(err);
      return Promise.reject(err);
    });
  }

  actualizarTrabajadores(){
    //ACTUALIZA LAS HACIENDAS
    let datos;
    return this.getDatos(this.urlTrabajadores).then( retorno => {
      // console.log(haciendas);
      datos = retorno;
      console.log(datos["<trabajadores>k__BackingField"]);
      let sql = this.prepararSql(datos["<trabajadores>k__BackingField"],'trabajadores');
      //console.log(sql);
      this.dbservice.limpiarCatalogosPorNombreTabla('trabajadores').then(() =>{
        
        this.dbservice.insertarCatalogosPorTransaccion(sql).catch(err => {console.log(err)});
      })
      
    }).catch((err)=>{
      console.log(err);
      return Promise.reject(err);
    });
  }

  actualizarEquipos(){
    //ACTUALIZA LAS HACIENDAS
    let datos;
    return this.getDatos(this.urlEquipos).then( retorno => {
      // console.log(haciendas);
      datos = retorno;
      console.log(datos["<Equipos>k__BackingField"]);
      let sql = this.prepararSql(datos["<Equipos>k__BackingField"],'equipos');
      //console.log(sql);
      this.dbservice.limpiarCatalogosPorNombreTabla('equipos').then(() =>{
        
        this.dbservice.insertarCatalogosPorTransaccion(sql).catch(err => {console.log(err)});
      })
      
    }).catch((err)=>{
      console.log(err);
      return Promise.reject(err);
    });
  }

  actualizarLiberaciones(){
    //ACTUALIZA LAS HACIENDAS
    let datos;
    return this.getDatos(this.urlLiberaciones).then( retorno => {
      // console.log(haciendas);
      datos = retorno;
      console.log(datos["<Liberaciones>k__BackingField"]);
      let sql = this.prepararSql(datos["<Liberaciones>k__BackingField"],'liberaciones');
      //console.log(sql);
      this.dbservice.limpiarCatalogosPorNombreTabla('liberaciones').then(() =>{
        
        this.dbservice.insertarCatalogosPorTransaccion(sql).catch(err => {console.log(err)});
      })
      
    }).catch((err)=>{
      console.log(err);
      return Promise.reject(err);
    });
  }

  actualizarBloques(){
    //ACTUALIZA LAS HACIENDAS
    let datos;
    return this.getDatos(this.urlBloques).then( retorno => {
      // console.log(haciendas);
      datos = retorno;
      console.log(datos["<Bloques>k__BackingField"]);
      let sql = this.prepararSql(datos["<Bloques>k__BackingField"],'bloques');
      //console.log(sql);
      this.dbservice.limpiarCatalogosPorNombreTabla('bloques').then(() =>{
        
        this.dbservice.insertarCatalogosPorTransaccion(sql).catch(err => {console.log(err)});
      })
      
    }).catch((err)=>{
      console.log(err);
      return Promise.reject(err);
    });
  }

  getToken(){
    return this.storage.get('token').then((val) => {
      this.token = val;
      });
  }
  //JSON.parse(data)
  getDatos(metodo){
    
    return   this.storage.get('server').then((val) => {
      let apiUrl = val+metodo;
    console.log(apiUrl);
    console.log("token:"+this.token);
    return this.http.get(apiUrl, {}, {'Content-Type': 'application/json', 'token':this.token})
    .then(data => {
      //console.log(data);
      return JSON.parse(data.data);
    })
    .catch(error => {
      console.log(error);
      return Promise.reject(error);
    });
    });

    
  }

  cambioEtapa(etapa, vehiculo){
    return   this.storage.get('server').then((val) => {
      let apiUrl = val+this.urlEtapa+"?etapa="+etapa+"&vehiculo="+vehiculo;
    console.log(apiUrl);
    console.log("token:"+this.token);
    return this.http.get(apiUrl, {}, {'Content-Type': 'application/json', 'token':this.token})
    .then(data => {
      //console.log(data);
      return JSON.parse(data.data);
    })
    .catch(error => {
      console.log(error);
      return Promise.reject(error);
    });
    });
  }

  etapaCargando(vehiculo){
    return this.cambioEtapa(5,vehiculo);
  }

  etapaCargadoProp(vehiculo){
    return this.cambioEtapa(6,vehiculo);
  }

  etapaCargadoHaciaIng(vehiculo){
    return this.cambioEtapa(7,vehiculo);
  }

  prepararSql(array: any[], tabla: string){
    let sqlarr: any[] = []; 
    let sql = "";
    if(array !== null)
    switch (tabla) {
      case 'haciendas':
        
        array.forEach(item => {

          sql = 'INSERT INTO '+tabla+'(idhacienda, bkhacienda, dshacienda, proveedor)'+
          ' VALUES('+item["<IdHacienda>k__BackingField"]+',\''+item["<Codigo>k__BackingField"].trim()+'\',\''+item["<Codigo>k__BackingField"].trim()+" - "+item["<Nombre>k__BackingField"].trim()+" - "+item["<DsProveedor>k__BackingField"].trim()+'\',\''+(item["<DsProveedor>k__BackingField"] == null ? item["<DsProveedor>k__BackingField"].trim() : '')+'\')';
          sqlarr.push(sql);
        });
        break;
        case 'lotes':
        
        array.forEach(item => {

          sql = "INSERT INTO "+tabla+"(idlote , bklote, dslote, idhacienda, poligono)"+
          " VALUES("+item["<IdLote>k__BackingField"]+",'"+item["<BkLote>k__BackingField"].trim()+"','"+(item["<DsLote>k__BackingField"] == null? item["<DsLote>k__BackingField"].trim():'')+"',"+item["<IdHacienda>k__BackingField"]+", '"+item["<Poligono>k__BackingField"]+"')";
          sqlarr.push(sql);
        });
        break;

        case 'trabajadores':
        
        array.forEach(item => {

          sql = "INSERT INTO "+tabla+"(idtrabajador , codtrabajador, nomtrabajador, nombre, profesion, generico)"+
          " VALUES("+item["<IdTrabajador>k__BackingField"]+",'"+item["<CodTrabajador>k__BackingField"].trim()+"','"+item["<CodTrabajador>k__BackingField"].trim()+" - "+item["<NomTrabajador>k__BackingField"].trim()+"','"+item["<NomTrabajador>k__BackingField"].trim()+"','"+item["<Profesion>k__BackingField"].trim()+"', "+(item["<Generico>k__BackingField"] == true ? 1 : 0)+")";
          sqlarr.push(sql);
        });
        break;
    
        case 'equipos':
        
        array.forEach(item => {

          sql = "INSERT INTO "+tabla+"(idequipo , codequipo , nombreequipo , flota, tipo )"+
          " VALUES("+item["<IdEquipo>k__BackingField"]+",'"+item["<CodEquipo>k__BackingField"].trim()+"','"+item["<CodEquipo>k__BackingField"].trim()+" - "+item["<NombreEquipo>k__BackingField"].trim()+"',"+item["<Flota>k__BackingField"]+","+item["<TIPO>k__BackingField"]+")";
          sqlarr.push(sql);
        });
        break;
        
        case 'liberaciones':
        
        array.forEach(item => {

          sql = "INSERT INTO "+tabla+"(codliberacion, idlote, idhacienda, idbloque, bloque, fechahora, manual, idordencosecha, idzafra)"+
          " VALUES('"+item["<Codigo>k__BackingField"].trim()+"',"+item["<IdLote>k__BackingField"]+","+item["<IdHacienda>k__BackingField"]+","+item["<IdBloque>k__BackingField"]+",'"+item["<Bloque>k__BackingField"]+"','"+item["<FechaHoraISO>k__BackingField"]+"',"+(item["<Manual>k__BackingField"] == true ? 1 : 0)+","+item["<IdOrdenCosecha>k__BackingField"]+","+item["<IdZafra>k__BackingField"]+")";
          sqlarr.push(sql);
        });
        break;

        case 'bloques':
        
        array.forEach(item => {

          sql = "INSERT INTO "+tabla+"( idbloque ,codbloque, dsbloque)"+
          " VALUES("+item["<IdFrente>k__BackingField"]+",'"+item["<BkFrente>k__BackingField"].trim()+"','"+item["<BkFrente>k__BackingField"].trim()+" - "+item["<DsFrente>k__BackingField"].trim()+"')";
          sqlarr.push(sql);
        });
        break;
    
      default:
        break;

      
    }
    return sqlarr;
  }

  sendLog(envio){

    
    

    return   this.storage.get('server').then((val) => {
      let apiUrl = val+this.urlLog;
    console.log(apiUrl);
    console.log("token:"+this.token);
    return this.http.post(apiUrl, envio.log, {'Content-Type': 'application/json', 'token':this.token})
    .then(data => {
      //console.log(data);
      return JSON.parse(data.data);
    })
    .catch(error => {
      console.log(error);
      return Promise.reject(error);
    });
    });
  }

}
