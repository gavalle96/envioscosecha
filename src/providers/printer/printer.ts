import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { Storage } from '@ionic/storage';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { DatabaseProvider } from '../database/database';
/*
  Generated class for the PrinterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PrinterProvider {
  private macAddress = "00:22:58:3C:C2:96";
  private appCoop = "mecanizada";
  private alturaPDF = 0;
  constructor(public http: HttpClient
    , private bluetoothSerial: BluetoothSerial
    , private storage: Storage
    , private dbsvc: DatabaseProvider
    ) {
    console.log('Hello PrinterProvider Provider');
    this.storage.get("aplicativo").then(val => {
      if(val != null && val != "" && val != undefined){
        this.appCoop = val;
      }
    })
  }

  getAplicativo(){
   return this.storage.get("aplicativo").then(val => {
      if(val != null && val != "" && val != undefined){
        this.appCoop = val;
      }
    })
  }

  getMacAdress(){
    this.storage.get("macprinter").then(data=>{
      this.macAddress = data;
    })
  }

  showDate(ISOFormat: string){
    let date = new Date(ISOFormat);
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let dt = date.getDate();

    let sdt:string = ""+dt;
    let smonth:string = ""+month;
    if (dt < 10) {
      sdt = '0' + dt;
    }
    if (month < 10) {
      smonth = '0' + month;
    }

    //console.log(sdt+'/' + smonth + '/'+year);
    return sdt+'/' + smonth + '/'+year;
  }

  showDateIniFin(ISOFormat: string){
    let date = new Date(ISOFormat);
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let dt = date.getDate();

    let syear = (year+"").slice(2);

    let sdt:string = ""+dt;
    let smonth:string = ""+month;
    if (dt < 10) {
      sdt = '0' + dt;
    }
    if (month < 10) {
      smonth = '0' + month;
    }

    //console.log(sdt+'/' + smonth + '/'+year);
    return sdt+'/' + smonth + '/'+syear;
  }

  showTime(ISOFormat: string){
    let date = new Date(ISOFormat);
    let hh = date.getHours();
    let mm = date.getMinutes();
    let ss = date.getSeconds();
    let shh =""+hh;
    let smm = ""+mm;
    let sss = ""+ss;
    if (hh < 10) {
      shh = '0' + hh;
    }
    if (mm < 10) {
      smm = '0' + mm;
    }
    if (ss < 10) {
      sss = '0' + ss;
    }

    //console.log(hh+":"+mm);
    return shh+":"+smm+":"+sss;
  }

  
  public getCadena2D(item, carga = 1, rozadores = [], manual = 2){
    
    console.log(item);
    let cadena = "";
    
      cadena +=  "<LIBERACION>="+item.codliberacion+"%"+
      "<FRENTE>="+item.codbloque+"%"+
      "<CODCAMION>="+item.codvehiculo+"%"+
      "<CODCONDUCTOR>="+item.codconductor+"%"+
       "<GONDOLA>="+item.codgondola+"%";
      
      if( (manual == 2 && (this.appCoop == "coop" || this.appCoop == "manual")) || manual == 1){
        cadena += "<CODALZADORA>="+item.codcosechadora+"%";
      }
      else if( (manual == 2 && this.appCoop == "mecanizada") || manual == 0) {
        cadena += "<CODCOSECHADORA>="+item.codcosechadora+"%";
      }
    
      if(this.appCoop != "picada"){
        cadena += "<CODCHOFER>="+item.codopcos+"%";
      }
      cadena += "<CODCHEQUERO>="+item.codchequero+"%";
      if(this.appCoop == "mecanizada" || this.appCoop == "manual"){
        cadena += "<CODSUPERALCE>="+item.codsupalce+"%";
      }
      
      cadena += "<ENVIO>="+item.envio+"%"+
      // "<FCORTE>="+this.showDate(item.fhcorte)+"%"+
      // "<HCORTE>="+this.showTime(item.fhcorte)+"%"+
      "<FCORTE>="+item.fhcorte.split(" ")[0]+"%"+
      "<HCORTE>="+item.fhcorte.split(" ")[1]+"%"+
      "<FECHAINI>="+this.showDateIniFin(item.fechaini)+" "+this.showTime(item.fechaini)+"%"+
      "<FECHAFIN>="+this.showDateIniFin(item.fechafin)+" "+this.showTime(item.fechafin)+"%";
      
      if(this.appCoop != "coop" && this.appCoop != "picada")
      {
      // A PARTIR DE AQUI SE AÑADEN CONDICIONALMENTE
      if(item.codtractor1 != "" && item.codtractor1 != undefined && item.codtractor1 != null){
        cadena += "<TRACTOR>="+item.codtractor1+"%";
      }
      
      if(item.codtractor2 != "" && item.codtractor2 != undefined && item.codtractor2 != null){
        cadena += "<TRACTOR>="+item.codtractor2+"%";
      }
      
      if(item.codtractor3 != "" && item.codtractor3 != undefined && item.codtractor3 != null){
        cadena += "<TRACTOR>="+item.codtractor3+"%";
      }
      
      if(item.codtractorista1 != "" && item.codtractorista1 != undefined && item.codtractorista1 != null){
        cadena += "<TRACTORISTA>="+item.codtractorista1+"%";
      }
      
      if(item.codtractorista2 != "" && item.codtractorista2 != undefined && item.codtractorista2 != null){
        cadena += "<TRACTORISTA>="+item.codtractorista2+"%";
      }
      
      if(item.codtractorista3 != "" && item.codtractorista3 != undefined && item.codtractorista3 != null){
          cadena += "<TRACTORISTA>="+item.codtractorista3+"%";
      }
      
      
      if(item.codacomodador1 != "" && item.codacomodador1 != undefined && item.codacomodador1 != null){
        cadena += "<ACOMODADOR>="+item.codacomodador1+"%";
      }
      
      if(item.codacomodador2 != "" && item.codacomodador2 != undefined && item.codacomodador2 != null){
        cadena += "<ACOMODADOR>="+item.codacomodador2+"%";
      }
      
      if(item.codayudante1 != "" && item.codayudante1 != undefined && item.codayudante1 != null){
        cadena += "<AYUDANTE>="+item.codayudante1+"%";
      }
      
      if(item.codayudante2 != "" && item.codayudante2 != undefined && item.codayudante2 != null){
        cadena += "<AYUDANTE>="+item.codayudante2+"%";
      }
      
      if(item.codayudante3 != "" && item.codayudante3 != undefined && item.codayudante3 != null){
        cadena += "<AYUDANTE>="+item.codayudante3+"%";
      }
      if(item.codayudante4 != "" && item.codayudante4 != undefined && item.codayudante4 != null){
        cadena += "<AYUDANTE>="+item.codayudante4+"%";
      }
      if(item.codayudante5 != "" && item.codayudante5 != undefined && item.codayudante5 != null){
        cadena += "<AYUDANTE>="+item.codayudante5+"%";
      }
      if(item.codayudante6 != "" && item.codayudante6 != undefined && item.codayudante6 != null){
        cadena += "<AYUDANTE>="+item.codayudante6+"%";
      }
        }

        console.log("DESPUNTADORES");
        for (let index = 1; index <= 5 ; index++) {
          console.log(item["coddespuntador"+index])
          if(item["coddespuntador"+index] != undefined){
            cadena += "<DESPUNTADOR>="+item["coddespuntador"+index]+"%";
          }
          
        }
      cadena += "<BARRIDA>="+item.barrida+"%";

      rozadores.forEach(item=>{
        cadena += "<ROZADOR>="+item.codtrabajador+"%<UNADAS>="+item.unadas+"%";
      })
      

      cadena += " %"; // FIN DE CADENA
   
   console.log("CADENA DE IMPRESION");
   console.log(cadena);

return cadena;
//  return cadAux;
 
  }
  getSubCadena(limite,subcadena){
    let cadena ="";
    
    if(subcadena.length > limite){
      for (let index = 0; index < limite+1; index++) {
        cadena += subcadena[index];
      }
      cadena+="\r\n";

      for (let index = limite +1; index < subcadena.length; index++) {
        cadena += subcadena[index];
        
      }
      
    }
    else{
      cadena = subcadena;
    }
    return cadena;

  }
  getCadenaTexto(item, rozadores = []){
    //coordenada vertical que aumentará gradualmente
    this.alturaPDF = this.alturaPDF + (rozadores.length * 6);
    
    let coordenada = this.alturaPDF +210;
    //limite de caracteres por linea
    let limcadena = 55;
    //nueva linea
    let nl = "\r\n";

    let cadena = "";
    let subcadena = "";
    //inicia cadena a imprimir
    cadena += "ML 25"+nl;
    cadena += "TEXT 5 0 25 "+coordenada+nl;
    // cadena += this.getSubCadena(limcadena,"Altura: "+this.alturaPDF) + nl;
    cadena += this.getSubCadena(limcadena,"LIBERACION: "+item.codliberacion+" - ["+item.dshacienda+"] - "+item.bklote+" - "+item.dslote) + nl;
    
    cadena += this.getSubCadena(limcadena,"CAMION: "+item.vehiculo) + nl;
    cadena += this.getSubCadena(limcadena,"ENVIO: "+item.envio) + nl;

    cadena += "ENDML"+nl;   

     coordenada += 130;
     let tituloTiquete = "TIQUETE DE COSECHA MECANIZADA";
     switch (this.appCoop) {
       case "coop":
         tituloTiquete = "TIQUETE DE COSECHA DE COOPERATIVAS"
         break;
        case "picada":
         tituloTiquete = "TIQUETE DE COSECHA DE CAÑA PICADA";
        break;
        case "manual":
         tituloTiquete = "TIQUETE DE COSECHA MANUAL";
        break;     
       default:
         break;
     }
    cadena += "CENTER"+nl;
    cadena+= "TEXT 0 3 25 "+coordenada+" "+tituloTiquete+nl;
    coordenada += 15;
    cadena += "TEXT 0 3 25 "+coordenada+" -----------------------------------"+nl;
    cadena += "LEFT"+nl;
    coordenada += 30;
    let cadena2 = "";
    cadena2 += "ML 20"+nl;
     cadena2 += "TEXT 7 0 25 "+coordenada+nl; //probar con fuente 5....
    //cadena += "CENTER"+nl;
    limcadena = 60;
    // cadena += this.getSubCadena(limcadena,item.codliberacion+" - "+item.dshacienda+" - "+item.bklote) + nl;
    // cadena += "LEFT"+nl;
    cadena2 += this.getSubCadena(limcadena,"LIBERACION: "+item.codliberacion+" - "+item.dshacienda+" - "+item.bklote+" - "+item.dslote) + nl;
    cadena2 += this.getSubCadena(limcadena,"BLOQUE: "+item.codbloque+" - NRO. ENVIO: "+item.envio) + nl;
    cadena2 += this.getSubCadena(limcadena,"CAMION: "+item.vehiculo) + nl;
    cadena2 += this.getSubCadena(limcadena,"GONDOLA: "+item.gondola) + nl;
    cadena2 += this.getSubCadena(limcadena,"CONDUCTOR: "+item.conductor) + nl;
    cadena2 += this.getSubCadena(limcadena,"FECHA CORTE: "+item.fhcorte.split(" ")[0]+" - HORA CORTE: "+item.fhcorte.split(" ")[1]) + nl;
    
    let txtcargadora ="COSECHADORA";
    let txtOpCargadora ="CHOFER COS";
    if(this.appCoop != "mecanizada")
    {
      txtcargadora = "ALZADORA";
      txtOpCargadora ="CHOFER ALZ";
    }
    
    if(this.appCoop != "picada"){
    cadena2 += this.getSubCadena(limcadena,txtcargadora+": "+item.cosechadora) + nl;
    cadena2 += this.getSubCadena(limcadena,txtOpCargadora+": "+item.opcos) + nl;
    }
    cadena2 += this.getSubCadena(limcadena,"CHEQUERO: "+item.chequero) + nl;
    if(this.appCoop == "mecanizada" || this.appCoop == "manual"){
      cadena2 += this.getSubCadena(limcadena,"SUPERVISOR ALCE: "+item.supalce) + nl;
    }
    cadena2 += this.getSubCadena(limcadena,"FECHA INICIO: "+this.showDate(item.fechaini)+" "+this.showTime(item.fechaini)) + nl;
    cadena2 += this.getSubCadena(limcadena,"FECHA FIN: "+this.showDate(item.fechafin)+" "+this.showTime(item.fechafin)) + nl;
    
    if(item.codtractor1 != "" && item.codtractor1 != undefined && item.codtractor1 != null){
      cadena2 += "TRACTOR: "+nl;
      cadena2 += item.tractor1+nl;
    }
    
    if(item.codtractor2 != "" && item.codtractor2 != undefined && item.codtractor2 != null){
      cadena2 += item.tractor2+nl;
    }
    
    if(item.codtractor3 != "" && item.codtractor3 != undefined && item.codtractor3 != null){
      cadena2 += item.tractor3+nl;
    }
    
    if(item.codtractorista1 != "" && item.codtractorista1 != undefined && item.codtractorista1 != null){
      cadena2 += "TRACTORISTA: "+nl;
      cadena2 += item.tractorista1+nl;
    }
    
    if(item.codtractorista2 != "" && item.codtractorista2 != undefined && item.codtractorista2 != null){
      cadena2 += item.tractorista2+nl;
    }
    
    if(item.codtractorista3 != "" && item.codtractorista3 != undefined && item.codtractorista3 != null){
      cadena2 += item.tractorista3+nl;
    }
    

    if(item.codacomodador1 != "" && item.codacomodador1 != undefined && item.codacomodador1 != null){
      cadena2 += "ACOMODADOR: "+nl;
      cadena2 += item.acomodador1+nl;
    }
    
    if(item.codacomodador2 != "" && item.codacomodador2 != undefined && item.codacomodador2 != null){
      cadena2 += item.acomodador2+nl;
    }
    
    

    if(item.codayudante1 != "" && item.codayudante1 != undefined && item.codayudante1 != null){
      cadena2 += "AYUDANTE: "+nl;
      cadena2 += item.ayudante1+nl;
    }
    
    if(item.codayudante2 != "" && item.codayudante2 != undefined && item.codayudante2 != null){
      cadena2 += item.ayudante2+nl;
    }
    if(item.codayudante3 != "" && item.codayudante3 != undefined && item.codayudante3 != null){
      cadena2 += item.ayudante3+nl;
    }
    if(item.codayudante4 != "" && item.codayudante4 != undefined && item.codayudante4 != null){
      cadena2 += item.ayudante4+nl;
    }
    if(item.codayudante5 != "" && item.codayudante5 != undefined && item.codayudante5 != null){
      cadena2 += item.ayudante5+nl;
    }
    if(item.codayudante6 != "" && item.codayudante6 != undefined && item.codayudante6 != null){
      cadena2 += item.ayudante6+nl;
    }
    
    

    

    
    // despuntadores.forEach(item =>{
      
    //   cadena2 += "ROZADOR: " + item.nomtrabajador+" : UÑADAS: "+item.unadas+nl;
    // })
    for(let i = 1; i<=5;i++){
      if(item["coddespuntador"+i] != "" && item["coddespuntador"+i] != undefined && item["coddespuntador"+i] != null){
        cadena2 += "DESPUNTADOR: " +item["despuntador"+i]+nl;
      }
    }

    rozadores.forEach(item =>{
      
      cadena2 += "ROZADOR: " + item.nomtrabajador+" : UÑADAS: "+item.unadas+nl;
    })

    cadena2 += "VIAJE BARRIDA: "+(item.barrida > 0? "SI" : "NO")+nl;
    cadena2 += "ENDML"+nl;
    cadena += cadena2;

    coordenada += (20 * cadena2.split(nl).length); //por cada linea aumenta el valor de coordenada
    this.alturaPDF = coordenada;
    return cadena;
  }
  
  getCadenaImpresion(item, copias, rozadores = [],manual=2){
    this.alturaPDF = 0;
    let cadena2D = this.getCadena2D(item,1, rozadores, manual);
    let cadenaTexto = this.getCadenaTexto(item, rozadores);
    let heigth = this.alturaPDF;
    console.log("Altura del ticket");
    console.log(heigth);
    
    if(this.appCoop=="coop" || this.appCoop =="picada" ){
      heigth = 700;
    }
    // else{
    //   heigth = heigth + 360; //Quise decir HEIGHT :P
    // }
    let cpclConfigLabel = "! 0 200 200 "+heigth+" "+copias+"\r\n" 
    + "TONE 0\r\n"
    + "SPEED 3\r\n"
    + "ON-FEED REPRINT\r\n" //O IGNORE
    + "NO-PACE\r\n"
    + "BAR-SENSE\r\n"
    + "B PDF-417 100 5 XD 2 YD 6 C 15 S 5\r\n"
    + cadena2D+ "\r\n"
    + "ENDPDF\r\n"   
    + cadenaTexto
    + "PRINT\r\n";

    return cpclConfigLabel;
  }
  //BLUETOOTH
  conectarImpresora(){
    return this.bluetoothSerial.connect(this.macAddress);
  }
  desconectarImpresora(){
    return this.bluetoothSerial.disconnect();

    //hacer esto en la clase cliente

    // .then(data =>{ 
    //   this.subBluetooth.unsubscribe();
    // })
    // .catch(err => console.log(err));
  }

  imprimir(item, copias, rozadores = []){
    this.alturaPDF = 0;
    // Write a string
    var manual = 2;
    return this.getAplicativo()
    .then(()=>{
    return this.dbsvc.getCatalogoFiltrado("liberaciones", [{campo: "codliberacion",valor: item.codliberacion}])
    }).
    then(data => {
      if(data.length > 0){
        manual = data[0]["manual"];
      }
      let cadena = this.getCadenaImpresion(item,copias, rozadores, manual);
      console.log(cadena);
      return this.bluetoothSerial.write(cadena);
    });
     
  }

}
