import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import {CrearPage} from '../crear/crear'
import { SincronizarPage } from '../sincronizar/sincronizar';
import { SettingsPage } from '../settings/settings';
import { IonfcPage } from '../ionfc/ionfc';
import { ConsultarPage } from '../consultar/consultar';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
import { AppVersion } from '@ionic-native/app-version';
import { RestProvider } from '../../providers/rest/rest';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';
import { CorreccionPage } from '../correccion/correccion';
import { CambioAppPage } from '../cambio-app/cambio-app';
import { FiltroEnviosPage } from '../filtro-envios/filtro-envios';
/**
 * Generated class for the PrincipalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-principal',
  templateUrl: 'principal.html',
})
export class PrincipalPage {
  versionnumber;
  gettingLocation: boolean = false;
  admon = false;
  appCoop = "mecanizada";
  pages: any[] =[];
  constructor(public navCtrl: NavController, public menu: MenuController, public navParams: NavParams
    , private storage: Storage
    , private appversion: AppVersion
    , private restSvc: RestProvider
    , private geolocation: Geolocation
  , private diagnostic: Diagnostic
  , private locationAccuracy: LocationAccuracy
  ) {
    
    
    this.appversion.getVersionNumber().then(valor => {
      this.versionnumber = valor;
    });

    this.storage.get("admon").then(val => {
      if(val != null && val != "" && val != undefined){
        this.admon = val;
      }
    })

    this.storage.get("aplicativo").then(val => {
      if(val != null && val != "" && val != undefined){
        this.appCoop = val;
        
      }

      this.addPagesMenu();
      
    })

    
  }

  ionViewDidEnter(){
    this.storage.get("admon").then(val => {
      if(val != null && val != "" && val != undefined){
        this.admon = val;
      }
    })
    
  }
  ionViewWillEnter(){
    this.storage.get("aplicativo").then(val => {
      if(val != null && val != "" && val != undefined){
        this.appCoop = val;
      }
      this.addPagesMenu();
      
    })
  }
  addPagesMenu(){

    this.pages = [
      { title: 'Principal', component: PrincipalPage, push:false, cierraSesion: false , icono: "", mostrar:true},
      { title: 'Descargar Datos', component: SincronizarPage, push:true , cierraSesion: false, icono:"cloud-download", mostrar:true},
      { title: 'Ajustes (TI)', component: SettingsPage, push: true , cierraSesion: false, icono:"settings", mostrar:true}];

      if(this.appCoop != "picada" && this.appCoop != "coop")
        this.pages.push({ title: 'Cambiar Aplicativo', component: CambioAppPage, push: true , cierraSesion: false, icono:"pricetag", mostrar: this.appCoop != "picada" && this.appCoop != "coop"});
      this.pages.push({ title: 'Cerrar Sesión', component: HomePage, push: false , cierraSesion: true, icono:"power", mostrar:true});
  }
  ionViewDidLoad() {
    // console.log('ionViewDidLoad PrincipalPage');
    // console.log("SendEnvios");
      this.restSvc.sendEnvios();

    // console.log("get ubicación");
    this.getUbicacion();

    setInterval(()=>{
      // console.log("SendEnvios");
      this.restSvc.sendEnvios();
    }, 10 * 1000 * 60); //minutos * 1000 milisegundos * 60 segundos

    setInterval(()=>{
      // console.log("get ubicación");
      this.getUbicacion();
    }, 1 * 1000 * 60); //minutos * 1000 milisegundos * 60 segundos
  }

  abreCrear(){
    this.navCtrl.push(CrearPage);
  }

  abreConsultar(){
    this.navCtrl.push(FiltroEnviosPage);
  }

  abreCorreccion(){
    this.navCtrl.push(CorreccionPage);
  }

  getUbicacion(){
    this.gettingLocation = true;


    if(this.gpsDisponible()){
      this.geolocation.getCurrentPosition().then((resp) => {
        
  
        this.storage.set("latitud",resp.coords.latitude);
        this.storage.set("longitud",resp.coords.longitude);
      })
    }
    else{
      //this.loading.dismiss();
    }
  }

  gpsDisponible(){
    return new Promise((resolve) =>{
      this.gpsHabilitado().then(enabled =>{
        //console.log(enabled);
        
        if(!enabled){
          this.enableLocation();
          }

        resolve(enabled);
      });
     });
  }
  gpsHabilitado(){
    return new Promise((resolve)=> {
      this.diagnostic.isGpsLocationEnabled().then(isEnabled => {
      
        return resolve(isEnabled);
      }).catch(err=>{
        console.log(err);
        return resolve(false);
      });
    });
    
  }
    enableLocation()
    {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {

    if(canRequest) {
    // the accuracy option will be ignored by iOS
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
    () => console.log('Request successful'),
    error => console.log('Error requesting location permissions'+JSON.stringify(error))
    );
    }

    });
    }

  openPage(page) {
    // console.log(page);
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    if(page.push){
      this.navCtrl.push(page.component);
    }
    else{
      if(page.cierraSesion){
        //this.storage.set("token",null).then(data=>{
          this.storage.set("sesionactiva",false).then(data=>{
          
          this.navCtrl.setRoot(page.component);
        });
      }
      else{

        this.navCtrl.setRoot(page.component);
      }
    }
  }
}
