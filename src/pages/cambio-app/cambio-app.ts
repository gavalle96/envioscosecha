import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the CambioAppPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cambio-app',
  templateUrl: 'cambio-app.html',
})
export class CambioAppPage {

  aplicativo = "mecanizada";
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private storage: Storage
    , private event: Events) {
      this.storage.get('aplicativo').then((val) => {
        this.aplicativo = val;
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CambioAppPage');
  }

  saveApp(){
    this.storage.set('aplicativo',this.aplicativo)
    .then(()=>{
      this.event.publish("finajustes");
      this.event.publish("cambioapp");
    })
    
  }
}
