import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CambioAppPage } from './cambio-app';

@NgModule({
  declarations: [
    CambioAppPage,
  ],
  imports: [
    IonicPageModule.forChild(CambioAppPage),
  ],
})
export class CambioAppPageModule {}
