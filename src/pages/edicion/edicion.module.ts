import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EdicionPage } from './edicion';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    EdicionPage,
  ],
  imports: [
    IonicPageModule.forChild(EdicionPage),
    SelectSearchableModule
  ],
})
export class EdicionPageModule {}
