import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { Storage } from '@ionic/storage';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { RestProvider } from '../../providers/rest/rest';
import { DatePicker } from '@ionic-native/date-picker';

/**
 * Generated class for the EdicionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edicion',
  templateUrl: 'edicion.html',
})
export class EdicionPage {

  seccion = "tripulacion";
  secciones = ['tripulacion','equipos'];
  envio;
  equipos = [];
  private transportes: any[] = [];
  gondolas = [];

  hayCambios = false;

  fecha;
  hora;
  fecora;

  fechaLiberacion;

  //#region Parametrización de profesiones
profalzadora = "20000232";  
profcosechadora = "20000269";
  profchequero = "20000026";
  proftractorista = "20000271";
  profsupalce = "20000268";
  profacomodadores = "20000262";
  profayudante = "20000270";
  profayudanteAlce = "20000174";
  profdespuntador = "20000176";
  saliendo = false;
//#endregion

//#region listas para campos de trabajadores
  private trabajadores: any[] = [];
  //private bloques: any[] = [];

  //Operadores de cosecha
  private operCosecha: any[] = [];

  //Tractoristas
  private tractoristas: any[] = [];

  //chequeros
  private chequeros: any[] = [];

  //supervisores de alce
  private supervisores: any[] = [];

  //acomodadores
  private acomodadores: any[] = [];

  //ayudantes
  private ayudantes: any[] = [];

  //despuntadores
  private despuntadores: any[] = [];
  //#endregion
  appCoop = "mecanizada";
  fhquema;
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private dbsvc: DatabaseProvider
    , private storage: Storage
    , private barcodeScanner: BarcodeScanner 
    , private alertCtrl: AlertController
    , private datePicker: DatePicker
    , private rest: RestProvider ) {
   let eTmp = navParams.get("envio");
    
    this.dbsvc.getCatalogoFiltrado("envios",[{campo:"envio",valor: eTmp.envio}])
    .then(data=>{
      if(data.length > 0){
        
        this.envio = data[0];
        console.log("ENVIO DE BD");
        console.log(this.envio);
        this.cargarDatos();
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EdicionPage');
    this.storage.get("aplicativo").then(val => {
      if(val != null && val != "" && val != undefined){
        this.appCoop = val;
        console.log(val);
      }
      this.getTrabajadores();
      this.getEquipos();
      return this.storage.get("fhquema");
    })
    .then(val=>
      {
        this.fhquema = val;
      })
    ;

    
  }
 

  
  
 confirmCambios(): Promise<Boolean> {
    let resolveLeaving;
    const canLeave = new Promise<Boolean>(resolve => resolveLeaving = resolve);
    const alert = this.alertCtrl.create({
      title: '¿Guardar Cambios?',
      message: '¿Realmente desea guardar y enviar los cambios?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => resolveLeaving(true)
        },
        {
          text: 'Si',
          handler: () => {
            this.envio.enviado = false;
            this.guardarEdicion();
            resolveLeaving(true);
          }
        }
      ]
    });
    alert.present();
    return canLeave
  }
  selectChange(event: {
    component: SelectSearchableComponent,
    value: any 
    }, campo){
      
      let pId = Object.keys(event.value)[0];
      this.envio[campo] = event.value[pId];
      this.hayCambios = true;
      console.log("Campo: "+campo)
     if(campo == 'idcamion'){
       
       this.envio["idgondola"] = event.value[pId];
       this["gondola"] = this["vehiculo"];
       this.envio.escaneado = 0;
     }
      
    }
    cargarDatos(){

      this.cargarFechaHoraCorte(this.envio["fhcorte"]);

      this.dbsvc.getCatalogoFiltrado("liberaciones", [{campo: "idtrabajador",valor: this.envio.idchequero}]).then(data => {
        if(data.length > 0){
          this["chequero"] = data[0];
        }
      });
      
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.idchequero}]).then(data => {
        if(data.length > 0){
          this["chequero"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.idsupalce}]).then(data => {
        if(data.length > 0){
          this["supAlce"] = data[0];
        }
      });

      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.idchofer}]).then(data => {
        if(data.length > 0){
          this["opcos"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.tractorista1}]).then(data => {
        if(data.length > 0){
          this["tr1"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.tractorista2}]).then(data => {
        if(data.length > 0){
          this["tr2"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.tractorista3}]).then(data => {
        if(data.length > 0){
          this["tr3"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.ayud1}]).then(data => {
        if(data.length > 0){
          this["ayud1"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.ayud2}]).then(data => {
        if(data.length > 0){
          this["ayud2"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.ayud3}]).then(data => {
        if(data.length > 0){
          this["ayud3"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.ayud4}]).then(data => {
        if(data.length > 0){
          this["ayud4"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.ayud5}]).then(data => {
        if(data.length > 0){
          this["ayud5"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.ayud6}]).then(data => {
        if(data.length > 0){
          this["ayud6"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.desp1}]).then(data => {
        if(data.length > 0){
          this["desp1"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.desp2}]).then(data => {
        if(data.length > 0){
          this["desp2"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.desp3}]).then(data => {
        if(data.length > 0){
          this["desp3"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.desp4}]).then(data => {
        if(data.length > 0){
          this["desp4"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.desp5}]).then(data => {
        if(data.length > 0){
          this["desp5"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.acom1}]).then(data => {
        if(data.length > 0){
          this["acom1"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.acom2}]).then(data => {
        if(data.length > 0){
          this["acom2"] = data[0];
        }
      });
      //maquinaria
      this.dbsvc.getCatalogoFiltrado("trabajadores", [{campo: "idtrabajador",valor: this.envio.idconductor}]).then(data => {
        if(data.length > 0){
          this["conductor"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("equipos", [{campo: "idequipo",valor: this.envio.idcamion}]).then(data => {
        if(data.length > 0){
          this["vehiculo"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("equipos", [{campo: "idequipo",valor: this.envio.idgondola}]).then(data => {
        if(data.length > 0){
          this["gondola"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("equipos", [{campo: "idequipo",valor: this.envio.idcosechadora}]).then(data => {
        if(data.length > 0){
          this["cosechadora"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("equipos", [{campo: "idequipo",valor: this.envio.tractor1}]).then(data => {
        if(data.length > 0){
          this["tractor1"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("equipos", [{campo: "idequipo",valor: this.envio.tractor2}]).then(data => {
        if(data.length > 0){
          this["tractor2"] = data[0];
        }
      });
      this.dbsvc.getCatalogoFiltrado("equipos", [{campo: "idequipo",valor: this.envio.tractor3}]).then(data => {
        if(data.length > 0){
          this["tractor3"] = data[0];
        }
      });
    }
  getTrabajadores(){
    let filtro = [{campo:"profesion", valor: "20000106' OR profesion = '20000225"}];
    this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
      // console.log(data);
      this["choferes"] = data;
    });
    this.dbsvc.getCatalogoFiltrado("trabajadores",[],[{campo: "generico desc"}, {campo: "nombre asc"}]).then(data => {
      //  console.log(data.length);
      this.trabajadores = data;

      this.chequeros = data.filter((item) =>{
        return item.profesion == this.profchequero;
      });
      
      
      if(this.appCoop!="mecanizada"){
        this.profcosechadora = this.profalzadora;
        
      }
      
      this.operCosecha = data.filter((item) =>{
       
        return item.profesion == this.profcosechadora;
             
       });
      

      this.tractoristas = data.filter((item) => {
        return item.profesion == this.proftractorista;
      });

      if(this.appCoop == "mecanizada"){
        this.supervisores = data.filter((item)=>{
          return item.profesion == this.profsupalce;
        });
      }
      else{
        this.supervisores = data.filter((item)=>{
          return item.profesion == "20000263";
        });
      }
      this.ayudantes = data.filter((item)=>{
        return item.profesion == (this.appCoop == "manual" ? this.profayudanteAlce : this.profayudante);
      });
      this.despuntadores = data.filter((item)=>{
        return item.profesion == this.profdespuntador;
      });

      this.acomodadores = data.filter((item)=>{
        return item.profesion == this.profacomodadores;
      });
      
    }); 

  }

  removeSelection(campo){
    this[campo] = "";
  }

  barVehiculo(){
    this.barcodeScanner.scan().then((barcodeData) => {
      //console.log('Barcode data', barcodeData);
      //this.chequero = barcodeData.text;
      // let filtro = [{campo: "codequipo",valor:barcodeData.text}];
      // this.dbsvc.getCatalogoFiltrado("equipos", filtro).then(data => {
        // console.log(data);
        let data = this.transportes.filter(item =>{
          return item.codequipo == this.trim(barcodeData.text);
        });
        if(data.length > 0){
          this.hayCambios = true;
          this["vehiculo"] = data[0];
          this["gondola"] = data[0];
          let pid = Object.keys(data[0])[0];
          this.envio["idcamion"] = data[0][pid];
          this.envio["idgondola"] = data[0][pid];
          this.envio.escaneado = true;
        }
      // });
    }).catch(err => {
      console.log('Error', err);
    });
  }

  getEquipos(){
    this.dbsvc.getCatalogoFiltrado("equipos").then(data => {
      
      this.equipos = data;
    });
    this.dbsvc.getCatalogoFiltrado("equipos", [{campo:"tipo", valor: 0}]).then(data => {
      
      this.transportes = data;
    });

    this.dbsvc.getEquiposGondola()
    .then(data =>{
      this.gondolas = data;
    })

    let tipocos = 5;
    if(this.appCoop != "mecanizada"){
      tipocos = 1;
    }
    this.dbsvc.getCatalogoFiltrado("equipos", [{campo:"tipo", valor: tipocos}]).then(data => {
      console.log(data);
      this["cosechadoras"] = data;
    });

    this.dbsvc.getCatalogoFiltrado("equipos", [{campo:"tipo", valor: 3}]).then(data => {
      console.log(data);
      this["tractores"] = data;
    });
    
  }

  trim(valor:string){
    return valor.replace(/^['0']+/g, '');
  }

  guardarEdicion(){
    if(this.construyeFechaCorte().toISOString() < this.fhquema){
      
        var msgValidacion = "¡La fecha ingresada es menor que la fecha en liberación!";
        this.alertCtrl.create({
          title: msgValidacion,
          buttons:['Aceptar']
        }).present();
    }
    else{
    this.dbsvc.modificarEnvio(this.envio)
    .then(()=>{
      this.hayCambios = false;
      this.rest.sendEnvios();
      this.alertCtrl.create({
        title: "Cambios guardados correctamente",
        buttons:['Aceptar']
      }).present();
      this.navCtrl.pop();
    });
  }
  }

  showDatePicker(componente:string, modo:string){
    
      this.datePicker.show({
        date: this.fecora.toISOString(),//componente == "fecha" ? this.fecha : this.hora,
        mode: modo,
        maxDate: new Date(),
        is24Hour: true,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT//THEME_DEVICE_DEFAULT_LIGHT
      }).then(
        date =>{ console.log('Got date: ', date.toISOString());
        this.fecora = date;
        this.hayCambios = true;
        if(componente == "fecha"){
          this.fecha = this.showDate(date.toISOString());
          //this.hora = this.showTime(this.fecha);
        }
        if(componente == "hora"){
          this.hora =this.showTime(date.toISOString());
          
        }

        this.envio["fhcorte"] = this.fecha+" "+this.hora;
        
      },
        
        err => console.log('Error occurred while getting date: ', err)
      );
    
  }

  cargarFechaHoraCorte(fhora){
    // var fhora = this.envio["fhcorte"];
    var fec = fhora.split(' ')[0];
    var hor = fhora.split(' ')[1];
    this.fecha = fec;
    this.hora = hor;
    var dt = new Date();
    dt.setDate(fec.split('/')[0]);
    dt.setMonth( ((fec.split('/')[1]) * 1) - 1);
    dt.setDate(fec.split('/')[2]);

    dt.setHours(hor.split(':')[0]);
    dt.setMinutes(hor.split(':')[1]);
    this.fecora = dt;
  }

  showDate(ISOFormat: string){
    let date = new Date(ISOFormat);
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let dt = date.getDate();

    let sdt:string = ""+dt;
    let smonth:string = ""+month;
    if (dt < 10) {
      sdt = '0' + dt;
    }
    if (month < 10) {
      smonth = '0' + month;
    }

    //console.log(sdt+'/' + smonth + '/'+year);
    return sdt+'/' + smonth + '/'+year;
  }

  showTime(ISOFormat: string){
    let date = new Date(ISOFormat);
    let hh = date.getHours();
    let mm = date.getMinutes();
    let shh =""+hh;
    let smm = ""+mm;

    if (hh < 10) {
      shh = '0' + hh;
    }
    
    if (mm < 10) {
      smm = '0' + mm;
    }

    //console.log(hh+":"+mm);
    return shh+":"+smm;
  }

  formatDate(){
    Date.prototype.toISOString = function() {
      var tzo = -this.getTimezoneOffset(),
          dif = tzo >= 0 ? '+' : '-',
          pad = function(num) {
              var norm = Math.floor(Math.abs(num));
              return (norm < 10 ? '0' : '') + norm;
          };
      return this.getFullYear() +
          '-' + pad(this.getMonth() + 1) +
          '-' + pad(this.getDate()) +
          'T' + pad(this.getHours()) +
          ':' + pad(this.getMinutes()) +
          ':' + pad(this.getSeconds()) +
          dif + pad(tzo / 60) +
          ':' + pad(tzo % 60);
  }
  }

  construyeFechaCorte(){
    var dtRet = new Date();
    if(this.fecha!=null){
      var dia, mes, anio;
      dia =this.fecha.split('/')[0];
      mes =this.fecha.split('/')[1];
      anio =this.fecha.split('/')[2];
      dtRet.setDate(dia);
      dtRet.setMonth(mes-1);
      dtRet.setFullYear(anio);
    }
    if(this.hora!=null){
      var hora, min;
      hora = this.hora.split(":")[0];
      min = this.hora.split(":")[1];

      dtRet.setHours(hora);
      dtRet.setMinutes(min);
      dtRet.setSeconds(0);
    }

    return dtRet;
  }

}
