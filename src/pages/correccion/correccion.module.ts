import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CorreccionPage } from './correccion';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    CorreccionPage,
  ],
  imports: [
    IonicPageModule.forChild(CorreccionPage),
    SelectSearchableModule
  ],
})
export class CorreccionPageModule {}
