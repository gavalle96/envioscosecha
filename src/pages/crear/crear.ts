import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs, AlertController, Events, LoadingController } from 'ionic-angular';
import { EnviosPage } from '../envios/envios';
import { MaquinariaPage } from '../maquinaria/maquinaria';
import { TripulacionPage } from '../tripulacion/tripulacion';
import { LiberacionPage } from '../liberacion/liberacion';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { Storage } from '@ionic/storage';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { RozadoresPage } from '../rozadores/rozadores';
import { DatabaseProvider } from '../../providers/database/database';
/**
 * Generated class for the CrearPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({ priority: 'high', segment: 'tabs' })
@Component({
  selector: 'page-crear',
  templateUrl: 'crear.html'
})
export class CrearPage {
  loaded:   boolean = false;
  tabIndex: number  = 0;


  @ViewChild('myTabs') tabRef: Tabs;
  Envios = EnviosPage;
  Tripulacion = TripulacionPage;
  Maquinaria = MaquinariaPage;
  Liberacion = LiberacionPage;
  Rozadores = RozadoresPage;
  loading: any;
  appCoop="mecanizada";
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private nativePageTransitions: NativePageTransitions
    , public storage: Storage
    , public alertCtrl: AlertController
    , private loadingCtrl: LoadingController
    , private event: Events
    , private diagnostic: Diagnostic
    , private locationAccuracy: LocationAccuracy
    , private dbsvc: DatabaseProvider
    ) {
      this.storage.get("aplicativo").then(val => {
        if(val != null && val != "" && val != undefined){
          this.appCoop = val;
        }
      });
      
      this.event.subscribe("cambioapp", ()=>{
        this.limpiarDatos();
      })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CrearPage');
    //this.tabRef.select(0);
    this.storage.get('tabActiva').then((val) => {
      
      this.tabRef.select(val);
      this.tabIndex = val;
    });
   }

   ionViewCanEnter(){
     return new Promise((resolve) =>{
      this.gpsHabilitado().then(enabled =>{
        console.log(enabled);
        
        if(!enabled){
          this.enableLocation();
          }

        resolve(enabled);
      });
     }); 
     
    }

    gpsHabilitado(){
      return this.diagnostic.isGpsLocationEnabled().then(isEnabled => {
        return Promise.resolve(isEnabled);
      }).catch(err=>{
        console.log(err);
        return Promise.resolve(false);
      });
    }
enableLocation()
{
this.locationAccuracy.canRequest().then((canRequest: boolean) => {

if(canRequest) {
// the accuracy option will be ignored by iOS
this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
() => {
  console.log('Request successful');

},
error => console.log('Error requesting location permissions'+JSON.stringify(error))
);
}

});
}
   presentLoadingDefault() {
    
    this.loading = this.loadingCtrl.create({
      content: 'Espere por favor...'
    });
  
    this.loading.present();
  
    
  }
  limpiarDatos(){
    this.storage.set('tabActiva',0);
    this.storage.set('libActual','');


    //TRIPULACION
    this.storage.remove('chequero');
    this.storage.remove('opcos');
    this.storage.remove('tr1');
    this.storage.remove('tr2');
    this.storage.remove('tr3');
    this.storage.remove('supAlce');
    this.storage.remove('ayud1');
    this.storage.remove('ayud2');
    this.storage.remove('ayud3');
    this.storage.remove('ayud4');
    this.storage.remove('ayud5');
    this.storage.remove('ayud6');

    this.storage.remove('desp1');
    this.storage.remove('desp2');
    this.storage.remove('desp3');
    this.storage.remove('desp4');
    this.storage.remove('desp5');
    this.storage.remove('desp6');
    
    this.storage.remove('acom1');
    this.storage.remove('acom2');
    this.storage.remove('acom3');
    this.event.publish("finturno");

    //MAQUINARIA
    this.storage.remove('cosechadora');
    this.storage.remove('tractor1');
    this.storage.remove('tractor2');
    this.storage.remove('tractor3');
    this.event.publish("finturno");

    //LIBERACION
    this.storage.remove('idhacienda');
    this.storage.remove('idlote');
    this.storage.remove('libActual');
    this.storage.remove('bloque');

    
    let sql = "DELETE FROM temprozadores;";
    this.dbsvc.insertarCatalogosPorTransaccion([sql]);
    
    this.event.publish("finturno");
  }
  finTurno(){
    //LOGICA DE FIN DE TURNO
    const confirm = this.alertCtrl.create({
      title: '¿Finalizar Turno?',
      message: 'Esta acción limpiará todos los campos.',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.presentLoadingDefault();

            this.event.subscribe("finload",() =>{
              if(this.loading!= null && this.loading !== undefined)
              this.loading.dismiss();
              this.event.unsubscribe("finload");
            });

            this.tabRef.select(0);
            
           this.limpiarDatos();
          }
        }
      ]
    });
    confirm.present();
  }
    
  
  
  
  public transition(e):void {
    let options: NativeTransitionOptions = {
     direction:this.getAnimationDirection(e.index),
     duration: 100,
     slowdownfactor: -1,
     slidePixels: 0,
     iosdelay: 20,
     androiddelay: 0,
     fixedPixelsTop: 0,
     fixedPixelsBottom: 48
    };
  
    if (!this.loaded) {
      this.loaded = true;
      return;
    }
  
    this.nativePageTransitions.curl(options);
  }

  private getAnimationDirection(index):string {
    var currentIndex = this.tabIndex;
  
    this.tabIndex = index;
    this.storage.set('tabActiva',index);
    switch (true){
      case (currentIndex < index):
        return('left');
      case (currentIndex > index):
        return ('right');
    }
  }
}
