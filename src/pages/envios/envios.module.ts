import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EnviosPage } from './envios';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    EnviosPage,
  ],
  imports: [
    IonicPageModule.forChild(EnviosPage),
    SelectSearchableModule
  ],
})
export class EnviosPageModule {}
