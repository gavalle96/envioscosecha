import { Component, ViewChild, Injector } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { DatabaseProvider } from '../../providers/database/database';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { Content } from 'ionic-angular';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Subscription } from 'rxjs';
import { PrinterProvider } from '../../providers/printer/printer';
import { RestProvider } from '../../providers/rest/rest';
import { NFC, Ndef } from '@ionic-native/nfc';
import { DatePicker } from '@ionic-native/date-picker';
import { Media, MediaObject } from '@ionic-native/media';
/**
 * Generated class for the EnviosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-envios',
  templateUrl: 'envios.html',
})
export class EnviosPage {
  @ViewChild('pageTop') pageTop: Content;

  prompt: any;
  confirm : any;
  carga = 1;
  liberacion ="";
  envio = "";
  conductor ="";
  vehiculo ="";
  gondola ="";
  fecha ="";
  hora ="";
  barrida:boolean = false;
  doble:boolean = false;
  fechaini = "";
  camposValidos = true;
  idvehiculo;
  idconductor;
  loading: any;
  private trabajadores: any[] = [];
  private choferes: any[] = [];
  private equipos: any[] = [];
  private transportes: any[] = [];
  private gondolas: any[] = [];
  envioscarga = [];
  escaneado = false;

  readingTag:   boolean   = false;
  writingTag:   boolean   = false;
  isWriting:    boolean   = false;
  ndefMsg:      string    = '';
  subscriptions: Array<Subscription> = new Array<Subscription>();

  readingCamion: boolean = false;

  latitud:number;
  longitud:number;
  gettingLocation: boolean = false;
  subBluetooth: Subscription = new Subscription();
  subslocation: Subscription = new Subscription();

  appCoop = "mecanizada";

  confirmNFC: any;
  file: MediaObject;
  fileOK : MediaObject;
  playing = false;

  macAddress = "00:00:00:00:00:00";


  fhquema;
  manual = 2;
  tiempo = 2; //Tiempo en horas para utilizar un camion
  
  constructor(public navCtrl: NavController
  , public navParams: NavParams
  , public storage: Storage
  , private dbsvc: DatabaseProvider
  , private barcodeScanner: BarcodeScanner
  , private alertCtrl: AlertController
  , private geolocation: Geolocation
  , private diagnostic: Diagnostic
  , private locationAccuracy: LocationAccuracy
  , private loadingCtrl: LoadingController
  , private printerProvider: PrinterProvider
  , private restsvc: RestProvider
  
  , public nfc: NFC
  
  , public ndef: Ndef
  , private injector: Injector
  , private datePicker: DatePicker
  , private media: Media) {
    this.file = this.media.create('/android_asset/www/assets/audio/beep.mp3');//('assets/audio/threebeep.mp3');
    this.file.setVolume(1);
    this.file.onStatusUpdate.subscribe(status =>{
      console.log("etapa: "+status);
      if(status == this.media.MEDIA_STOPPED && this.playing){
        this.file.play();
      }

    } ); 

    this.fileOK = this.media.create('/android_asset/www/assets/audio/done.mp3');//('assets/audio/threebeep.mp3');
    this.fileOK.setVolume(1);
   
    this.dbsvc = injector.get(DatabaseProvider);
    
    let options = {
      frequency: 3000,
      enableHighAccuracy: true
    };
    this.getEquipos();
    this.getTrabajadores();

    this.formatDate();



    //Fecha y hora de corte del momento
    // this.storage.get("fechacorte").then(val =>{
    //   if(val==null){
      // let dt = new Date();
      // dt.setMinutes(dt.getMinutes() - 1);

      //   this.fecha = this.showDate(dt.toISOString());
      //   this.hora = this.showTime(dt.toISOString());// new Date().toISOString();
    //   }
    //   else{
    //     this.fecha = val;
    //     this.hora = val;
    //   }
    // });
    

    this.printerProvider.getMacAdress();

    this.storage.get("aplicativo").then(val => {
      if(val != null && val != "" && val != undefined){
        this.appCoop = val;
      }
    })
  }

  ionViewWillLeave() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
    this.subscriptions = [];
    console.log("VA A SALIR DE ENVIOS")
    this.storage.set("fechacorte",this.fecha);
    this.storage.set("horacorte", this.hora);
    // this.subBluetooth.unsubscribe();
    
  }

  validarAsignacion(contenido){
    this.readingCamion = false;
    this.writingTag = false;
    this.readingTag = false;

    let obj = JSON.parse(contenido);
    console.log(obj);
    if(obj != null)
    {

      /**
       * Se espera una estructura parecida a:
       * { 
       *   codconductor: "33483",
       *   codvehiculo: "72000000041",
       *   liberacion: "063413",
       *   hacienda: "3001 - EL DIAMANTE DE SAN MARCOS",
       *   lote: "01"
       * }
       * {"codconductor":"721","codvehiculo:"7200000067","liberacion":"036361"}
       */
      this.fechaini = new Date().toISOString();
      let codcond = obj.codconductor;
      let codv = obj.codvehiculo;
      let libe = obj.liberacion;
      let hda = obj.hacienda;
      let lote = obj.lote

      let trabs = this.trabajadores.filter(item =>{
        return item.codtrabajador == codcond;
      });
      if(trabs.length > 0){
        this.conductor = trabs[0];
        this.idconductor = trabs[0].idtrabajador;
      }

      let eqs = this.equipos.filter(item =>{
        return item.codequipo == codv;
      });
      if(eqs.length > 0){
          this.vehiculo = eqs[0];
          this.idvehiculo = eqs[0].idvehiculo;
          // this.restsvc.etapaCargando(codv);
      }


      if(libe != this.liberacion){
        let alert = this.alertCtrl.create({
          title: '¡Liberación no coincide!',
          subTitle: 'Liberación asignada al motorista no coincide.\n Liberación: '+libe+'\n Hacienda: '+hda+'\n Lote: '+lote,
          enableBackdropDismiss: false,
          buttons: [{
            text: 'Ok',
            handler: () => {
              alert.dismiss();
              return false;
            }
          }]
        });

        alert.present();
      }
    }
  }

  verificarLugar(lat, lng){
    return this.storage.get("idlote").then(val =>{
      let idlote = 0;
      if(val !== undefined && val !== null)
      idlote = val;

      console.log(idlote);
      return this.dbsvc.puntoEnPoligono({lat:lat, lng:lng},val)
    })
      .then(ok=>{
        if(!ok){
          let preventEdit =  this.alertCtrl.create({
            enableBackdropDismiss: false,
            title: "LIBERACIÓN NO COINCIDE",
            subTitle: "La ubicación no coincide con la ubicación de la liberación" ,
            buttons: [{
              text: 'OK',
              handler: () => {
                 preventEdit.dismiss().catch(er => console.log(er));
                 this.gettingLocation = false;

                 return false;
              }
            }]
          });
          
          preventEdit.present();
        }
        //#region COINCIDE UBICACION
        // else{
          
        //   let edit =  this.alertCtrl.create({
        //     enableBackdropDismiss: false,
        //     title: "LIBERACIÓN SI COINCIDE UBICACIÓN",
        //     subTitle: "La ubicación SI coincide con la ubicación de la liberación" ,
        //     buttons: [{
        //       text: 'OK',
        //       handler: () => {
        //          edit.dismiss().catch(er => console.log(er));
                 
        //           this.gettingLocation = false;
        //          return false;
        //       }
        //     }]
        //   });
          
        //   edit.present();
        // }
        //#endregion
        return true;//ok;
      })
      .catch(err => {console.log(err);
        return false;
      });
  }
  alertLoc(lat, lng){
    let msg = this.alertCtrl.create({
      title: "POSICIÓN CAMBIÓ",
      subTitle: "Nuevas coordenadas: "+lat+", "+lng ,
      buttons: [{
        text: 'Ok',
        handler: () => {
           msg.dismiss().catch(er => console.log(er));
           return false
        }
      }]
    });
    
    msg.present();

  }
  presentLoadingDefault() {
    
    this.loading = this.loadingCtrl.create({
      content: 'Guardando...'
    });
  
    this.loading.present();
  
    
  }
  gpsDisponible(){
    return new Promise((resolve) =>{
      this.gpsHabilitado().then(enabled =>{
        //console.log(enabled);
        
        if(!enabled){
          this.enableLocation();
          }

        resolve(enabled);
      });
     });
  }
  gpsHabilitado(){
    return new Promise((resolve)=> {
      this.diagnostic.isGpsLocationEnabled().then(isEnabled => {
      
        return resolve(isEnabled);
      }).catch(err=>{
        console.log(err);
        return resolve(false);
      });
    });
    
  }
    enableLocation()
    {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {

    if(canRequest) {
    // the accuracy option will be ignored by iOS
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
    () => console.log('Request successful'),
    error => console.log('Error requesting location permissions'+JSON.stringify(error))
    );
    }

    });
    }

  formatDate(){
    Date.prototype.toISOString = function() {
      var tzo = -this.getTimezoneOffset(),
          dif = tzo >= 0 ? '+' : '-',
          pad = function(num) {
              var norm = Math.floor(Math.abs(num));
              return (norm < 10 ? '0' : '') + norm;
          };
      return this.getFullYear() +
          '-' + pad(this.getMonth() + 1) +
          '-' + pad(this.getDate()) +
          'T' + pad(this.getHours()) +
          ':' + pad(this.getMinutes()) +
          ':' + pad(this.getSeconds()) +
          dif + pad(tzo / 60) +
          ':' + pad(tzo % 60);
  }
  }

  selectChange(event: {
    component: SelectSearchableComponent,
    value: any 
    }){

      this.fechaini = new Date().toISOString();
      switch (event.component._labelText) {
        case "Conductor":
          this.idconductor = event.value.idtrabajador;
          break;
      case "Vehiculo":
          this.idvehiculo = event.value.idequipo;
          this.gondola = event.value;
          this.escaneado = false;
          var dt = new Date();
          dt.setHours(dt.getHours() - this.tiempo);

          this.dbsvc.consultarCamionEnEnviosRecientes(this.idvehiculo,dt.toISOString())
          .then(resp=>{
            if(resp!=null){
              if(resp.length > 0){
                this.vehiculo = "";
                this.gondola = "";
                
                this.alertCtrl.create({
                  title:"CAMION USADO HACE MENOS DE "+this.tiempo+" HORAS", 
                  
                  message:"Puede editar el envío en 'Consultar Envíos Emitidos'",
                  buttons:[
                    {
                      text: "Aceptar"
                    }
                  ]
                }).present();
              }
            }
          })

          //this.restsvc.etapaCargando(event.value.codequipo);
          break;
      
        default:
          break;
      }
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EnviosPage');
    
  }

  ionViewDidEnter(){
   // console.log('tab activo');
   this.storage.get("fhquema")
   .then(fhq =>{
     this.fhquema = fhq;
   })

   this.storage.get("tiempo")
   .then(val =>{
     if(val!=null && val != undefined)
     this.tiempo = val;
   })

   //#region MANEJO DE NFC

   let subs = this.subscriptions.push(this.nfc.addNdefListener()
    .subscribe(data => {
  if (this.readingTag) {
    let payload = data.tag.ndefMessage[0].payload;
    let tagContent = this.nfc.bytesToString(payload).substring(3);
    console.log("Tag Content",tagContent);
    this.readingTag = false;
    
    if(tagContent == this.ndefMsg){
      this.confirmNFC.dismiss();
      let alert = this.alertCtrl.create({
        title: '¡Grabado Correcto!',
        subTitle: 'Envío grabado correctamente',
        enableBackdropDismiss: false,
        buttons: [ {
          text: 'Aceptar',
          handler: data => {
            
            alert.dismiss();
            if(this.macAddress != null && this.macAddress != undefined && this.macAddress != "" && this.macAddress != "00:00:00:00:00:00"){
              this.imprimirEnvio();
            }
            else{
              this.envioscarga = [];
            }
            return false;
          }
        }]
      });

      alert.present();
      this.fileOK.play();
    }
    
    if(this.readingCamion){
      this.validarAsignacion(tagContent);
    }
  } 
  else if (this.writingTag) {
    if (!this.isWriting) {
      this.isWriting = true;
      let value = this.ndef.textRecord(this.ndefMsg);
      console.log("ndefMsg: ",this.ndefMsg);
      this.nfc.write([value])
        .then(() => {
          this.writingTag = false;
          this.isWriting = false;
          console.log("written");
          this.readingTag = true;

          this.confirmNFC.dismiss();
         // this.restsvc.sendLog(this.envio)
         this.playing = false;
         this.file.stop();

          this.confirmNFC = this.alertCtrl.create({
            title: 'Tarjeta grabada',
            subTitle: 'ACERCAR TARJETA DE NUEVO',
            enableBackdropDismiss: false,
            buttons: [ {
              text: 'Cancelar',
              handler: data => {
                
                this.confirmNFC.dismiss();
                this.readingTag = false;
                console.log(this.macAddress);
                if(this.macAddress != null && this.macAddress != undefined && this.macAddress != "" && this.macAddress != "00:00:00:00:00:00"){
                  this.imprimirEnvio();
                }
                else{
                  this.envioscarga = [];
                }
                return false;
              }
            }]
          });
          this.confirmNFC.present()

        })
        .catch(err => {
          this.writingTag = false;
          this.isWriting = false;
          this.confirmNFC.dismiss();
          console.log("Error en escritura")
          console.log(err);
          this.playing = false;
          this.file.stop();
          let alert = this.alertCtrl.create({
            title: 'Error en grabado',
            subTitle: 'Intentelo de nuevo',
            enableBackdropDismiss: false,
            buttons: [ {
              text: 'Aceptar',
              handler: data => {
                this.grabarNFC();
                alert.dismiss();
                return false;
              }
            }]
          });
          alert.present()
        });
    }
  }
},
err => {

})
);
console.log("SUBSCRIPCIONES NFC: "+subs);
//#endregion

    this.storage.get('libActual').then(val => 
      {
        console.log("LIBERACION EN ENVIOS")
        console.log(val +" - "+this.liberacion)

        this.storage.get("horacorte")
        .then(hora =>{ this.hora = hora
     
          return this.storage.get("fechacorte");
        })
        .then(fecha => {
          if(this.appCoop == "mecanizada"){
            let dt = new Date();
              dt.setMinutes(dt.getMinutes() - 1);
    
              this.fecha = this.showDate(dt.toISOString());
              this.hora = this.showTime(dt.toISOString());
          }
          if(this.appCoop != "mecanizada")
          if(fecha == null  || fecha == undefined){
            if(val != this.liberacion){
              // let dt = new Date();
              // dt.setMinutes(dt.getMinutes() - 1);
    
              this.fecha = null//; this.showDate(dt.toISOString());
              this.hora = null;//this.showTime(dt.toISOString());

              
            }
          }
          else{
            //this.showDate(fecha);
            if(val != this.liberacion){
              this.fecha= null;
              this.hora = null;
            }
            // else{
            //   this.fecha = fecha;
            //   this.storage.get("horacorte")
            //   .then(hcort =>{
            //     this.hora = this.hora;
            //   })
            // }
          }
          this.liberacion = val;
          return Promise.resolve();
        })
        // .then(hora => {
          
        //   if(this.appCoop != "mecanizada")
        //   if(hora != null  && hora != undefined){
        //     this.hora = hora;//this.showTime(hora);
        //   }
        // });

        
        
        return this.storage.get("ultEnvio");
      })
      .then(val =>{
        this.envio = (parseInt(val,10)+1)+"";
        if(this.appCoop == "mecanizada"){
          let dt = new Date();
          dt.setMinutes(dt.getMinutes() - 1);
  
            this.fecha = this.showDate(dt.toISOString());
              this.hora = this.showTime(dt.toISOString());
          }
      });

      //FECHA DE CORTE DEL MOMENTO
      // this.storage.get("fechacorte").then(val =>{
      //   if(val==null){
        
      //   }
      //   else{
      //     this.fecha = val;
      //     this.hora = val;
      //   }
      // });

  }


  getTrabajadores(){
    //obtiene los choferes
    let filtro = [{campo:"profesion", valor: "20000106' OR profesion = '20000225"}];
    this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
      // console.log(data);
      this.choferes = data;
    });
    this.dbsvc.getCatalogo("trabajadores").then(data =>{
      this.trabajadores = data;
    });
  }

  getEquipos(){
    this.dbsvc.getCatalogoFiltrado("equipos").then(data => {
      
      this.equipos = data;
    });
    this.dbsvc.getCatalogoFiltrado("equipos", [{campo:"tipo", valor: 0}]).then(data => {
      
      this.transportes = data;
    });

    this.dbsvc.getEquiposGondola()
    .then(data =>{
      this.gondolas = data;
    })
    
  }

  barVehiculo(){
    this.barcodeScanner.scan().then((barcodeData) => {
      //console.log('Barcode data', barcodeData);
      //this.chequero = barcodeData.text;
      // let filtro = [{campo: "codequipo",valor:barcodeData.text}];
      // this.dbsvc.getCatalogoFiltrado("equipos", filtro).then(data => {
        // console.log(data);
        let data = this.transportes.filter(item =>{
          return item.codequipo == this.trim(barcodeData.text);
        });
        if(data.length > 0){
          this.vehiculo = data[0];
          this.gondola = data[0];
          this.storage.set("vehiculo",barcodeData.text)
          this.restsvc.etapaCargando(barcodeData.text);
          this.idvehiculo = data[0].idequipo;
          this.escaneado = true;
        }
      // });
    }).catch(err => {
      console.log('Error', err);
    });
  }
  trim(valor:string){
    return valor.replace(/^['0']+/g, '');
  }
  barConductor(){
    this.barcodeScanner.scan().then((barcodeData) => {
      //console.log('Barcode data', barcodeData);
      //this.chequero = barcodeData.text;
      // let filtro = [{campo: "codtrabajador",valor:barcodeData.text}];
      // this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
      
      let data = this.choferes.filter(item =>
        {
          return item.codtrabajador == this.trim(barcodeData.text);
        });
        if(data.length > 0){
          this.conductor = data[0];
          this.storage.set("conductor",barcodeData.text)
          this.fechaini = new Date().toISOString();
          this.idconductor = data[0].idtrabajador;
        }
      // });
    }).catch(err => {
      console.log('Error', err);
    });
  }

  leerNFC() {
    let alert = this.alertCtrl.create({
      title: 'Leer Asignación',
      subTitle: 'Acerque la tarjeta NFC al teléfono',
      enableBackdropDismiss: false,
      buttons: [{text:"Cancelar",
    handler:()=>{
      alert.dismiss();
      return false;
    }},{
        text: 'Aceptar',
        handler: () => {
          alert.dismiss();
          this.readingCamion = true;
          this.readingTag = true;
          return false;
        }
      }]
    });
    
    alert.present();
  }

  swipe(event) {
    
    if(event.direction === 4) {
      this.navCtrl.parent.select(2);
    }
  }

  cambiarLibe(){
    this.navCtrl.parent.select(2);
    
  }

  getUbicacion(){
    this.gettingLocation = true;
    // if(this.latitud == null  || this.longitud == null){
    //   this.loading = this.loadingCtrl.create({
    //     content: 'Obteniendo Ubicación...'
    //   });
    
    //   this.loading.present();
    // }


    if(this.gpsDisponible()){
      this.geolocation.getCurrentPosition().then((resp) => {
        //Al obtener la posición, se guardan los datos
        this.camposValidos = true;

        if(this.latitud == null  || this.longitud == null){
          if(this.loading !== undefined)
          this.loading.dismiss();
        }
  
        this.latitud = resp.coords.latitude;
        this.longitud = resp.coords.longitude;
        
        console.log("Obtiene ubicación cada 10 segundos lat: "+this.latitud+" lng: "+this.longitud);
        
      })
    }
    else{
      //this.loading.dismiss();
    }
  }
  guardarCarga1(){

  }
  guardarNuevo(){
    //console.log(this.gpsHabilitado());
    
    //VERIFICAR QUE EL NUMERO DE ENVIO ESTÉ DENTRO DEL RANGO DEL TELÉFONO
    let min = 0;
    let max = 0;
    this.storage.get("envioMin").then(val =>{
      min = val;
      return this.storage.get("envioMax");
    }).then(val =>{
      max = val;
      let env = parseInt(this.envio);
      if(env>=min && env <= max){
        if(this.gpsDisponible()){
          this.presentLoadingDefault();

          //VERIFICAR ANTES SI ESTÁ EN LA UBICACIÓN ADECUADA
          this.storage.get("latitud")
          .then( data =>{ 
            this.latitud = data;

            return this.storage.get("longitud");
          })
          .then(data =>{
            this.longitud = data;

              console.log("lat: "+this.latitud+", lon: "+this.longitud);
              return this.verificarLugar(this.latitud, this.longitud);
          })
          .then(data => {
            if(data ==true){
              this.guardarEnvio().then(()=>{
                console.log("Envio guardado, carga: "+this.carga);
                if(this.carga == 2 || !this.doble){
                  this.limpiarCampos();
                  
                }
                else{
                  this.gondola = null;
                  this.incrementEnvio();
                  if(this.appCoop == "mecanizada"){
                    let dt = new Date();
                    dt.setMinutes(dt.getMinutes() - 1);
              
                    this.fecha = this.showDate(dt.toISOString());
                    this.hora = this.showTime(dt.toISOString());//new Date().toISOString();
                  }
                }
              }).catch(err => {
                console.log("Error de GuardarNuevo")
                console.log(err);
                
              });
            }

            this.loading.dismiss();
          });
          

          
        }
      }
      else{
        //NO ESTA DENTRO DEL RANGO, LLAMAR A TI
        let msg = this.alertCtrl.create({
          title: "FUERA DE RANGO",
          subTitle: "Envío fuera de rango configurado. Llame a Informática.",
          buttons: [{
            text: 'Ok',
            handler: () => {
               msg.dismiss();
            }
          }]
        });
        
        msg.present();
      }
    })

    
    
  }
  incrementEnvio(){
    //aumenta el numero de envio
    this.storage.get("ultEnvio").then(val =>{
      this.envio = (parseInt(val,10)+1)+"";
    });
  }
  limpiarCampos(){
    
    this.incrementEnvio();
    // this.envioscarga = [];
    //limpia conductor y vehiculo
    this.conductor = "";
    this.vehiculo = "";
    this.gondola = null;

    //resetea fecha y hora de corte
    if(this.appCoop == "mecanizada"){
      let dt = new Date();
      dt.setMinutes(dt.getMinutes() - 1);

      this.fecha = this.showDate(dt.toISOString());
      this.hora = this.showTime(dt.toISOString());//new Date().toISOString();
    }

    
    
    this.pageTop.scrollToTop()
    
  }



  guardarEnvio(){
    //console.log("barrida");
    //console.log(this.barrida);

    var msgValidacion ="";
    let entidad:any = {};
    this.camposValidos = true;

    let tractoristas = 0;
    let tractores = 0;
        
  
      entidad['lat'] = this.latitud;
      entidad['long'] = this.longitud;
      console.log("resuelve lote")
      return this.storage.get("idlote")
  .then(data=>{
    entidad['idlote'] = data;

    console.log("Resuelve libactual");
    return this.storage.get("libActual");
  })
    .then(val => {
      console.log("ValorLibActual: "+val);
      console.log("Valor de libActual");
      console.log(val);
        entidad['codliberacion'] = val;
        
        if(val == null){
          this.camposValidos = false;
          msgValidacion = "¡No ha seleccionado liberación!";
          this.cambiarLibe();
          console.log(msgValidacion)
          return Promise.reject(msgValidacion)
        }

        return this.dbsvc.getCatalogoFiltrado("liberaciones",[{campo:"codliberacion",valor:entidad['codliberacion']}]);
      })
      .then(val=>{
        if(val!=null){
          entidad['idordencosecha'] = val[0]["idordencosecha"];
          this.manual = val[0]["manual"];
          console.log("MANUAL?, ",this.manual)
        }
        
        //BLOQUE
        return this.storage.get("bloque");
      }).then((val) =>{

        entidad['idbloque'] = val;
          if(val == null){
            msgValidacion = "¡No ha seleccionado el Bloque de Cosecha!";
            //console.log(msgValidacion)
            this.camposValidos = false;
            this.navCtrl.parent.select(0);
            return Promise.reject(msgValidacion)
          }

        //CAMION
       entidad['idcamion'] = this.idvehiculo;
        
        if(this.vehiculo == null || this.vehiculo ==""){
          this.camposValidos = false;
          msgValidacion = "¡No ha seleccionado Vehículo!";
          return Promise.reject(msgValidacion)
        }
        this.dbsvc.getCatalogoFiltrado("equipos",[{campo:"idequipo",valor:this.idvehiculo}])
        .then(data =>{
          if(data!=null && data != undefined){
            if(data.length > 0){
              this.restsvc.etapaCargadoProp(data[0].codequipo);
            }
          }
        })
        
        //CONDUCTOR
       entidad['idconductor'] = this.idconductor;
        if(this.conductor == null || this.conductor ==""){
          this.camposValidos = false;
          msgValidacion = "¡No ha seleccionado Conductor!";
          return Promise.reject(msgValidacion)
        }
        
        //FECHA Y HORA DE CORTE
        if(this.fecha == null){
          this.camposValidos = false;
          msgValidacion = "¡FALTA FECHA DE CORTE!";
          return Promise.reject(msgValidacion)
        }

        if(this.hora == null){
          this.camposValidos = false;
          msgValidacion = "¡FALTA HORA DE CORTE!";
          return Promise.reject(msgValidacion)
        }
        console.log("FechaQuema: ",this.fhquema);
        console.log("FCorte: ",this.construyeFechaCorte().toISOString());
        if(this.construyeFechaCorte().toISOString() < this.fhquema){
          this.camposValidos = false;
            msgValidacion = "¡La fecha ingresada es menor que la fecha en liberación!";
          
          
          return Promise.reject(msgValidacion)
        }

        if(this.construyeFechaCorte().toISOString() > new Date().toISOString()){
          this.camposValidos = false;
            msgValidacion = "¡La fecha no puede ser en el futuro!";
          
          
          return Promise.reject(msgValidacion)
        }

        //COSECHADORA
        return this.storage.get("cosechadora");

      }).then(val =>{
          
         entidad['idcosechadora'] = this.getIdEquipo(val);
          
        if(val == null && this.appCoop != "picada"){
          this.camposValidos = false;
          if(this.appCoop =="manual" || this.appCoop =="coop")
          {
            msgValidacion = "¡No ha seleccionado Alzadora!";
          }
          else if(this.appCoop=="mecanizada"){
            msgValidacion = "¡No ha seleccionado Cosechadora!";
          }
          this.navCtrl.parent.select(1);
          return Promise.reject(msgValidacion)
        }
        
        //CHOFER
        return this.storage.get("opcos")
      }).then(val =>{
         entidad['idchofer'] = this.getIdTrabajador(val);
          if(val == null && this.appCoop != "picada"){
            this.camposValidos = false;
            if(this.appCoop =="manual" || this.appCoop =="coop")
            {
              msgValidacion = "¡No ha seleccionado operador de Alzadora!";
            }
            else if(this.appCoop=="mecanizada"){
              msgValidacion = "¡No ha seleccionado operador de Cosechadora!";
            }
            this.navCtrl.parent.select(1);
          return Promise.reject(msgValidacion)
          }

        // CHEQUERO
        return this.storage.get("chequero");
        }).then(val =>{
         entidad['idchequero'] = this.getIdTrabajador(val);
          if(val == null){
            this.camposValidos = false;
            msgValidacion = "¡No ha seleccionado Chequero!";
            this.navCtrl.parent.select(0);
          return Promise.reject(msgValidacion)
          }

      //   // SUPERVISOR DE ALCE
       return this.storage.get("supAlce")
       }).then(val =>{
         if(val == null || val == undefined){
           val = '9990014';
         }
          entidad['idsupalce'] = this.getIdTrabajador(val);
          if(entidad.idsupalce == null || entidad.idsupalce == 'null'){
            this.dbsvc.getCatalogoFiltrado("trabajadores",[{campo:"codtrabajador",valor:"9990014"}])
            .then(data =>{
              if(data!= null && data!=undefined){
                if(data.length > 0){
                  entidad.idsupalce = data[0].idtrabajador;
                }
              }
            })
          }

        // AYUDANTE 1 Y 2
       return this.storage.get("ayud1")
        }).then(val =>{
         entidad['ayud1'] = this.getIdTrabajador(val);
        
        return this.storage.get("ayud2");
        }).then(val =>{
         entidad['ayud2'] = this.getIdTrabajador(val);
         return this.storage.get("ayud3");
        }).then(val =>{
         entidad['ayud3'] = this.getIdTrabajador(val);
         return this.storage.get("ayud4");
        }).then(val =>{
         entidad['ayud4'] = this.getIdTrabajador(val);

         return this.storage.get("ayud5");
        }).then(val =>{
         entidad['ayud5'] = this.getIdTrabajador(val);

         return this.storage.get("ayud6");
        }).then(val =>{
         entidad['ayud6'] = this.getIdTrabajador(val);

         //DESPUNTADORES
         return this.storage.get("desp1");
        }).then(val =>{
         entidad['desp1'] = this.getIdTrabajador(val);

         return this.storage.get("desp2");
        }).then(val =>{
         entidad['desp2'] = this.getIdTrabajador(val);

         return this.storage.get("desp3");
        }).then(val =>{
         entidad['desp3'] = this.getIdTrabajador(val);

         return this.storage.get("desp4");
        }).then(val =>{
         entidad['desp4'] = this.getIdTrabajador(val);


         return this.storage.get("desp5");
        }).then(val =>{
         entidad['desp5'] = this.getIdTrabajador(val);

        // ACOMODADOR 1, 2 Y 3
        return this.storage.get("acom1")
        }).then(val =>{
         entidad['acom1'] = this.getIdTrabajador(val);
        return this.storage.get("acom2")
        }).then(val =>{
         entidad['acom2'] = this.getIdTrabajador(val);
        // return this.storage.get("acom3");
        // }).then(val =>{
        //  entidad['acom3'] = this.getIdTrabajador(val);

        // NUMERO DE ENVIO
       entidad['envio'] = this.envio;

        // FECHA DE CORTE
        //console.log(this.hora);
       entidad['fhcorte'] = this.fecha+" "+this.hora; //hora ya contiene la fecha y hora
       
        // TRACTOR 1, 2 Y 3
      return this.storage.get("tractor1");
        })
      .then(val =>{
         entidad['tractor1'] = this.getIdEquipo(val);
          if(val == null && this.appCoop=="mecanizada"){
            this.camposValidos = false;
            msgValidacion = "¡No ha seleccionado Tractor (1)!";
            this.navCtrl.parent.select(1);
            return Promise.reject(msgValidacion)
          }
          tractores++;
      return this.storage.get("tractor2");
        }).then(val =>{
          let id = this.getIdEquipo(val);
        if(id>0) tractores++;
         entidad['tractor2'] = id;
        
      return this.storage.get("tractor3")})
      .then(val =>{
        let id = this.getIdEquipo(val);
        if(id>0) tractores++;
         entidad['tractor3'] = id;
        // TRACTORISTA 1, 2 Y 3
      return this.storage.get("tr1")
      }).then(val =>{
         entidad['tractorista1'] = this.getIdTrabajador(val);
         if(val == null && this.appCoop=="mecanizada"){
          this.camposValidos = false;
          msgValidacion = "¡No ha seleccionado tractorista (1)!";
          this.navCtrl.parent.select(0);
          return Promise.reject(msgValidacion)
        }

      tractoristas++;

      return this.storage.get("tr2")
      }).then(val =>{
        let id = this.getIdTrabajador(val);
        if(id>0) tractoristas++;
         entidad['tractorista2'] =id;
        
      return this.storage.get("tr3");
      }).then(val =>{
        let id = this.getIdTrabajador(val);
        if(id>0) tractoristas++;
         entidad['tractorista3'] =id;
        // FECHA INI - FECHA FIN
        return  this.dbsvc.getRozadoresTemp();
      }).then(rozt=>{

        //ROZADORES OBLIGATORIOS PARA COSECHA MANUAL
        if(this.appCoop == "manual" && rozt.length == 0){
          this.camposValidos = false;
          msgValidacion = "¡NO HA INGRESADO ROZADORES!";
          return Promise.reject(msgValidacion);
        }

        //VERIFICAR TRACTORES Y TRACTORISTAS

        if(tractores != tractoristas){
          this.camposValidos = false;
          msgValidacion = "¡Número de tractores diferente a número de tractoristas!";
          if(tractores > tractoristas)
            this.navCtrl.parent.select(0);
          else if(tractores < tractoristas)
            this.navCtrl.parent.select(1);
            
          return Promise.reject(msgValidacion)
        }

        //console.log(this.camposValidos);
        entidad['barrida'] = this.barrida;
        this.barrida = false;
        entidad['fechaini'] = this.fechaini;
       entidad['fechafin'] = new Date().toISOString();
       entidad['tipoenvio'] = this.appCoop;
       entidad["enviorel"] = null;
        //si es la segunda carga, colocar el envio relacionado (envio de la carga 1)
       if(this.carga == 2){
         entidad["enviorel"] = this.envioscarga[0]["envio"];
       }

       if(this.gondola == null){
         this.camposValidos = false;
         msgValidacion = "Seleccione la góndola de la CARGA "+this.carga;
       }
       entidad['idgondola'] = this.gondola['idequipo'] == undefined || this.gondola['idequipo'] == null ? 0 : this.gondola['idequipo'];
        console.log(entidad);

        let nrocopias = 1;

        console.log("Fin de cadena, campos validos: "+this.camposValidos);
        if(this.camposValidos){
          entidad["escaneado"] = this.escaneado ? 1 : 0;
          
        return this.dbsvc.EnvioCreate(entidad).then(()=>{
          console.log("Ejecutó guardado");
          if(this.appCoop=="manual"){
            return this.dbsvc.getRozadoresTemp()
            .then(data =>{
              let sqlb = [];
              let sql = "";

              console.log("ROZADORES TEMP");
              console.log(data);

              data.forEach(item=>{
                sql = "INSERT INTO rozadores(envio, idtrabajador, unadas) values('"+this.envio+"',"+item.idtrabajador+","+item.unadas+")";
                sqlb.push(sql);
              });
              console.log(sqlb);
              return this.dbsvc.insertarCatalogosPorTransaccion(sqlb)
              .then(()=>{
                return this.dbsvc.limpiarRozTemp();
              });
            })
          }
          else{
            return Promise.resolve();
          }
        }).then(()=>{
          // if(this.doble){
          //   entidad['envio'] = (parseInt(this.envio,10)+1);
          //   this.dbsvc.EnvioCreate(entidad);
          //   this.storage.set("ultEnvio",(parseInt(this.envio,10)+1));
          //   entidad['envio'] = (parseInt(this.envio,10));
          // }            
          // else
            this.storage.set("ultEnvio",this.envio);
          console.log("Envío insertado");
          
          let alrtcarga1 = this.alertCtrl.create({
            title: 'Envío Guardado: carga '+this.carga,
            buttons: [{text: "Aceptar", role: "cancel"}]
          });
          this.storage.get("macprinter")
          .then(val => {
            this.macAddress = val;
            let env;
            this.dbsvc.getSingleEnvio(entidad['envio'])
            .then((data) => {
              data[0]["carga"] = this.carga;
              env = data[0];
              if(this.appCoop == "manual"){
                return this.dbsvc.getRozadoresEnvio(env.envio);
              }
              else{
                return Promise.resolve([]);
              }
            })
            .then(rozadores=>{
              env["rozadores"] = this.appCoop == "manual" ? rozadores : [];
              this.envioscarga.push(env);
            console.log("Carga: "+this.carga);
            console.log(this.envioscarga);
            if(this.carga == 2 || !this.doble){
              // if(val != null && val != undefined && val != "" && val != "00:00:00:00:00:00"){
              //   this.imprimirEnvio();
              // }
              // else{
                this.grabarNFC();
             // }
             
            
             this.restsvc.sendEnvios();
            
            }
            else if(this.doble && this.carga == 1){
              alrtcarga1.present();
              
            }
            if(this.doble){
              
              this.carga = this.carga == 1 ? 2 : 1;
            }
          });
          })
        
          
          

/*
          this.dbsvc.getCatalogo("envios").then(data =>{
            console.log(data);
          });*/
        })
        }
      }).catch((error) => {
       console.log('Error en validación de datos: ', error);
       let titulo = "Campos incompletos";
       if(this.camposValidos){
         titulo = "Error al guardar";
       }
       let alert = this.alertCtrl.create({
        title: titulo,
        subTitle: msgValidacion,
        buttons: [{
          text: 'Ok',
          handler: () => {
             alert.dismiss();
            
            return false;
          }
        }]
      });
      
      alert.present();
      return Promise.reject(error);
     });
  }

  grabarNFC(){
    this.playing = true;
    this.file.play();

    //#region nfc
    this.ndefMsg = "";
    let variasCargas = this.envioscarga.length > 1;
    this.envioscarga.forEach(item => {
      if(variasCargas)
        this.ndefMsg += //"<CARGA"+item.carga+">"+
                          this.printerProvider.getCadena2D(item,1,item.rozadores, this.manual)+"&";
                          //"</CARGA"+item.carga+">";
      else{
        this.ndefMsg += this.printerProvider.getCadena2D(item, item.carga, item.rozadores, this.manual);
      }
    });
    if(this.ndefMsg.lastIndexOf("&") == (this.ndefMsg.length-1)){
      this.ndefMsg = this.ndefMsg.substr(0,this.ndefMsg.length-1);
    }
    //this.envioscarga = [];
    this.carga = 1;
    this.doble = false;
    this.writingTag = true;
    
    //#endregion

    this.confirmNFC = this.alertCtrl.create({
      title: 'ACERQUE LA TARJETA',
      message: "Acerque la tarjeta para grabar el envío",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
            
            // this.envioscarga = [];
            this.carga = 1;
            this.doble = false;
            //cancela el grabado
            this.writingTag = false;
            this.playing = false;
            this.file.stop();
            this.confirmNFC.dismiss();
            console.log(this.macAddress);
            if(this.macAddress != null && this.macAddress != undefined && this.macAddress != "" && this.macAddress != "00:00:00:00:00:00"){
                this.imprimirEnvio();
              }
              else{
                this.envioscarga = [];
              }
            return false;
          }
        }//,
        // {
        //   text: 'Aceptar',
        //   handler: data => {
        //     console.log('Aceptar clicked');
            

            
        //   }
        // }
      ]
    });
    this.confirmNFC.present();
  }

  imprimirEnvio(){
    let copias =1;
    this.prompt = this.alertCtrl.create({
      title: '¿Imprimir Envío?',
      message: "Digite la cantidad de copias",
      enableBackdropDismiss: false,
      inputs: [
        {
          name: 'copias',
          placeholder: 'copias',
          value: "1",
          type: "number"
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
            this.prompt.dismiss();
            this.envioscarga = [];
            //this.grabarNFC();
          }
        },
        {
          text: 'Imprimir',
          handler: data => {
            console.log('Imprimir clicked');
            // this.grabarNFC();

            this.loading = this.loadingCtrl.create({
              content: 'Imprimiendo...'
            });
          
            this.loading.present();
            //guarda numero de copias en variable
            copias = data.copias;
            
            //#region MANEJO DE IMPRESORA
                    this.subBluetooth = this.printerProvider.conectarImpresora().subscribe(ok =>{
                      console.log("conecta impresora");
                      console.log(ok);
                      console.log(this.envioscarga);
                      let promarr = [];
                      let prom = Promise.resolve();
                      for (let index = 1; index <= copias; index++) {
                        prom = this.printerProvider.imprimir(this.envioscarga[0], 1, this.envioscarga[0].rozadores)
                        .then(data =>{
                          if(this.envioscarga.length > 1)
                          return this.printerProvider.imprimir(this.envioscarga[1], 1, this.envioscarga[0].rozadores)
                          else{
                            return Promise.resolve();
                          }
                        });

                        promarr.push(prom);
                        
                      }
                    Promise.all(promarr).then(data =>{
                        console.log("imprime");
                        this.loading.dismiss();
                        let alert = this.alertCtrl.create({
                          title:"Impresion finalizada",
                          buttons:[{
                            text: "Ok",
                            handler: () =>{
                              alert.dismiss();
                              return false;
                            }
                          }]
                        });
                        alert.present();
                        this.envioscarga = [];
                        this.subBluetooth.unsubscribe();
                      });
                    }
                    , err => {
                      console.log("error al imprimir")
                      console.log(err);
                      this.loading.dismiss();
                      this.subBluetooth.unsubscribe();
                      this.envioscarga = [];
                    });

                    //#endregion

            
            
          }
        }
      ]
    });
    this.prompt.present();
  }
  finEnvio(){
    this.storage.get("macprinter")
          .then(val => {
            this.macAddress = val;
            console.log("Final de envío");
              // if(val != null && val != undefined && val != "" && val != "00:00:00:00:00:00"){
              //   this.imprimirEnvio();
              // }
              // else{
                this.grabarNFC();
              // }
          })
  }

  leerAsignacionNFC(){

  }

  getIdEquipo(codigo:string){
    if(codigo == null || codigo == "") return 0;

    var newArray = this.equipos.filter(function (el) {
      return el.codequipo == codigo;
    });
    return newArray[0].idequipo;

  //   let filtro = [{campo:"codequipo", valor: codigo}];
  //   return new Promise((resolve) => {this.dbsvc.getCatalogoFiltrado("equipos", filtro).then(data =>{
  //     console.log(data);
  //     if(data.length>0){
  //       return resolve(data[0].idequipo);
  //     }
  //     else
  //     {
  //       return resolve(0);
  //     }
  //   }).catch(err => {
  //     console.log("error de equipo");
  //     console.log(err);
  //     return resolve(null);
  //   });
  // });
  }

  getIdTrabajador(codigo:string){
    if(codigo == null || codigo == "" || codigo == undefined) return 0;

    var newArray = this.trabajadores.filter((el) =>{
      return el.codtrabajador == codigo;
    });

    if(newArray.length > 0)
      return newArray[0].idtrabajador;
    else{
      return 0;
    }

  //   let filtro = [{campo:"codtrabajador", valor: codigo}];
  //   return new Promise((resolve)=>{this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data =>{
  //     console.log(data);
  //     if(data.length>0){
  //       return resolve(data[0].idtrabajador);
  //     }
  //     else
  //     {
  //       return resolve(0);
  //     }
  //   }).catch(err => {
  //     console.log("error de trabajador");
  //     console.log(err);
  //     return resolve(null);
  //   });
  //  });
  }

  fechaChange(){
    //this.hora = this.fecha;
  }

  showDatePicker(componente:string, modo:string){
    
      this.datePicker.show({
        date: componente == "fecha" ? this.fecha : this.hora,
        mode: modo,
        maxDate: new Date(),
        is24Hour: true,
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT//THEME_DEVICE_DEFAULT_LIGHT
      })
      .then(
        date =>{ console.log('Got date: ', date.toISOString());
        if(componente == "fecha"){
          this.fecha = this.showDate(date.toISOString());
          //this.hora = this.showTime(this.fecha);
        }
        if(componente == "hora"){
          this.hora =this.showTime(date.toISOString());
          
        }
        
      },
        
        err => console.log('Error occurred while getting date: ', err)
      );
    
  }
  showDate(ISOFormat: string){
    let date = new Date(ISOFormat);
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let dt = date.getDate();

    let sdt:string = ""+dt;
    let smonth:string = ""+month;
    if (dt < 10) {
      sdt = '0' + dt;
    }
    if (month < 10) {
      smonth = '0' + month;
    }

    //console.log(sdt+'/' + smonth + '/'+year);
    return sdt+'/' + smonth + '/'+year;
  }

  showTime(ISOFormat: string){
    let date = new Date(ISOFormat);
    let hh = date.getHours();
    let mm = date.getMinutes();
    let shh =""+hh;
    let smm = ""+mm;

    if (hh < 10) {
      shh = '0' + hh;
    }
    
    if (mm < 10) {
      smm = '0' + mm;
    }

    //console.log(hh+":"+mm);
    return shh+":"+smm;
  }

  construyeFechaCorte(){
    var dtRet = new Date();
    if(this.fecha!=null){
      var dia, mes, anio;
      dia =this.fecha.split('/')[0];
      mes =this.fecha.split('/')[1];
      anio =this.fecha.split('/')[2];
      dtRet.setDate(dia);
      dtRet.setMonth(mes-1);
      dtRet.setFullYear(anio);
    }
    if(this.hora!=null){
      var hora, min;
      hora = this.hora.split(":")[0];
      min = this.hora.split(":")[1];

      dtRet.setHours(hora);
      dtRet.setMinutes(min);
      dtRet.setSeconds(0);
    }

    return dtRet;
  }
}
