import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RozadoresPage } from './rozadores';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    RozadoresPage,
  ],
  imports: [
    IonicPageModule.forChild(RozadoresPage),
    SelectSearchableModule
  ],
})
export class RozadoresPageModule {}
