import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { DatabaseProvider } from '../../providers/database/database';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { Vibration } from '@ionic-native/vibration';

/**
 * Generated class for the RozadoresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rozadores',
  templateUrl: 'rozadores.html',
})
export class RozadoresPage {

  rozador: any;
  rozadores : any[] = [];
  listRozadores: any[] = [];
  profrozador ="20000166";
  unadas = 0;
  seleccionado = false;
  constructor(public navCtrl: NavController, public navParams: NavParams
    ,private barcodeScanner: BarcodeScanner
    ,private dbsvc: DatabaseProvider
    ,private alertCtrl: AlertController
    ,private vibration: Vibration
    ) {
      this.dbsvc.getRozadoresTemp()
    .then(data =>{
      console.log("ROZADORES TEMPORALES")
      console.log(data);
      this.listRozadores = data;
    
      this.getTrabajadores();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RozadoresPage');
  }
  ionViewWillEnter(){
    this.dbsvc.getRozadoresTemp()
    .then(data =>{
      
      this.listRozadores = data;
    
    });
  }
  barRozador(campo){
    this.barcodeScanner.scan().then((barcodeData) => {
      
      
        let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
        this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
          // console.log(data);
          if(data.length > 0){
            this[campo] = data[0];  
            this.unadas = 0;   
            this.seleccionado = true;     
          }
        });

     
    }).catch(err => {
      console.log('Error', err);
    });
  }
  trim(valor:string){
    return valor.replace(/^['0']+/g, '');
  }

  getTrabajadores(){
    this.dbsvc.getCatalogoFiltrado("trabajadores",[],[{campo: "nombre asc"}]).then(data => {
      
     

     this.rozadores = data.filter((item) =>{
       return item.profesion == this.profrozador;
     });
  });
}
decremento(){
  
  this.vibration.vibrate(25);
  if(this.unadas>0)
  this.unadas = this.unadas -1;
  
}
incremento(){
  this.vibration.vibrate(25);
  this.unadas = this.unadas +1;
  
}
selectChange(event: {
  component: SelectSearchableComponent,
  value: any 
  }){
    
    this.unadas = 0;
    this.seleccionado = true;  
    console.log(event.value);
  }

  agregar(){
    let obj = {idtrabajador: this.rozador.idtrabajador, codtrabajador: this.rozador.codtrabajador, nomtrabajador: this.rozador.nomtrabajador, unadas: this.unadas};
    
    
    
    
    this.unadas = 0;
    this.rozador = null;
    this.seleccionado = false;
    console.log(this.listRozadores);
   
    this.dbsvc.insertRozadorTemp(obj)
    .then(data =>{
      console.log("insertando...");
      obj["id"] = data.insertId;
      //this.listRozadores = this.listRozadores.reverse();
      this.listRozadores.push(obj);
      //this.listRozadores = this.listRozadores.reverse();
      console.log(data);
    })
    .catch(err =>{
      console.log("error insertando");
      console.log(err);
    })
  }

  editar(r,i){
    let alert = this.alertCtrl.create({
      title: "Editar uñadas",
      subTitle: r.nomtrabajador,
      inputs: [
        {
          name: 'unadas',
          placeholder: 'Uñadas',
          value: r.unadas,
          type:"number"
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Aceptar',
          
          handler: data => {
            if(data.unadas > 0){
              
              this.listRozadores[i].unadas = data.unadas;
              this.dbsvc.updateRozTemp(this.listRozadores[i]);
            }
          }
        }
      ]
    });
    alert.present();
  }

  eliminar(r,i){
    let alert = this.alertCtrl.create({
      title: "¿Quitar rozador?",
      subTitle: r.nomtrabajador,
     
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Aceptar',
          
          handler: data => {
            this.dbsvc.deleteRozTemp(r);
            this.listRozadores.splice(i,1);
          }
        }
      ]
    });
    alert.present();
  }
}
