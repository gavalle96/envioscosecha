import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the SincronizarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sincronizar',
  templateUrl: 'sincronizar.html',
})
export class SincronizarPage {

  liberaciones=false;
  trabajadores=false;
  bloque=false;
  haciendas=false;
  equipos=false;

  loading:any;
  opciones: string[] =[];
  constructor(public navCtrl: NavController, private alertCtrl: AlertController, public navParams: NavParams, private restsvc: RestProvider
    , private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SincronizarPage');
  }

  ObtenerDatos(){
    this.opciones = [];

    if(this.haciendas){
      this.opciones.push('haciendas');
    }

    if(this.trabajadores){
      this.opciones.push('trabajadores');
    }

    if(this.equipos){
      this.opciones.push('equipos');
    }

    if(this.liberaciones){
      this.opciones.push('liberaciones');
    }
    
    if(this.bloque){
      this.opciones.push('bloque');
    }

    
    
    this.presentLoadingDefault();
    this.restsvc.syncData(this.opciones).then(()=>{
      this.loading.dismiss();
      let alert = this.alertCtrl.create({
        title: 'Datos recuperados correctamente',
        
        buttons: ['Aceptar']
      });
      alert.present();
    }).catch(err => {
      console.log(err);
      let alert = this.alertCtrl.create({
        title: 'Ocurrió un error al descargar la información solicitada',
        message: 'Revise su conexión a Internet',
        buttons: ['Aceptar']
      });
      alert.present();
      this.loading.dismiss();
    });
  }

  presentLoadingDefault() {
    
    this.loading = this.loadingCtrl.create({
      content: 'Descargando información...'
    });
  
    this.loading.present();
  
    
  }
}
