import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TripulacionPage } from './tripulacion';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    TripulacionPage,
  ],
  imports: [
    IonicPageModule.forChild(TripulacionPage),
    SelectSearchableModule
  ],
})
export class TripulacionPageModule {}
