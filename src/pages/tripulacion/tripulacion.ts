import { Component, Injector } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { DatabaseProvider } from '../../providers/database/database';
import { Storage } from '@ionic/storage';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { Console } from '@angular/core/src/console';
/**
 * Generated class for the TripulacionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tripulacion',
  templateUrl: 'tripulacion.html',
})
export class TripulacionPage {
  //#region ELEMENTOS VISUALES
  // public bloque = "";  
  public chequero = "";
  public opcos ="";
  public tr1 = "";
  public tr2 = "";
  public tr3 = "";
  public supAlce = "";
  public ayud1 ="";
  public ayud2 ="";
  public ayud3 ="";
  public acom1 ="";
  public acom2 ="";
  public acom3 ="";

  disTr2 = false;
  disTr3 = false;

  //#endregion

  private camposTrabajadores: Array<{campo:string, valor:string}>= [];



//#region Parametrización de profesiones
profalzadora = "20000232";  
profcosechadora = "20000269";
  profchequero = "20000026";
  proftractorista = "20000271";
  profsupalce = "20000268";
  profacomodadores = "20000262";
  profayudante = "20000270";
  profayudanteAlce = "20000174";
  profdespuntador = "20000176";

//#endregion

//#region listas para campos de trabajadores
  private trabajadores: any[] = [];
  //private bloques: any[] = [];

  //Operadores de cosecha
  private operCosecha: any[] = [];

  //Tractoristas
  private tractoristas: any[] = [];

  //chequeros
  private chequeros: any[] = [];

  //supervisores de alce
  private supervisores: any[] = [];

  //acomodadores
  private acomodadores: any[] = [];

  //ayudantes
  private ayudantes: any[] = [];

  //despuntadores
  private despuntadores: any[] = [];
  //#endregion
  
  appCoop = "mecanizada";
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private barcodeScanner: BarcodeScanner, private dbsvc : DatabaseProvider
    , private storage: Storage, private event: Events
    , private alertCtrl: AlertController
    , private injector: Injector) {
      this.dbsvc = injector.get(DatabaseProvider);
      
      this.event.subscribe("finturno",()=>{
        this.loadDataTemporal();
      });
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TripulacionPage');
  }

  ionViewDidEnter(){
    //console.log("Entró tripulacion");
    this.storage.get("aplicativo").then(val => {
      if(val != null && val != "" && val != undefined){
        this.appCoop = val;
        console.log(val);
      }
      this.getTrabajadores();
    })
    this.loadDataTemporal();
  }

  loadDataTemporal(){
    //SE OBTIENEN LOS CÓDIGOS ALMACENADOS
    //CHEQUEROS
    this.camposTrabajadores = [];

    this.storage.get("chequero").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this.chequero = data[0];
          this.camposTrabajadores.push({campo:"chequero", valor:val});
        }
      });
      }
      else{
        this.chequero = "";
      }
    })
    // .then(() => {
    //   this.storage.get("bloque").then(val =>{
    //     if(val !== null){
  
    //       let filtro = [{campo: "idbloque",valor: val}];
    //     this.dbsvc.getCatalogoFiltrado("bloques", filtro).then(data => {
    //       // console.log(data);
    //       if(data.length > 0){
    //         this.bloque = data[0];
            
    //       }
    //     });
    //     }
    //     else{
    //       this.bloque = "";
    //     }
    //   })
    // })

   .then(() => {
      //OPERADOR DE COSECHADORA
    this.storage.get("opcos").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this.opcos = data[0];
          this.camposTrabajadores.push({campo:"opcos", valor:val});
        }
      });
      }
      else{
        this.opcos = "";
      }
    });
    }).then(() =>{
      //TRACTORISTA1
    this.storage.get("tr1").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this.tr1 = data[0];
          this.camposTrabajadores.push({campo:"tr1", valor:val});
        }
      });
      }
      else{
        this.tr1 = "";
      }
    });
    }).then(() => {
      //TRACTORISTA2
    this.storage.get("tr2").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this.tr2 = data[0];
          this.camposTrabajadores.push({campo:"tr2", valor:val});
        }
      });
      }
      else{
        this.tr2 = "";
        this.disTr2 = true;
      }
    });
    }).then(() => {
      //TRACTORISTA3
    this.storage.get("tr3").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this.tr3 = data[0];
          this.camposTrabajadores.push({campo:"tr3", valor:val});
        }
      });
      }
      else{
        this.tr3 = "";
        this.disTr3 = true;
        this.storage.get("tractor2").then(data =>{
          if(data !== "" && data !== undefined && data !== null){
            this.disTr3 = false;
          }
        })
      }
    });
    }).then(() => {
      //SUPERVISOR DE ALCE
    this.storage.get("supAlce").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this.supAlce = data[0];
          this.camposTrabajadores.push({campo:"supAlce", valor:val});
        }
      });
      }
      else{
        this.supAlce = "";
      }
    });
    }).then(() => {
      //ayudante1
    this.storage.get("ayud1").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this.ayud1 = data[0];
          this.camposTrabajadores.push({campo:"ayud1", valor:val});
        }
      });
      }
      else{
        this.ayud1 = "";
      }
    });
    })
    .then(() => {
      //ayudante2
    this.storage.get("ayud2").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this.ayud2 = data[0];
          this.camposTrabajadores.push({campo:"ayud2", valor:val});
        }
      });
      }
      else{
        this.ayud2 = "";
      }
    });
    })
    .then(() => {
      //ayudante3
    this.storage.get("ayud3").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this.ayud3 = data[0];
          this.camposTrabajadores.push({campo:"ayud3", valor:val});
        }
      });
      }
      else{
        this.ayud3 = "";
      }
    });
    })
    .then(() => {
      //ayudante4
    this.storage.get("ayud4").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this["ayud4"] = data[0];
          this.camposTrabajadores.push({campo:"ayud4", valor:val});
        }
      });
      }
      else{
        this["ayud4"] = "";
      }
    });
    })
     .then(() => {
      //ayudante5
    this.storage.get("ayud5").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this["ayud5"] = data[0];
          this.camposTrabajadores.push({campo:"ayud5", valor:val});
        }
      });
      }
      else{
        this["ayud5"] = "";
      }
    });
    })
    .then(() => {
      //ayudante6
    this.storage.get("ayud6").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this["ayud6"] = data[0];
          this.camposTrabajadores.push({campo:"ayud6", valor:val});
        }
      });
      }
      else{
        this["ayud6"] = "";
      }
    });
    })
    .then(() => {
      //despuntador 1
    this.storage.get("desp1").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this["desp1"] = data[0];
          this.camposTrabajadores.push({campo:"desp1", valor:val});
        }
      });
      }
      else{
        this["desp1"] = "";
      }
    });
    })
    .then(() => {
      //despuntador 2
    this.storage.get("desp2").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this["desp2"] = data[0];
          this.camposTrabajadores.push({campo:"desp2", valor:val});
        }
      });
      }
      else{
        this["desp2"] = "";
      }
    });
    })
    .then(() => {
      //despuntador 3
    this.storage.get("desp3").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this["desp3"] = data[0];
          this.camposTrabajadores.push({campo:"desp3", valor:val});
        }
      });
      }
      else{
        this["desp3"] = "";
      }
    });
    })
    .then(() => {
      //despuntador 4
    this.storage.get("desp4").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this["desp4"] = data[0];
          this.camposTrabajadores.push({campo:"desp4", valor:val});
        }
      });
      }
      else{
        this["desp4"] = "";
      }
    });
    })
    .then(() => {
      //despuntador 5
    this.storage.get("desp5").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this["desp5"] = data[0];
          this.camposTrabajadores.push({campo:"desp5", valor:val});
        }
      });
      }
      else{
        this["desp5"] = "";
      }
    });
    })
    .then(() => {
      //acomodador1
    this.storage.get("acom1").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this.acom1 = data[0];
          this.camposTrabajadores.push({campo:"acom1", valor:val});
        }
      });
      }
      else{
        this.acom1 = "";
      }
    });
    })
    .then(() => {
      //acomodador2
    this.storage.get("acom2").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this.acom2 = data[0];
          this.camposTrabajadores.push({campo:"acom2", valor:val});
        }
      });
      }
      else{
        this.acom2 = "";
      }
    });
    })
    //SE REMUEVE ACOMODADOR 3 SEGÚN ACUERDO Y OBSERVACIÓN DE NILTON Y SU PERSONAL
    /*.then(() => {
      //acomodador3
    this.storage.get("acom3").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codtrabajador",valor: val}];
      this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        // console.log(data);
        if(data.length > 0){
          this.acom3 = data[0];
          this.camposTrabajadores.push(val);
        }
      });
      }
      else{
        this.acom3 = "";
      }
    });
    })*/.then(() => {
      this.event.publish("finload");
    });
  }

  msgTrabajadorRepetido(){
    let alert = this.alertCtrl.create({
      title: "Trabajador repetido",
      buttons: [{
        text: 'Ok',
        handler: () => {
           alert.dismiss();
          
          return false;
        }
      }]
    });
    
    alert.present();
  }

  esRepetido(campo:string, valor:string){
    this.storage.set(campo,null);
    let resultados = this.camposTrabajadores.filter((item)=>{
      return (item.campo != campo && item.valor == valor); //el valor está repetido en otro campo
            
    });

    
    let repetido = resultados.length > 0;
    //Recuperamos el valor anterior del campo si lo hubiere
    let valorCampo = this.camposTrabajadores.filter(item => {
      return item.campo == campo;
    });
    if(valorCampo.length>0){
      let indexRemove = this.camposTrabajadores.indexOf({campo:valorCampo[0].campo,valor:valorCampo[0].valor});
      this.camposTrabajadores.splice(indexRemove,1); //quita el campo de la lista de campos
    }
    if(!repetido){
      
      
      this.camposTrabajadores.push({campo:campo,valor:valor});
    }

    //Si la aplicación está configurada para cooperativas, permitir la repetición de genéricos
    if(this.appCoop=="coop" ){
      let gener = this.trabajadores.filter(item => {
        return item.generico == 1 && item.codtrabajador == valor;
      })
      return !((gener.length > 0) || !repetido);
    } 
    return repetido;
  }
  selectChange(event: {
    component: SelectSearchableComponent,
    value: any 
    }, campo){
      
      if(!this.esRepetido(campo,event.value.codtrabajador)){
        console.log("campo: "+campo)
        this.storage.set(campo,event.value.codtrabajador);

        
      }
      else{
        this.msgTrabajadorRepetido();
        event.component.value = "";
      }

    
      
    }
  

  getTrabajadores(){
    this.dbsvc.getCatalogoFiltrado("trabajadores",[],[{campo: "generico desc"}, {campo: "nombre asc"}]).then(data => {
      //  console.log(data.length);
      this.trabajadores = data;

      this.chequeros = data.filter((item) =>{
        return item.profesion == this.profchequero;
      });
      
      
      
      
      
      this.operCosecha = data.filter((item) =>{
       if(this.appCoop == "coop"){
         return item.profesion == this.profcosechadora || item.profesion == this.profalzadora;
       }
       else if(this.appCoop == "mecanizada"){
          return item.profesion == this.profcosechadora;
         }
      else if(this.appCoop == "manual"){
      return item.profesion == this.profalzadora;
      }
       });
      

      this.tractoristas = data.filter((item) => {
        return item.profesion == this.proftractorista;
      });

      if(this.appCoop == "mecanizada"){
        this.supervisores = data.filter((item)=>{
          return item.profesion == this.profsupalce;
        });
      }
      else{
        this.supervisores = data.filter((item)=>{
          return item.profesion == "20000263";
        });
      }
      this.ayudantes = data.filter((item)=>{
        return item.profesion == (this.appCoop == "manual" ? this.profayudanteAlce : this.profayudante);
      });
      this.despuntadores = data.filter((item)=>{
        return item.profesion == this.profdespuntador;
      });

      this.acomodadores = data.filter((item)=>{
        return item.profesion == this.profacomodadores;
      });
      
    }); 

  }


  trim(valor:string){
    return valor.replace(/^['0']+/g, '');
  }
//#region ESCANEO DE CODIGOS DE BARRAS
  barChequero(){
    this.barcodeScanner.scan().then((barcodeData) => {
      //console.log('Barcode data', barcodeData);
      //this.chequero = barcodeData.text;
      
      if(!this.esRepetido("chequero",this.trim(barcodeData.text))){
        
        
        let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
        this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
          // console.log(data);
          if(data.length > 0){
            this.chequero = data[0];          
          }
        });
        this.storage.set("chequero",this.trim(barcodeData.text))


      }
      else{
        this.msgTrabajadorRepetido();
        this.chequero = "";
      }

     
    }).catch(err => {
      console.log('Error', err);
    });
    
  }
  barOpCos(){
    this.barcodeScanner.scan().then((barcodeData) => {
      
      if(!this.esRepetido("opcos",this.trim(barcodeData.text))){
        
        
        let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
        this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
          // console.log(data);
          if(data.length > 0){
            this.opcos = data[0];          
          }
        });
        this.storage.set("opcos",this.trim(barcodeData.text))


      }
      else{
        this.msgTrabajadorRepetido();
        this.opcos = "";
      }

     
    }).catch(err => {
      console.log('Error', err);
    });
  }

  barTr1(){
    this.barcodeScanner.scan().then((barcodeData) => {
      
      if(!this.esRepetido("tr1",this.trim(barcodeData.text))){
        
        
        let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
        this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
          // console.log(data);
          if(data.length > 0){
            this.tr1 = data[0];          
          }
        });
        this.storage.set("tr1",this.trim(barcodeData.text))


      }
      else{
        this.msgTrabajadorRepetido();
        this.tr1 = "";
      }

     
    }).catch(err => {
      console.log('Error', err);
    });
  }

  barTr2(){
    this.barcodeScanner.scan().then((barcodeData) => {
      
      if(!this.esRepetido("tr2",this.trim(barcodeData.text))){
        
        
        let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
        this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
          // console.log(data);
          if(data.length > 0){
            this.tr2 = data[0];          
          }
        });
        this.storage.set("tr2",this.trim(barcodeData.text))


      }
      else{
        this.msgTrabajadorRepetido();
        this.tr2 = "";
      }

     
    }).catch(err => {
      console.log('Error', err);
    });
  }

  barTr3(){
    this.barcodeScanner.scan().then((barcodeData) => {
      
      if(!this.esRepetido("tr3",this.trim(barcodeData.text))){
        
        
        let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
        this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
          // console.log(data);
          if(data.length > 0){
            this.tr3 = data[0];          
          }
        });
        this.storage.set("tr3",this.trim(barcodeData.text))


      }
      else{
        this.msgTrabajadorRepetido();
        this.tr3 = "";
      }

     
    }).catch(err => {
      console.log('Error', err);
    });
  }
  barSupAlce(){
    this.barcodeScanner.scan().then((barcodeData) => {
      
      if(!this.esRepetido("supAlce",this.trim(barcodeData.text))){
        
        
        let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
        this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
          // console.log(data);
          if(data.length > 0){
            this.supAlce = data[0];          
          }
        });
        this.storage.set("supAlce",this.trim(barcodeData.text))


      }
      else{
        this.msgTrabajadorRepetido();
        this.supAlce = "";
      }

     
    }).catch(err => {
      console.log('Error', err);
    });
  }
  barTrab(campo){
    this.barcodeScanner.scan().then((barcodeData) => {
      
      if(!this.esRepetido(campo,this.trim(barcodeData.text))){
        
        
        let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
        this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
          // console.log(data);
          if(data.length > 0){
            this[campo] = data[0];          
          }
        });
        this.storage.set(campo,this.trim(barcodeData.text))


      }
      else{
        this.msgTrabajadorRepetido();
        this[campo] = "";
      }

     
    }).catch(err => {
      console.log('Error', err);
    });
  }
  barAyud1(){
    this.barcodeScanner.scan().then((barcodeData) => {
      
      if(!this.esRepetido("ayud1",this.trim(barcodeData.text))){
        
        
        let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
        this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
          // console.log(data);
          if(data.length > 0){
            this.ayud1 = data[0];          
          }
        });
        this.storage.set("ayud1",this.trim(barcodeData.text))


      }
      else{
        this.msgTrabajadorRepetido();
        this.ayud1 = "";
      }

     
    }).catch(err => {
      console.log('Error', err);
    });
  }

  barAyud2(){
    this.barcodeScanner.scan().then((barcodeData) => {
      
      if(!this.esRepetido("ayud2",this.trim(barcodeData.text))){
        
        
        let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
        this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
          // console.log(data);
          if(data.length > 0){
            this.ayud2 = data[0];          
          }
        });
        this.storage.set("ayud2",this.trim(barcodeData.text))


      }
      else{
        this.msgTrabajadorRepetido();
        this.ayud2 = "";
      }

     
    }).catch(err => {
      console.log('Error', err);
    });
  }

  barAyud3(){
    this.barcodeScanner.scan().then((barcodeData) => {
      
      if(!this.esRepetido("ayud3",this.trim(barcodeData.text))){
        
        
        let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
        this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
          // console.log(data);
          if(data.length > 0){
            this.ayud3 = data[0];          
          }
        });
        this.storage.set("ayud3",this.trim(barcodeData.text))


      }
      else{
        this.msgTrabajadorRepetido();
        this.ayud3 = "";
      }

     
    }).catch(err => {
      console.log('Error', err);
    });
  }

  barAcom1(){
    this.barcodeScanner.scan().then((barcodeData) => {
      
      if(!this.esRepetido("acom1",this.trim(barcodeData.text))){
        
        
        let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
        this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
          // console.log(data);
          if(data.length > 0){
            this.acom1 = data[0];          
          }
        });
        this.storage.set("acom1",this.trim(barcodeData.text))


      }
      else{
        this.msgTrabajadorRepetido();
        this.acom1 = "";
      }

     
    }).catch(err => {
      console.log('Error', err);
    });
  }
  barAcom2(){
    this.barcodeScanner.scan().then((barcodeData) => {
      
      if(!this.esRepetido("acom2",this.trim(barcodeData.text))){
        
        
        let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
        this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
          // console.log(data);
          if(data.length > 0){
            this.acom2 = data[0];          
          }
        });
        this.storage.set("acom2",this.trim(barcodeData.text))


      }
      else{
        this.msgTrabajadorRepetido();
        this.acom2 = "";
      }

     
    }).catch(err => {
      console.log('Error', err);
    });
  }
  // barAcom3(){
  //   this.barcodeScanner.scan().then((barcodeData) => {
  //     let filtro = [{campo: "codtrabajador",valor:this.trim(barcodeData.text)}];
  //     this.dbsvc.getCatalogoFiltrado("trabajadores", filtro).then(data => {
        
  //       if(data.length > 0){
  //         this.acom3 = data[0];
  //         this.storage.set("acom3",this.trim(barcodeData.text))
  //       }
  //     });
  //   }).catch(err => {
  //     console.log('Error', err);
  //   });
  // }

  //#endregion

  swipe(event) {
    if(event.direction === 2) {
      this.navCtrl.parent.select(1);
    }
  }

  removeSelection(campo){
    this.storage.remove(campo);
    this[campo] = "";
  }
}
