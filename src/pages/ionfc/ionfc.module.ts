import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IonfcPage } from './ionfc';

@NgModule({
  declarations: [
    IonfcPage,
  ],
  imports: [
    IonicPageModule.forChild(IonfcPage),
  ],
})
export class IonfcPageModule {}
