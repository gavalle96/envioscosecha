import { Component, SystemJsNgModuleLoader } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { NFC, Ndef } from '@ionic-native/nfc';
import { Subscription } from 'rxjs';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';


/**
 * Generated class for the IonfcPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ionfc',
  templateUrl: 'ionfc.html',
})
export class IonfcPage {

  readingTag:   boolean   = false;
  writingTag:   boolean   = false;
  isWriting:    boolean   = false;
  ndefMsg:      string    = '';
  mensaje ="";
  subscriptions: Array<Subscription> = new Array<Subscription>();

  subBluetooth: Subscription = new Subscription();
  contenido: Array<{campo:string, valor:string}> = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public nfc: NFC,
    public ndef: Ndef,
    public alertCtrl: AlertController ) {
      this.subscriptions.push(this.nfc.addNdefListener()
        .subscribe(data => {
          if (this.readingTag) {
            let payload = data.tag.ndefMessage[0].payload;
            let tagContent = this.nfc.bytesToString(payload).substring(3);
            this.readingTag = false;
            console.log("tag data", tagContent);
            console.log("msg data", this.ndefMsg);
            console.log(tagContent == this.ndefMsg);
            if(tagContent == this.ndefMsg){
              let alert = this.alertCtrl.create({
                title: '¡Grabado Correcto!',
                subTitle: 'Envío grabado correctamente',
                buttons: [ {
                  text: 'Aceptar',
                  handler: data => {
                    
                    alert.dismiss();
                    return false;
                  }
                }]
              });
              alert.present()
            }
            this.ndefMsg = tagContent;
            this.descomponer();
          } 
          else if (this.writingTag) {
            if (!this.isWriting) {
              this.isWriting = true;
              let value = this.ndef.textRecord(this.ndefMsg);
              this.nfc.write([value])
                .then(() => {
                  this.writingTag = false;
                  this.isWriting = false;
                  console.log("written");
                  this.readingTag = true;

                  let alert = this.alertCtrl.create({
                    title: 'Tarjeta grabada',
                    subTitle: 'Acerque de nuevo para confirmar',
                    buttons: [ {
                      text: 'Aceptar',
                      handler: data => {
                        
                        alert.dismiss();
                        return false;
                      }
                    }]
                  });
                  alert.present()

                })
                .catch(err => {
                  this.writingTag = false;
                  this.isWriting = false;
                  console.log("Error en escritura")
                  console.log(err);
                });
            }
          }
        },
        err => {
        
        })
     );
     
  }
  
  ionViewWillEnter() {
    // this.nfc.addNdefListener(() => {
    //   console.log('successfully attached ndef listener');
    // }, (err) => {
    //   console.log('error attaching ndef listener', err);
    // }).subscribe((event) => {
    //   console.log('received ndef message. the tag contains: ', event.tag);
    //   console.log('decoded tag id', this.nfc.bytesToHexString(event.tag.id));
    
    //   let message = this.ndef.textRecord('Hello world');
    //   this.nfc.share([message]).then((data)=> {
    //     console.log(data);
    //     console.log("Escribió bien")
    //   }).catch(err => {
    //     console.log("neles pasteles");
    //     console.log(err);
    //   });
    // });
  }
  ionViewWillLeave() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
    
    // this.subBluetooth.unsubscribe();
    
  }

  readTag() {
    this.readingTag = true;
    //Attaching the NFC Listener
    
    // this.nfc.addNdefListener(() => {
    //   this.SuccessAttach();
    // }, (err) => {
    //   this.ErrorAttach();
    //   console.log(err)
    //   }).subscribe((event) => {
    //     // let alert = this.alertCtrl.create({
    //     //   subTitle: 'RECEIVED!',
    //     //   buttons: ['Dismiss']
    //     // });
    //     // alert.present();
    //   // this.receivedValue = event.tag;
    //   console.log("tag recibido");
    //   console.log(event.tag)
    //    let payload = event.tag.ndefMessage[0].payload;
    //    let tagContent = this.nfc.bytesToString(payload).substring(3);
      
    //   //  console.log(payload);
    //   //  console.log(tagContent);
    //   this.ndefMsg = tagContent;
    //   this.subBluetooth.unsubscribe();
    // });
  }

  writeTag() {
    let cadena = "";
    for (let i = 0; i < this.contenido.length; i++) {
      cadena += "<"+this.contenido[i].campo+">="+document.getElementById(i+"").getElementsByTagName("input")[0].value+"%";
      //console.log(document.getElementById(i+"").getElementsByTagName("input")[0].value);
      
    }
    cadena += " %";
    console.log(cadena);
    this.ndefMsg = cadena;
    this.showConfirm().then(data => {
      if(data){
        this.writingTag = true;
      }
    });
    
    //this.ndefMsg = 
    // this.ndef.textRecord(this.ndefMsg);
    //Attaching the NFC Listener
    // this.subBluetooth = this.nfc.addNdefListener(() => {
    //   this.SuccessAttach();
    // }, (err) => {
    //   this.ErrorAttach();


    // }).subscribe((event) => {
    //   //Send the message
    //   console.log("VA A ESCRIBIR");
    //   let value = this.ndef.textRecord(this.ndefMsg);
    //   this.nfc.write([value]).then(data => {
        
    //     console.log("Funciona ok");
    //     console.log(data);
    //     this.subBluetooth.unsubscribe();
    //   })
    //   .catch(err =>{
    //     console.log("NO FUNCIONA");
    //     console.log(err);
    //     this.subBluetooth.unsubscribe();
    //   });
    // });


  }
  showConfirm(): Promise<boolean> {
    return new Promise((resolve,reject)=>{
    const prompt = this.alertCtrl.create({
      title: 'Permisos Requeridos',
      message: "Requiere contraseña de administrador para entrar",
      inputs: [
        {
          name: 'pwd',
          placeholder: 'Contraseña',
          type: 'password'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler:_=> resolve(false)
        },
        {
          text: 'Aceptar',
          handler: data => {

            resolve(data.pwd == "12505566"); //comparar contraseña
          }
        }
      ]
    });
    prompt.present();
    })
  }
  SuccessAttach() {
    
    console.log("Attatch correcto");
  }
  ErrorAttach() {
    console.log("Error en Attatch");
  }
  descomponer(){
    this.ndefMsg = this.ndefMsg.substr(0,this.ndefMsg.length -3);
    console.log(this.ndefMsg);
    this.ndefMsg = this.ndefMsg.split("<").join("");
    console.log(this.ndefMsg);
    this.ndefMsg = this.ndefMsg.split(">").join("")
    console.log(this.ndefMsg);
    let arr = [];
    arr = this.ndefMsg.split("%");
    console.log(arr);
    this.contenido = [];
    arr.forEach(item => {
      let spl = item.split("=");
      console.log(spl);
      this.contenido.push({campo:spl[0], valor:spl[1]});
    });
  }

}