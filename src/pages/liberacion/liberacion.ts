import { Component, Injector } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { SelectSearchableComponent } from 'ionic-select-searchable';
import { Storage } from '@ionic/storage';
import { DatabaseProvider } from '../../providers/database/database';
import { THIS_EXPR, IfStmt } from '@angular/compiler/src/output/output_ast';
/**
 * Generated class for the LiberacionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-liberacion',
  templateUrl: 'liberacion.html',
})
export class LiberacionPage {
  hdas: any[] = [];
  hda: any;
  liberacion: any;
  liberaciones: any[] = [];
  copyliberaciones: any[] = [];
  gettingLocation = false;

  public bloque = "";
  bloques: any[] = []; 
  copybloques: any[] = [];  
  lote: any;
  lotes: any[] = [];
  copylotes: any[] = [];
  loadingdata = false;
  todaslib = false;
  appCoop = "mecanizada";

  constructor(public navCtrl: NavController, public navParams: NavParams
    ,public alertCtrl: AlertController
    , private dbservice: DatabaseProvider
    , private storage: Storage
    , private event: Events
    , private injector: Injector) {
      this.todaslib = true;
      this.changeLib();
      this.dbservice = injector.get(DatabaseProvider);
      this.storage.get("aplicativo").then(val => {
        if(val != null && val != "" && val != undefined){
          this.appCoop = val;
        }
        this.load();
      });
    
    this.event.subscribe("finturno",()=>{
      this.loadDataTemporal();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LiberacionPage');
  }

  ionViewDidEnter(){
    //console.log("Entró tripulacion");
    this.loadDataTemporal();
  }

  loadDataTemporal(){
    let idhacienda = 0;
    this.loadingdata = true;
    this.load();
    //SE OBTIENEN LOS CÓDIGOS ALMACENADOS
    //HACIENDA
    this.bloque = "";
    this.storage.get("idhacienda")
    .then(val =>{
      if(val !== null){

        let filtro = [{campo: "idhacienda",valor: val}];
        idhacienda = val;
      
      return this.dbservice.getCatalogoFiltrado("haciendas", filtro);
      }
      else{
        this.hda = "";
        this.changeLib();
      }
    }).then(data => {
        // console.log(data);

        if(idhacienda > 0){
          if(data != undefined && data != null)
          if(data.length > 0){
            this.hda = data[0];
            //CARGA LOTES
          return this.dbservice.getLotesConLiberaciones(idhacienda);//.getCatalogoFiltrado("lotes",filtro, orden)
            
        }
        }
      })
      .then(retorno =>{
        //console.log(retorno); 
        
        this.lotes = retorno;

        return this.storage.get("idlote");
      })
      .then(val =>{
        
        if(val !== null){
  
          let filtro = [{campo: "idlote",valor: val}];
        return this.dbservice.getCatalogoFiltrado("lotes", filtro);
        
        }
        else{
          this.lote = "";
          this.changeLib();
        }

        
      })
      .then(data => {
        //console.log(data);
        if(data != null && data != undefined)
        if(data.length > 0){
          this.lote = data[0];
          this.lote["bklote"] = this.lote["bklote"]+" - "+this.lote["dslote"];
        //   let filtro = [{campo:"idlote", valor:data[0].idlote}];
        // let orden = [{campo:"codliberacion"}];
        //  this.dbservice.getCatalogoFiltrado("liberaciones",filtro, orden)
        //  .then(retorno =>{
        //   // this.liberaciones = retorno;
          
        //  })
        //  .catch(err => {console.log(err)});
        this.changeLib();
        console.log("liberacion "+this.liberacion);
        }
      return this.storage.get("todaslib");
    }).then((valtodaslib)=> {

      // if(valtodaslib == null)
      //   valtodaslib = false;
      // this.todaslib = valtodaslib;
      this.todaslib = true;
      this.changeLib();

      //LIBERACION
      return this.storage.get("libActual");
    })
    .then(val =>{
        console.log(val);
        console.log("cambio libactual");
        console.log("liberacion "+this.liberacion);
          if(val !== null && val != undefined){

            let filtro = [{campo: "codliberacion",valor: val}];
          return this.dbservice.getCatalogoFiltrado("liberaciones", filtro)
          }
          else{
            this.liberacion = null;
          }
    })
    .then(data => {

      if(data != null && data != undefined){
          let libs = this.copyliberaciones.filter(item =>{
            return item.codliberacion == data[0].codliberacion;
          });
            if(libs.length > 0){
              this.liberacion = libs[0];
              let blqs = this.bloques.filter(item =>{
                return item.idbloque == data[0].idbloque;
              });
      
              if(blqs.length > 0){
                this.bloque = blqs[0].dsbloque;
              }
            }
          }
            console.log("cambio liberacion filtrada");
            console.log("liberacion "+this.liberacion);
        this.loadingdata = false;
        
    });

    

    
  }

  load(){

    


    this.dbservice.getHaciendasConLiberaciones()//getCatalogoFiltrado("haciendas",[],orden)
    .then(data=>{
      console.log(data);
      this.hdas = data;
      return this.dbservice.getCatalogo("bloques");
    })
    .then(data => {
      this.copybloques = data;
      this.bloques = data;
      return this.dbservice.getLotesConLiberaciones();
    })
    .then(data =>{
      this.copylotes = data;

      this.copylotes.forEach(item => {
        item.bklote = item.bklote + " - " + item.dslote;
      });

      //this.lotes = data;
      let manual = this.appCoop == "mecanizada" ? 0 : 1;
      if(this.appCoop == "coop"){
        return this.dbservice.getCatalogoFiltrado("liberaciones",[],[{campo:"fechahora desc"}]);
      }
      else{
        return this.dbservice.getCatalogoFiltrado("liberaciones",[{campo:"manual",valor: manual}],[{campo:"fechahora desc"}]);
      }
    })
    .then(data =>{
      if(data !== null && data !== undefined){
        data.forEach(item => {
          item["dsliberacion"] = item.codliberacion + " - Fecha: " + this.showDate(item.fechahora) + " " + this.showTime(item.fechahora);
        });
      }
      this.copyliberaciones = data;
      // this.liberaciones  = data;
    })
    .catch(err =>{
      console.log(err);
    });
    
  }

  hdaChange(event: {
    component: SelectSearchableComponent,
    value: any 
    }) {
    if(!this.loadingdata)
      this.storage.set('libActual',null);
    
      this.storage.set('fechacorte', null)

      
      this.storage.set("idlote",null);
      this.storage.set("bloque",null);
      /*let filtro = [{campo:"idhacienda", valor:event.value.idhacienda}];
      let orden = [{campo:"bklote"}];*/
      
       this.dbservice.getLotesConLiberaciones(event.value.idhacienda)//.getCatalogoFiltrado("lotes",filtro, orden)
       .then(retorno =>{
        //console.log(retorno); 
        this.storage.set("idhacienda",event.value.idhacienda);
        this.changeLib();
        this.lotes = retorno;
        this.lote ="";
        if(!this.loadingdata)
        this.liberacion ="";
        this.bloque ="";
       })
       .catch(err => {console.log(err)});
    }

    

    loteChange(event: {
      component: SelectSearchableComponent,
      value: any 
      }) {

        
        let lat = 0;
        let lon = 0;
        
        let filtro = [{campo:"idlote", valor:event.value.idlote}];
        let orden = [{campo:"codliberacion"}];
         this.dbservice.getCatalogoFiltrado("liberaciones",filtro, orden)
         .then(retorno =>{
          this.storage.set("idlote",event.value.idlote);
          //this.liberaciones = retorno;
          //Al seleccionar lote va a cargar liberaciones según opción
          this.changeLib();

          console.log("cambio lote");
          console.log(this.loadingdata);
          if(!this.loadingdata){
            this.liberacion = "";
            this.storage.remove('libActual');
          }

         
         return this.storage.get("latitud");
        })
         .then(data=>{
           lat = data;
           return this.storage.get("longitud");
         })
         .then(data => {
           lon = data;
           this.verificarLugar(lat, lon);
         })
         .catch(err => {console.log(err)});
      }

    swipe(event) {
      if(event.direction === 2) {
        this.navCtrl.parent.select(3);
      }
      if(event.direction === 4) {
        this.navCtrl.parent.select(1);
      }
    }

    setLiberacion(event: {
      component: SelectSearchableComponent,
      value: any 
      }){
      var lat, lon;
      this.storage.remove('fechacorte');
      this.storage.remove('horacorte');
      this.storage.set('libActual',event.value.codliberacion);
      this.storage.set('fechacorte', this.showDate(event.value.fechahora))
      this.storage.set('horacorte', this.showTime(event.value.fechahora))
      
      this.storage.set('fhquema', event.value.fechahora)
      

      this.storage.set("idhacienda",event.value.idhacienda);
      this.storage.set("idlote",event.value.idlote);
      this.storage.set("bloque",event.value.idbloque);
      

      this.dbservice.getCatalogoFiltrado("haciendas", [{campo:"idhacienda", valor:event.value.idhacienda}])
      .then(data => {
        
        this.hda = data[0];
        return this.dbservice.getCatalogoFiltrado("lotes", [{campo:"idlote", valor: event.value.idlote}]);
      })
      .then(data => {

        this.lote = data[0];
        this.lote["bklote"] = this.lote["bklote"]+" - "+this.lote["dslote"];
        this.lotes = this.copylotes.filter(item =>{
          return item.idhacienda == event.value.idhacienda
        });
        

        let blqs = this.bloques.filter(item =>{
          return item.idbloque == event.value.idbloque;
        });

        if(blqs.length > 0){
          this.bloque = blqs[0].dsbloque;
        // console.log(this.bloque);
        }
        return this.storage.get("latitud");
      })
       .then(data=>{
         lat = data;
         return this.storage.get("longitud");
       })
       .then(data => {
         lon = data;
         this.verificarLugar(lat, lon);
       })
       .catch(err => {console.log(err)});
      

      //console.log(this.liberacion);
    }

    
    changeLib(){
      
      console.log("changelib", this.todaslib);
      //Setea si el usuario quiere ver todas las liberaciones o solo las del lote seleccionado
      this.storage.set("todaslib",this.todaslib);
      if(this.todaslib){
        this.storage.get("idhacienda").then(val=>{
          if(val!==null){
            this.liberaciones = this.copyliberaciones.filter(item=>{
              return item.idhacienda == val;
            });
          }
          else{
            this.liberaciones = [];
          }
        });
        
      }
      else{
        // TODAS LAS LIBERACIONES
        this.liberaciones = this.copyliberaciones;
        // Solo liberaciones del lote seleccionado
        // this.storage.get("idlote").then(val =>{
        //   if(val !== null){
        //     this.liberaciones = this.copyliberaciones.filter(item =>{
        //       return item.idlote == val;
        //     });
        //   }
        //   else{
        //     this.liberaciones = [];
        //   }
        // });
        
        
      }
      console.log("end-changelib");
    }

    verificarLugar(lat, lng){
      return this.storage.get("idlote").then(val =>{
        let idlote = 0;
        if(val !== undefined && val !== null)
        idlote = val;
  
        console.log(idlote);
        return this.dbservice.puntoEnPoligono({lat:lat, lng:lng},val)
      })
        .then(ok=>{
          if(!ok){
            let preventEdit =  this.alertCtrl.create({
              enableBackdropDismiss: false,
              title: "UBICACIÓN NO COINCIDE",
              subTitle: "La ubicación no coincide con la ubicación de la liberación" ,
              buttons: [{
                text: 'OK',
                handler: () => {
                   preventEdit.dismiss().catch(er => console.log(er));
                   this.gettingLocation = false;
  
                   return false;
                }
              }]
            });
            
            preventEdit.present();
          }
          //#region COINCIDE UBICACION
          // else{
            
          //   let edit =  this.alertCtrl.create({
          //     enableBackdropDismiss: false,
          //     title: "LIBERACIÓN SI COINCIDE UBICACIÓN",
          //     subTitle: "La ubicación SI coincide con la ubicación de la liberación" ,
          //     buttons: [{
          //       text: 'OK',
          //       handler: () => {
          //          edit.dismiss().catch(er => console.log(er));
                   
          //           this.gettingLocation = false;
          //          return false;
          //       }
          //     }]
          //   });
            
          //   edit.present();
          // }
          //#endregion
          return ok;
        })
        .catch(err => {console.log(err);
          return false;
        });
    }
    showDate(ISOFormat: string){
      let date = new Date(ISOFormat);
      let year = date.getFullYear();
      let month = date.getMonth()+1;
      let dt = date.getDate();
  
      let sdt:string = ""+dt;
      let smonth:string = ""+month;
      if (dt < 10) {
        sdt = '0' + dt;
      }
      if (month < 10) {
        smonth = '0' + month;
      }
  
      //console.log(sdt+'/' + smonth + '/'+year);
      return sdt+'/' + smonth + '/'+year;
    }
  
    showTime(ISOFormat: string){
      let date = new Date(ISOFormat);
      let hh = date.getHours();
      let mm = date.getMinutes();
      let ss = date.getSeconds();
      let shh =""+hh;
      let smm = ""+mm;
      let sss = ""+ss;
      if (hh < 10) {
        shh = '0' + hh;
      }
      if (mm < 10) {
        smm = '0' + mm;
      }
      if (ss < 10) {
        sss = '0' + ss;
      }
  
      //console.log(hh+":"+mm);
      return shh+":"+smm;//+":"+sss;
    }
}
