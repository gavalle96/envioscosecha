import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiberacionPage } from './liberacion';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    LiberacionPage,
  ],
  imports: [
    IonicPageModule.forChild(LiberacionPage),
    SelectSearchableModule
  ],
})
export class LiberacionPageModule {}
