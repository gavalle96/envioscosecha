import { Component, Injector } from '@angular/core';
import { IonicPage, NavController, Events, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { DatabaseProvider } from '../../providers/database/database';
/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  ambiente: string="qaslan";
  url: string ="";
  mac: string = "00:00:00:00:00:00";
  minEnvio:  number;
  maxEnvio: number;
  ultEnvio: number;
  checkpoligono: boolean;
  
  checkAdmon: boolean;
  nomdb = "";

  aplicativo = "mecanizada"

  

  constructor(public navCtrl: NavController, public navParams: NavParams
  ,public alertCtrl: AlertController, public storage: Storage
  , private sqLite: SQLite, private bdService :DatabaseProvider
  , private injector: Injector
  , private event: Events
  ) {
    this.bdService = injector.get(DatabaseProvider);
  }

  ionViewDidLoad() {
    
    console.log('ionViewDidLoad SettingsPage');


    this.storage.get('ambiente').then((val) => {
      this.ambiente = val;
    });
    this.storage.get('server').then((val) => {
      this.url = val;
    });

    this.storage.get('envioMin').then((val) => {
      this.minEnvio = val;
    });

    this.storage.get('envioMax').then((val) => {
      this.maxEnvio = val;
    });

    this.storage.get('ultEnvio').then((val) => {
      this.ultEnvio = val;
    });

    this.storage.get('macprinter').then((val) => {
      if(val != null && val != undefined && val != "")
        this.mac = val;
    });

    this.storage.get('checkpoligono').then((val) => {
      this.checkpoligono = val;
    });
    this.storage.get('aplicativo').then((val) => {
      this.aplicativo = val;
    });
    this.storage.get('admon').then((val) => {
      this.checkAdmon = val;
    });

    this.storage.get('db').then(val => {
      this.nomdb = val;
    })

  }
ionViewCanEnter(){
return this.showConfirm();
}
ionViewCanLeave(){
  if(/^([0-9A-F]{2}[:]){5}([0-9A-F]{2})$/.test(this.mac) || this.mac =="" || this.mac == "00:00:00:00:00:00" )
  return true
  else
  {
    let alert = this.alertCtrl.create({
      title: 'Verificar',
      subTitle: 'Formato de MAC Address no es correcto',
      buttons: ['OK']
    });
    alert.present();
    return false;
  }
}
  ionViewWillLeave() {
    if(this.ambiente == 'qaslan' || this.ambiente == 'qaspub' ){
      this.storage.set('db','dbqas').then(()=>{
        this.createDatabase();
      });
    }
    else if(this.ambiente == 'prdlan' || this.ambiente == 'prdpub'){
      this.storage.set('db','dbprd').then(()=>{
        this.createDatabase();
      });
    }
    this.saveServer();
    
    this.storage.set('envioMin',this.minEnvio);
    this.storage.set('envioMax',this.maxEnvio);
    this.storage.set('ultEnvio',this.ultEnvio);
    this.storage.set('macprinter',this.mac);
    this.storage.set('checkpoligono',this.checkpoligono);
    this.storage.set('aplicativo',this.aplicativo);
    this.storage.set('admon',this.checkAdmon);
    this.event.publish("finajustes");
  }

    ionViewWillUnload() {
      
    }


    public createDatabase() {
      let dbname = "";
      return this.storage.get('db').then((val) => {
        dbname = val;
  
        let promise = this.sqLite.create({
          name: dbname+'.db',//'dbcatalogos.db',
          location: 'default'
        });
        promise.then((db: SQLiteObject) => {
            this.bdService.setDatabase(db);
            this.bdService.createTables();
            this.bdService.onStart();
            console.log("Database creada");
        }).catch(e => console.log("Error : " + e));
        
      });
      
      
  }
  showAlert() {
      const alert = this.alertCtrl.create({
        title: 'Autenticar',
        subTitle: 'Necesita autenticar',
        buttons: ['OK']
      });
      alert.present();
    }

    showConfirm(): Promise<boolean> {
    return new Promise((resolve,reject)=>{
    const prompt = this.alertCtrl.create({
      title: 'Permisos Requeridos',
      message: "Requiere contraseña de administrador para entrar",
      enableBackdropDismiss: false,
      inputs: [
        {
          name: 'pwd',
          placeholder: 'Contraseña',
          type: 'password'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler:_=> resolve(false)
        },
        {
          text: 'Aceptar',
          handler: data => {

            resolve(data.pwd == "12505566"); //comparar contraseña
          }
        }
      ]
    });
    prompt.present();
    })
  }
  saveServer(){
    switch(this.ambiente){
      case "qaslan":
        this.storage.set('serverqaslan',this.url);
        this.storage.set('server',this.url);
        this.storage.set('db','dbqas');
        this.nomdb = 'dbqas';

      break;
      case "qaspub":
        this.storage.set('serverqaspub',this.url);
        this.storage.set('server',this.url);
        this.storage.set('db','dbqas');
        this.nomdb = 'dbqas';
      break;
      case "prdlan":
        this.storage.set('serverprdlan',this.url);
        this.storage.set('server',this.url);
        this.storage.set('db','dbprd');
        this.nomdb = 'dbprd';
      break;
      case "prdpub":
        this.storage.set('serverprdpub',this.url);
        this.storage.set('server',this.url);
        this.storage.set('db','dbprd');
        this.nomdb = 'dbprd';
      break;
    }
  }
  getServer(){

    switch(this.ambiente){
      case "qaslan":
      this.storage.get('serverqaslan').then((val) => {
        this.url = val;
        this.storage.set('server',val);
        this.storage.set('ambiente',this.ambiente);
        this.storage.set('db','dbqas');
      });

      break;
      case "qaspub":
      this.storage.get('serverqaspub').then((val) => {
        this.url = val;
        this.storage.set('server',val);
        this.storage.set('ambiente',this.ambiente);
        this.storage.set('db','dbqas');
      });

      break;
      case "prdlan":
      this.storage.get('serverprdlan').then((val) => {
        this.url = val;
        this.storage.set('server',val);
        this.storage.set('ambiente',this.ambiente);
        this.storage.set('db','dbprd');
      });

      break;
      case "prdpub":
      this.storage.get('serverprdpub').then((val) => {
        this.url = val;
        this.storage.set('server',val);
        this.storage.set('ambiente',this.ambiente);
        this.storage.set('db','dbprd');
      });

      break;
    }

  }

  validarMinMax(){
    
  }
  cleanEnvios(){
    let msg ="Envíos eliminados correctamente";
    let confirm = this.alertCtrl.create({
      title: '¿Seguro de borrar la tabla de envíos?',
      buttons:[{text:'Cancelar'}, {text:'Aceptar',handler:data =>{
        this.borrarEnvios().then(val =>{

          let alrt = this.alertCtrl.create({
            title:msg,
            buttons:['OK']
          });
          if(!val){
            msg ="Ocurrió un problema al eliminar";
          }
          alrt.present();
          return false;
        });
      }}]
    });

    confirm.present();
  }
  borrarEnvios(){
    return this.bdService.limpiarCatalogosPorNombreTabla("envios").then(
      ()=>{
        return true;
      }
    ).catch(err => 
    {
      console.log(err);
      return false;
    });
    
  }
}
