import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverconsultarPage } from './popoverconsultar';

@NgModule({
  declarations: [
    PopoverconsultarPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverconsultarPage),
  ],
})
export class PopoverconsultarPageModule {}
