import { Component } from '@angular/core';
import { IonicPage,ViewController, NavController, NavParams } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';

/**
 * Generated class for the PopoverconsultarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-popoverconsultar',
  templateUrl: 'popoverconsultar.html',
})
export class PopoverconsultarPage {

  PassedFilteredItems:any;
  itemsToFilterBy:any;


  filtrarfecha:boolean = false;
  fechaini;
  fechafin;
  fecini;
  fecfin;

  horini;
  horfin;
  
  params: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
      public viewCtrl: ViewController
      , private datePicker: DatePicker) {
      this.params = this.navParams.data;
      this.PassedFilteredItems = this.params.items;
      //console.log('parameters:', this.params);
      this.formatDate();
      if(this.params.fechaini != null){
        this.fechaini = this.params.fechaini;
        this.fecini = this.showDate(this.fechaini);
        this.horini = this.showTime(this.fechaini)
      }
      if(this.params.fechafin != null){
        this.fechafin = this.params.fechafin;
        this.fecfin = this.showDate(this.fechafin);
        this.horfin = this.showTime(this.fechafin)
      }
      

      this.filtrarfecha = this.params.filtrarfecha;
      
      
    }

    cerrarPopover(){
    console.log("popover dismissed");
      /*if(this.filtrarfecha){
        this.PassedFilteredItems = this.PassedFilteredItems.filter((item)=>{
          return (item.fhcorte >= this.fechaini && item.horaEnvio >= this.showTime(this.horaini))
          && (item.fhcorte <= this.fechafin && item.horaEnvio <= this.showTime(this.horafin))
        });
      }*/
      
      let params = {items: this.PassedFilteredItems, filtrarfecha: this.filtrarfecha, fechaini:this.fechaini, fechafin: this.fechafin};
      
      this.viewCtrl.dismiss(params);

  }
  showDate(ISOFormat: string){
    let date = new Date(ISOFormat);
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let dt = date.getDate();

    let sdt:string = ""+dt;
    let smonth:string = ""+month;
    if (dt < 10) {
      sdt = '0' + dt;
    }
    if (month < 10) {
      smonth = '0' + month;
    }

    //console.log(sdt+'/' + smonth + '/'+year);
    return sdt+'/' + smonth + '/'+year;
  }

  showTime(ISOFormat: string){
    let date = new Date(ISOFormat);
    let hh = date.getHours();
    let mm = date.getMinutes();
    let shh =""+hh;
    let smm = ""+mm;
    if (hh < 10) {
      shh = '0' + hh;
    }
    if (mm < 10) {
      smm = '0' + mm;
    }

    //console.log(hh+":"+mm);
    return shh+":"+smm;
  }

  formatDate(){
    Date.prototype.toISOString = function() {
      var tzo = -this.getTimezoneOffset(),
          dif = tzo >= 0 ? '+' : '-',
          pad = function(num) {
              var norm = Math.floor(Math.abs(num));
              return (norm < 10 ? '0' : '') + norm;
          };
      return this.getFullYear() +
          '-' + pad(this.getMonth() + 1) +
          '-' + pad(this.getDate()) +
          'T' + pad(this.getHours()) +
          ':' + pad(this.getMinutes()) +
          ':' + pad(this.getSeconds()) +
          dif + pad(tzo / 60) +
          ':' + pad(tzo % 60);
  }
  }

  showDatePicker(componente:string){
    if(this.filtrarfecha){
      this.datePicker.show({
        date: new Date(),
        mode: 'datetime',
        minDate: (componente=="fecfin" ? new Date(this.fechaini) : ""),
        maxDate: (componente=="fecini" ? new Date(this.fechafin) : ""),
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT//THEME_DEVICE_DEFAULT_LIGHT
      }).then(
        date =>{ console.log('Got date: ', date.toISOString());
        if(componente == "fecini"){
          this.fechaini = date.toISOString();
          this.fecini = this.showDate(date.toISOString());
          this.horini = this.showTime(date.toISOString());
        }
        if(componente == "fecfin"){
          this.fechafin = date.toISOString();
          this.fecfin = this.showDate(date.toISOString());
          this.horfin = this.showTime(date.toISOString());
        }
        
      },
        
        err => console.log('Error occurred while getting date: ', err)
      );
    }
  }

  


}
