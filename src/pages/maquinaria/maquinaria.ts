import { Component, Injector } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, AlertController } from 'ionic-angular';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { DatabaseProvider } from '../../providers/database/database';
import { Storage } from '@ionic/storage';
import { SelectSearchableComponent } from 'ionic-select-searchable';
/**
 * Generated class for the MaquinariaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-maquinaria',
  templateUrl: 'maquinaria.html',
})
export class MaquinariaPage {

  cosechadora ="";
  tractor1 = "";
  tractor2 = "";
  tractor3 = "";

  private equipos: any[] = [];

  cosechadoras: any[] = [];
  tractores:any[] = [];

  camposEquipos: Array<{campo:string,valor:any}> = [];
  appCoop = "mecanizada";
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private barcodeScanner: BarcodeScanner
    , private dbsvc: DatabaseProvider
    , private storage: Storage
    , private alertCtrl: AlertController
    , private event: Events
    , private injector: Injector) {
      this.dbsvc = injector.get(DatabaseProvider);
      this.storage.get("aplicativo").then(val => {
        if(val != null && val != "" && val != undefined){
          this.appCoop = val;
        }
        this.getEquipos();
      })
      
      this.event.subscribe("finturno",()=>{
        this.loadDataTemporal();
      });

      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MaquinariaPage');
  }

  ionViewDidEnter(){
    //console.log("Entró tripulacion");
    this.loadDataTemporal();
  }

  msgEquipoRepetido(){
    let alert = this.alertCtrl.create({
      title: "Equipo repetido",
      buttons: [{
        text: 'Ok',
        handler: () => {
           alert.dismiss();
          
          return false;
        }
      }]
    });
    
    alert.present();
  }
  selectChange(event: {
    component: SelectSearchableComponent,
    value: any
    }, campo){

      if(!this.esRepetido(campo,event.value.codequipo)){
        console.log("campo: "+campo)
        this.storage.set(campo,event.value.codequipo);

        
      }
      else{
        this.msgEquipoRepetido();
        event.component.value = "";
      }
      
    }

  loadDataTemporal(){
    //SE OBTIENEN LOS CÓDIGOS ALMACENADOS
    //COSECHADORA
    this.camposEquipos = [];
    this.storage.get("cosechadora").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codequipo",valor: val}];
      this.dbsvc.getCatalogoFiltrado("equipos", filtro).then(data => {
        // console.log(data);
        if(data!=null) if(data.length > 0){
          this.cosechadora = data[0];
        }
      });
      }
      else{
        this.cosechadora = "";
      }
    });

    //TRACTOR1
    this.storage.get("tractor1").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codequipo",valor: val}];
      this.dbsvc.getCatalogoFiltrado("equipos", filtro).then(data => {
        // console.log(data);
        if(data!=null) if(data.length > 0){
          this.tractor1 = data[0];
          this.camposEquipos.push({campo:"tractor1", valor:val});
        }
      });
      }
      else{
        this.tractor1 = "";
      }
    });

    //TRACTOR2
    this.storage.get("tractor2").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codequipo",valor: val}];
      this.dbsvc.getCatalogoFiltrado("equipos", filtro).then(data => {
        // console.log(data);
        if(data!=null) if(data.length > 0){
          this.tractor2 = data[0];
          this.camposEquipos.push({campo:"tractor2", valor:val});
        }
      });
      }
      else{
        this.tractor2 = "";
      }
    });

    //TRACTOR3
    this.storage.get("tractor3").then(val =>{
      if(val !== null){

        let filtro = [{campo: "codequipo",valor: val}];
      this.dbsvc.getCatalogoFiltrado("equipos", filtro).then(data => {
        // console.log(data);
        if(data!=null) if(data.length > 0){
          this.tractor3 = data[0];
          this.camposEquipos.push({campo:"tractor3", valor:val});
        }
      });
      }
      else{
        this.tractor3 = "";
      }
    });
  }

  trim(valor:string){
    return valor.replace(/^['0']+/g, '');
  }

  removeSelection(campo){
    switch (campo) {
      case "tractor2":
        this.tractor2 = "";
        this.storage.remove("tractor2");
        break;
      case "tractor3":
        this.tractor3 = "";
        this.storage.remove("tractor3");
        break;
    
      default:
        break;
    }
  }

  esRepetido(campo:string, valor:string){
    this.storage.set(campo,null);
    let resultados = this.camposEquipos.filter((item)=>{
      return (item.campo != campo && item.valor == valor); //el valor está repetido en otro campo
            
    });

    //Si es una aplicación de cooperativa, que permita repetir los genéricos
    if(this.appCoop != "mecanizada") return false;

    let repetido = resultados.length > 0;
    //Recuperamos el valor anterior del campo si lo hubiere
    let valorCampo = this.camposEquipos.filter(item => {
      return item.campo == campo;
    });
    if(valorCampo.length>0){
      let indexRemove = this.camposEquipos.indexOf({campo:valorCampo[0].campo,valor:valorCampo[0].valor});
      this.camposEquipos.splice(indexRemove,1); //quita el campo de la lista de campos
    }
    if(!repetido){
      this.camposEquipos.push({campo:campo,valor:valor});
    }

    //console.log(this.camposTrabajadores);
    return repetido;
  }

  getEquipos(){
    this.dbsvc.getCatalogoFiltrado("equipos").then(data => {

      this.equipos = data;
      console.log(data);
    });

    let tipocos = 5;
    if(this.appCoop != "mecanizada"){
      tipocos = 1;
    }

    if(this.appCoop=="coop"){
      this.dbsvc.getCargadorasCosechadoras().then(data => {
        console.log(data);
        this.cosechadoras = data;
      });
    }
    else{

      this.dbsvc.getCatalogoFiltrado("equipos", [{campo:"tipo", valor: tipocos}]).then(data => {
        console.log(data);
        this.cosechadoras = data;
      });
    }

    this.dbsvc.getCatalogoFiltrado("equipos", [{campo:"tipo", valor: 3}]).then(data => {
      console.log(data);
      this.tractores = data;
    });

  }

  barCosechadora(){
    this.barcodeScanner.scan().then((barcodeData) => {
      //console.log('Barcode data', barcodeData);
      //this.chequero = this.trim(barcodeData.text);
      // let filtro = [{campo: "codequipo",valor:this.trim(barcodeData.text)}];
      // this.dbsvc.getCatalogoFiltrado("equipos", filtro).then(data => {
        // console.log(data);
        let data = this.cosechadoras.filter(item =>{
          return item.codequipo == this.trim(barcodeData.text)
        });
        if(data!=null) if(data.length > 0){
          this.cosechadora = data[0];
          this.storage.set("cosechadora",this.trim(barcodeData.text))
        }
      // });
    }).catch(err => {
      console.log('Error', err);
    });
  }



  barTractor1(){
    this.barcodeScanner.scan().then((barcodeData) => {
      //console.log('Barcode data', barcodeData);
      //this.chequero = this.trim(barcodeData.text);
      // let filtro = [{campo: "codequipo",valor:this.trim(barcodeData.text)}];
      // this.dbsvc.getCatalogoFiltrado("equipos", filtro).then(data => {
        // console.log(data);
        let data = this.tractores.filter(item =>{
          return item.codequipo == this.trim(barcodeData.text)
        });
        if(data!=null) if(data.length > 0){
          this.tractor1 = data[0];
          this.storage.set("tractor1",this.trim(barcodeData.text))
        }
      // });
    }).catch(err => {
      console.log('Error', err);
    });
  }

  barTractor2(){
    this.barcodeScanner.scan().then((barcodeData) => {
      //console.log('Barcode data', barcodeData);
      //this.chequero = this.trim(barcodeData.text);
      // let filtro = [{campo: "codequipo",valor:this.trim(barcodeData.text)}];
      // this.dbsvc.getCatalogoFiltrado("equipos", filtro).then(data => {
        let data = this.tractores.filter(item =>{
          return item.codequipo == this.trim(barcodeData.text)
        });
        if(data!=null) if(data.length > 0){
          this.tractor2 = data[0];
          this.storage.set("tractor2",this.trim(barcodeData.text))
        }
      // });
    }).catch(err => {
      console.log('Error', err);
    });
  }

  barTractor3(){
    this.barcodeScanner.scan().then((barcodeData) => {
      //console.log('Barcode data', barcodeData);
      //this.chequero = this.trim(barcodeData.text);
      // let filtro = [{campo: "codequipo",valor:this.trim(barcodeData.text)}];
      // this.dbsvc.getCatalogoFiltrado("equipos", filtro).then(data => {
        // console.log(data);
        let data = this.tractores.filter(item =>{
          return item.codequipo == this.trim(barcodeData.text)
        });
        if(data!=null) if(data.length > 0){
          this.tractor3 = data[0];
          this.storage.set("tractor3",this.trim(barcodeData.text))
        }
      // });
    }).catch(err => {
      console.log('Error', err);
    });
  }
  swipe(event) {
    if(event.direction === 2) {
      this.navCtrl.parent.select(2);
    }
    if(event.direction === 4) {
      this.navCtrl.parent.select(0);
    }
  }
}
