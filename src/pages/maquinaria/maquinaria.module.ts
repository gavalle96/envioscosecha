import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MaquinariaPage } from './maquinaria';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    MaquinariaPage,
  ],
  imports: [
    IonicPageModule.forChild(MaquinariaPage),
    SelectSearchableModule
  ],
})
export class MaquinariaPageModule {}
