import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import {SettingsPage} from '../settings/settings'
import {PrincipalPage} from '../principal/principal'
import { RestProvider } from '../../providers/rest/rest';
import { Storage } from '@ionic/storage';
import { AppVersion } from '@ionic-native/app-version';
import { Network } from '@ionic-native/network';
import { Diagnostic } from '@ionic-native/diagnostic';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  msgsvc:string ="";

  loading:any;
  versionnumber;
  usr="";
  pwd="";
  constructor(public navCtrl: NavController
    , private loadingCtrl: LoadingController
    , private alertCtrl: AlertController
    , private storage: Storage
    , public restsvc: RestProvider
    , private appversion: AppVersion
    , private diagnostic: Diagnostic
    , private network: Network) {
     
      this.appversion.getVersionNumber().then(valor => {
        this.versionnumber = valor;
      });

      this.diagnostic.isNFCPresent().then(val => {
        if(val != true){
          let alert = this.alertCtrl.create({
            title: '¡Hardware NFC no disponible!',
            subTitle: 'La app funciona mejor en un teléfono NFC',
            message: 'Puedes seguir utilizando la app, pero sin grabar tarjetas o llaveros NFC.',
            buttons: ['Aceptar']
          });
          alert.present();
        }
        else{
          this.diagnostic.isNFCEnabled().then(enabled =>{
            if(enabled != true){
              let alert = this.alertCtrl.create({
                title: '¡NFC desactivado!',
                subTitle: 'Activa la función NFC del teléfono y Android Beam si estuviere disponible.',
                
                buttons: ['Aceptar']
              });
              alert.present();
            }
          })
        }
      })

      this.restsvc.getImei()
      .then(data =>{
        this.storage.set("imei",data);
        console.log("Imei ", data);
      });
      
      this.checkLogin();
      

  }
  openSettings() {
      this.navCtrl.push(SettingsPage)
    }

  isConnected(): boolean {
    let conntype = this.network.type;
    return conntype && conntype !== 'unknown' && conntype !== 'none';
  }

  getTokenSesion(){
    let usuarioT = "";
    let pwdT = "";
    
      return this.storage.get("usuario")
      .then(val => {
        console.log(val)
        if(val!== null){
          usuarioT = val;
        }
        return this.storage.get("pwd");
      }).then(pwd => {
        console.log(pwd);
        if(pwd !== null){
          pwdT = pwd;
        }
        return this.restsvc.autenticar(usuarioT, pwdT);
      });
  }

  checkLogin(){
    
    let sesAct = true;
    this.storage.get("sesionactiva").then(val=>{
      console.log("sesion activa");
      console.log(val);
      sesAct = val;
      //Si hay sesión activa, se procede a entrar a la app
      if(val!== null && val == true){
        // token = val;
        
        this.navCtrl.setRoot(PrincipalPage);

        //Si hay conexión a internet, consultar token de sesión
        if(this.isConnected())
        return this.getTokenSesion();
      }
    })
    .then(data =>{
      console.log("token como valor esperado")
      console.log(data);
      //si se tiene conexión a internet, devolverá el estado de la sesión
      if(data !== null && data !== undefined){
        //La operación getTokenSesion() verifica la existencia de un token.
        //Si no hay token, inicia sesión y devuelve uno.

        //Si el proceso de autenticación es correcto, 
        // guardaremos el token para operaciones posteriores de consulta
        if(data.Key == true){
          this.storage.set("token",data.Value);
        }
        //Si la sesión ya no es válida o si ocurre un error de autenticación
        // se lanzará el mensaje y se pedirá inicio de sesión manual.
        //<<Después de todo, en esta condición, tiene conexión a internet>>
        else{
          let alert = this.alertCtrl.create({
            title: 'Error al entrar',
            subTitle: data.Value,
            buttons: ['Aceptar']
          });
          alert.present();

          //De haber error de sesión, se pedirá iniciar de nuevo
          let act = this.navCtrl.getActive();
          console.log("PAGE ACTIVO")
          console.log(act.component.name);
          //this.navCtrl.setRoot(HomePage);
        }
      }
    })
    .catch( err =>{

      //Se espera que solo la promesa de conexión al servidor devuelva un error (de conexión o resolución de direcciones)
      console.log(err);

      
    })
  }

  presentLoginError() {
    let alert = this.alertCtrl.create({
      title: 'Credenciales no válidas',
      subTitle: this.msgsvc,
      buttons: ['Aceptar']
    });
    alert.present();
  }
  iniciarSesion(){
    this.presentLoadingDefault();
    /*console.log(this.usr);
    console.log(this.pwd);*/
    if(this.isConnected()){
    return this.restsvc.autenticar(this.usr,this.pwd).then(
      response => {
      
        if(response.Key == true){
          this.storage.set("token",response.Value);
          this.storage.set('usuario',this.usr);
          this.storage.set("pwd",this.pwd);
        }
        else{
          this.msgsvc = response.Value;
        }
        return response.Key;
      }
    );
    }
    else{
      return this.storage.get('usuario').then(val =>{
        console.log("get usuario");
        console.log(val);
        if(val !== null && val !== undefined){
          if(this.usr.toLocaleLowerCase() == val.toLocaleLowerCase()){
            return this.storage.get('pwd');
          }
          else{
            this.presentLoginError();
          }
        }
      }).then(val => {
        console.log("get pwd");
        console.log(val);
        if(val !== null && val !== undefined){
          if(this.pwd.toLocaleLowerCase() == val.toLocaleLowerCase()){
            this.navCtrl.setRoot(PrincipalPage);
            this.storage.set("sesionactiva",true);
            return true;
          }
          else{
            this.presentLoginError();
            return false;
          }
        }
      })
    }
    
  }

  presentLoadingDefault() {
    
    this.loading = this.loadingCtrl.create({
      content: 'Espere por favor...'
    });
  
    this.loading.present();
  
    
  }

  autenticar(){
    this.iniciarSesion().then(res =>{
      this.loading.dismiss();
      if(res){
        this.navCtrl.setRoot(PrincipalPage);
        this.storage.set("sesionactiva",true);
      }
      else{
        this.presentLoginError();
      }
    })
      
    
  }

  

 hideShowPassword() {
     this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
     this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
 }
}
