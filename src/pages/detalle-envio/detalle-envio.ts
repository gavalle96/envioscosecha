import { Component } from '@angular/core';
import { IonicPage, NavParams, AlertController, ModalController, ViewController, LoadingController, NavController } from 'ionic-angular';
import { Subscription } from 'rxjs';
import { PrinterProvider } from '../../providers/printer/printer';
import { NFC, Ndef } from '@ionic-native/nfc';
import { Storage } from '@ionic/storage';
import { DatabaseProvider } from '../../providers/database/database';
import { MediaObject, Media } from '@ionic-native/media';
import { EdicionPage } from '../edicion/edicion';


/**
 * Generated class for the DetalleEnvioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalle-envio',
  templateUrl: 'detalle-envio.html',
})


export class DetalleEnvioPage {
  subBluetooth: Subscription = new Subscription();
  envio;
  loading;
  readingTag:   boolean   = false;
  writingTag:   boolean   = false;
  isWriting:    boolean   = false;
  ndefMsg:      string    = '';
  subscriptions: Array<Subscription> = new Array<Subscription>();
  createdCode = "";
  appCoop = "mecanizada";
  rozadores = [];
  manual = 0;

  confirmNFC: any;
  file: MediaObject;
  fileOK : MediaObject;
  playing = false;
  editable = true;

  constructor(public viewCtrl: ViewController
    , public navCtrl: NavController
    , public navParams: NavParams
    , private alertCtrl: AlertController
    , private printerProvider: PrinterProvider
    , private loadingCtrl: LoadingController
    , public nfc: NFC
    , public ndef: Ndef
    , private storage: Storage
    , private dbsvc: DatabaseProvider
    , private media: Media
    ) {

    this.formatDate();
    
    


      this.file = this.media.create('/android_asset/www/assets/audio/beep.mp3');//('assets/audio/threebeep.mp3');
    this.file.setVolume(1);
    this.file.onStatusUpdate.subscribe(status =>{
      console.log("etapa: "+status);
      if(status == this.media.MEDIA_STOPPED && this.playing){
        this.file.play();
      }

    } ); 

    this.fileOK = this.media.create('/android_asset/www/assets/audio/done.mp3');//('assets/audio/threebeep.mp3');
    this.fileOK.setVolume(1);

    this.envio = navParams.data;
    
    let limite = new Date();
    limite.setDate(limite.getDate() - 1);
    this["editable"] = this.envio.fechafin > limite.toISOString();
    
    this.printerProvider.getMacAdress();


    this.storage.get("aplicativo").then(val => {
      if(val != null && val != "" && val != undefined){
        this.appCoop = val;
      }
      this.getRozadores();
    })
    
    this.dbsvc.getCatalogoFiltrado("liberaciones", [{campo: "codliberacion",valor: this.envio.codliberacion}]).then(data => {
      if(data.length > 0){
        this.manual = data[0]["manual"];
      }
    });
    this.createdCode = this.printerProvider.getCadena2D(this.envio,1,[], this.manual);
    console.log(this.createdCode);

    //#region MANEJO DE NFC
this.subscriptions.push(this.nfc.addNdefListener()
.subscribe(data => {
  if (this.readingTag) {
    let payload = data.tag.ndefMessage[0].payload;
    let tagContent = this.nfc.bytesToString(payload).substring(3);
    console.log("Tag Content",tagContent);
    this.readingTag = false;
    if(tagContent == this.ndefMsg){
      this.confirmNFC.dismiss();
      let alert = this.alertCtrl.create({
        title: '¡Grabado Correcto!',
        subTitle: 'Envío grabado correctamente',
        enableBackdropDismiss: false,
        buttons: [ {
          text: 'Aceptar',
          handler: data => {
            
            alert.dismiss();
            this.fileOK.play();
            return false;
          }
        }]
      });
      alert.present()
    }
  } 
  else if (this.writingTag) {
    if (!this.isWriting) {
      this.isWriting = true;
      let value = this.ndef.textRecord(this.ndefMsg);
      console.log("ndefMsg: ",this.ndefMsg);
      this.nfc.write([value])
        .then(() => {
          this.writingTag = false;
          this.isWriting = false;
          console.log("written");
          this.readingTag = true;

          
          this.confirmNFC.dismiss();
         // this.restsvc.sendLog(this.envio)
         this.playing = false;
         this.file.stop();

          this.confirmNFC = this.alertCtrl.create({
            title: 'Tarjeta grabada',
            subTitle: 'ACERCAR TARJETA DE NUEVO',
            enableBackdropDismiss: false,
            buttons: [ {
              text: 'Cancelar',
              handler: data => {
                
                this.confirmNFC.dismiss();
                this.readingTag = false;
                return false;
              }
            }]
          });
          this.confirmNFC.present()

        })
        .catch(err => {
          this.writingTag = false;
          this.isWriting = false;
          this.confirmNFC.dismiss();
          console.log("Error en escritura")
          console.log(err);
          this.playing = false;
          this.file.stop();
          let alert = this.alertCtrl.create({
            title: 'Error en grabado',
            subTitle: 'Intentelo de nuevo',
            enableBackdropDismiss: false,
            buttons: [ {
              text: 'Aceptar',
              handler: data => {
                this.writeNFC();
                alert.dismiss();
                return false;
              }
            }]
          });
          alert.present()
        });
    }
  }
},
err => {

})
);
//#endregion

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetalleEnvioPage');
  }

  ionViewWillLeave() {
    console.log("sale de modal");     
    
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
    this.subscriptions = [];
  }
  

  writeNFC() {
    
    this.playing = true;
    this.ndefMsg = "";
    this.file.play();
    this.confirmNFC = this.alertCtrl.create({
      title: 'ACERQUE LA TARJETA',
      message: "Acerque la tarjeta para grabar el envío",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
            this.readingTag = false;
            this.playing = false;
            this.file.stop();
            this.confirmNFC.dismiss();

            this.confirmNFC.dismiss();
            return false;
          }
        },
        // {
        //   text: 'Aceptar',
        //   handler: data => {
        //     console.log('Aceptar clicked');
        //           //#region MANEJO DE GRABADO NFC
        //             this.ndefMsg = this.printerProvider.getCadena2D(this.envio,1,this.rozadores);
        //             this.writingTag = true;
        //             //#endregion
        //             this.confirmNFC.dismiss();
        //             return false;
            
        //   }
        // }
      ]
    });

    this.ndefMsg = this.printerProvider.getCadena2D(this.envio,1,this.rozadores, this.manual);
                    this.writingTag = true;
                    

    this.confirmNFC.present();

  }

  imprimir(){
    this.loading = this.loadingCtrl.create({
      content: 'Imprimiendo...'
    });
    this.loading.present();
    //#region MANEJO DE IMPRESORA
    this.subBluetooth = this.printerProvider.conectarImpresora().subscribe(ok =>{
      console.log("conecta impresora");
      console.log(ok);
      this.printerProvider.imprimir(this.envio, 1,this.rozadores)
      .then(data =>{
        console.log("imprime");
        console.log(data);
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          title:"Impresion finalizada",
          buttons:[{
            text: "Ok",
            handler: () =>{
              alert.dismiss();
              return false;
            }
          }]
        });
        alert.present();
        this.subBluetooth.unsubscribe();
      });
    }
    , err => {
      console.log("error al imprimir")
      console.log(err);
      this.loading.dismiss();
      this.subBluetooth.unsubscribe();
    });

    //#endregion

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  getRozadores(){
    this.dbsvc.getRozadoresEnvio(this.envio.envio)
    .then(data => {
      this.rozadores = data;
    })
  }
  editar(){
    this.navCtrl.push(EdicionPage,{envio:this.envio, rozadores: this.rozadores});
  }

 
  formatDate(){
    Date.prototype.toISOString = function() {
      var tzo = -this.getTimezoneOffset(),
          dif = tzo >= 0 ? '+' : '-',
          pad = function(num) {
              var norm = Math.floor(Math.abs(num));
              return (norm < 10 ? '0' : '') + norm;
          };
      return this.getFullYear() +
          '-' + pad(this.getMonth() + 1) +
          '-' + pad(this.getDate()) +
          'T' + pad(this.getHours()) +
          ':' + pad(this.getMinutes()) +
          ':' + pad(this.getSeconds()) +
          dif + pad(tzo / 60) +
          ':' + pad(tzo % 60);
  }
  }
}
