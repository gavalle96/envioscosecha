import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalleEnvioPage } from './detalle-envio';
import { QRCodeModule } from 'angularx-qrcode';


@NgModule({
  declarations: [
    DetalleEnvioPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalleEnvioPage),
    QRCodeModule
  ],
})
export class DetalleEnvioPageModule {}
