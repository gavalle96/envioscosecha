import { Component, Injector } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, ModalController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { PopoverconsultarPage } from '../popoverconsultar/popoverconsultar';
import { DetalleEnvioPage } from '../detalle-envio/detalle-envio';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ConsultarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-consultar',
  templateUrl: 'consultar.html',
})

export class ConsultarPage {
fechaini;
fechafin;
filtrarfecha:boolean = false;
envios:any[]=[];
copyenvios:any[]=[];
filtro;
appCoop="mecanizada";
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private dbsvc: DatabaseProvider
    , private popoverCtrl: PopoverController
    , private modalCtrl: ModalController
    , private injector: Injector
    , private storage: Storage) {
      this.dbsvc = injector.get(DatabaseProvider);
      

      
    
  }
  ionViewWillEnter(){
    this.storage.get("aplicativo").then(val => {
      if(val != null && val != "" && val != undefined){
        this.appCoop = val;
      }
      return this.initializeItems();
    })
    .then(()=>{
      this.fechaini = this.navParams.get("fechaini");
      this.fechafin = this.navParams.get("fechafin");
      console.log(this.fechaini);
      console.log(this.fechafin);
      this.getItems();
    })
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ConsultarPage');
    
  }

  presentModal(envio) {
    const modal = this.modalCtrl.create(DetalleEnvioPage, envio);
    modal.present();
  }

  initializeItems(){
    return this.dbsvc.getEnvios(this.appCoop).then(data=>{
      console.log(data);
      if(data!=null)
      data.forEach(item => {
        item["fechaEnvio"] = this.showDate(item.fechafin);
        item["horaEnvio"] = this.showTime(item.fechafin);
        item["dsenviado"] = item.enviado > 0 ? "SI" :"NO";
      });
      //this.envios = data;
      this.copyenvios = data;
      
      return Promise.resolve();
      
    })
    .catch(err=>{
      console.log(err);
    });
    // this.dbsvc.getCatalogoFiltrado("envios", [], [{campo:"envio desc"}])
  }

  openPopover(event){
    let params = {items: this.envios, filtrarfecha : this.filtrarfecha, fechaini:this.fechaini, fechafin: this.fechafin};
    let popover = this.popoverCtrl.create(PopoverconsultarPage, params, {enableBackdropDismiss: false});
    popover.onDidDismiss(data => {
      console.log(data);
        this.envios = data.items;
        this.filtrarfecha = data.filtrarfecha;
        this.fechaini = data.fechaini;
        this.fechafin = data.fechafin;
        this.getItems();
    });
    popover.present({ev: event});
}

  showDate(ISOFormat: string){
    let date = new Date(ISOFormat);
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let dt = date.getDate();

    let sdt:string = ""+dt;
    let smonth:string = ""+month;
    if (dt < 10) {
      sdt = '0' + dt;
    }
    if (month < 10) {
      smonth = '0' + month;
    }

    //console.log(sdt+'/' + smonth + '/'+year);
    return sdt+'/' + smonth + '/'+year;
  }

  showTime(ISOFormat: string){
    let date = new Date(ISOFormat);
    let hh = date.getHours();
    let mm = date.getMinutes();
    let shh =""+hh;
    let smm = ""+mm;
    if (hh < 10) {
      shh = '0' + hh;
    }
    if (mm < 10) {
      smm = '0' + mm;
    }

    //console.log(hh+":"+mm);
    return shh+":"+smm;
  }

  getItems() {
    // Reset items back to all of the items
    //this.initializeItems();
    // this.envios = this.copyenvios;

    // set val to the value of the searchbar
    let val = this.filtro;
    
    if(val == null) 
    val = '';

    // if the value is an empty string don't filter the items
    //if (val && val.trim() != '') {
      this.envios = this.copyenvios.filter((item) => {
        return (item.fechafin >= this.fechaini) && (item.fechafin <= this.fechafin)
        ;
      })
      console.log(this.copyenvios);
      
    //}
  }

  tapEnvio(ev,item){
    console.log(item);
  }
}
