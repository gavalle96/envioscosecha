import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConsultarPage } from '../consultar/consultar';
import { DatePicker } from '@ionic-native/date-picker';

/**
 * Generated class for the FiltroEnviosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filtro-envios',
  templateUrl: 'filtro-envios.html',
})
export class FiltroEnviosPage {

  fechaInicio;
  fechaFin;

  dtini;
  dtfin;
  constructor(public navCtrl: NavController, public navParams: NavParams
    , private datePicker: DatePicker) {
      this.dtini = new Date();
      this.dtfin = new Date();
      this.fechaInicio = this.showDate(this.dtini);
      this.fechaFin = this.showDate(this.dtfin);
      this.formatDate();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FiltroEnviosPage');
  }

  filtrar(){
    this.navCtrl.push(ConsultarPage, {fechaini: this.dtini.toISOString(), fechafin: this.dtfin.toISOString()});
  }

  showPicker(control,dt){
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      date =>{ console.log('Got date: ', date);
      this[control] = this.showDate(date);
      this[dt] = date;
      
    },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  showDate(date){
    //let date = new Date(ISOFormat);
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let dt = date.getDate();

    let sdt:string = ""+dt;
    let smonth:string = ""+month;
    if (dt < 10) {
      sdt = '0' + dt;
    }
    if (month < 10) {
      smonth = '0' + month;
    }

    let hh = date.getHours();
    let mm = date.getMinutes();
    let shh =""+hh;
    let smm = ""+mm;

    if (hh < 10) {
      shh = '0' + hh;
    }
    
    if (mm < 10) {
      smm = '0' + mm;
    }

    //console.log(sdt+'/' + smonth + '/'+year);
    return sdt+'/' + smonth + '/'+year + " "+ shh+":"+smm;
  }
  formatDate(){
    Date.prototype.toISOString = function() {
      var tzo = -this.getTimezoneOffset(),
          dif = tzo >= 0 ? '+' : '-',
          pad = function(num) {
              var norm = Math.floor(Math.abs(num));
              return (norm < 10 ? '0' : '') + norm;
          };
      return this.getFullYear() +
          '-' + pad(this.getMonth() + 1) +
          '-' + pad(this.getDate()) +
          'T' + pad(this.getHours()) +
          ':' + pad(this.getMinutes()) +
          ':' + pad(this.getSeconds()) +
          dif + pad(tzo / 60) +
          ':' + pad(tzo % 60);
  }
  }
}
