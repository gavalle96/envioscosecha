import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FiltroEnviosPage } from './filtro-envios';

@NgModule({
  declarations: [
    FiltroEnviosPage,
  ],
  imports: [
    IonicPageModule.forChild(FiltroEnviosPage),
  ],
})
export class FiltroEnviosPageModule {}
