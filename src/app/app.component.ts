import { Component } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { Storage } from '@ionic/storage';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';
import { DatabaseProvider } from '../providers/database/database';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;
  selectedTheme: string = 'verde';
  constructor(platform: Platform, statusBar: StatusBar
    , splashScreen: SplashScreen, private storage: Storage
    , private sqLite: SQLite, private bdService :DatabaseProvider
    , private event: Events
    ) {
      console.log("constructor de la app");
    platform.ready().then(() => {
    console.log("platform lista");
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.recuperarConfig();
      storage.get('ambiente').then((val) => {
        console.log("ambiente");
        console.log(val);
        if (val !== null) {
           //Si no es primera vez que inicia
        } else {
           //Si es primera vez
          this.inicializarParametros();
        
          } 
          return Promise.resolve();   
        }).then(()=>
        {
          return this.createDatabase();
        }
        ).then(()=>
        {
          statusBar.styleDefault();
          splashScreen.hide();
        });
      
        


      
      
    });

    this.event.subscribe("finajustes",()=>{
      this.recuperarConfig();
    });
    
  }
recuperarConfig(){
  this.storage.get("aplicativo").then(val =>{
    if(val != null && val != "" && val != undefined){
      let aplicativo = val;
      switch (aplicativo) {
        case "manual":
          this.selectedTheme = "verde"
          break;
          case "coop":
          this.selectedTheme ="cafe";
          break;
          case "picada":
          this.selectedTheme ="amarillo";
          break;
      
        default:
        this.selectedTheme = "azul";
          break;
      }
    }
  });
}
  inicializarParametros(){
    //iniciamos el storage de configuración general
    this.storage.set('ambiente','prdpub');
    //BASE DE DATOS POR DEFECTO
    this.storage.set('db','dbqas'); //VALORES: dbprd, dbqas

    //Servidor establecido
    this.storage.set('server','https://168.243.156.109:8001');

    //direcciones de servers según ambiente y tipo de conección

    //CALIDAD LAN
    this.storage.set('serverqaslan','https://webapp-devqas.grupocassa.com:8001');

    //PRODUCTIVO LAN
    this.storage.set('serverprdlan','https://192.168.23.76:8001');

    //CALIDAD PUBLICO
    this.storage.set('serverqaspub','https://168.243.156.109:8001');

    //PRODUCTIVO PUBLICO
    this.storage.set('serverprdpub','https://168.243.156.106:8001');

    //VARIABLE GLOBAL PARA APLICATIVO
    this.storage.set("aplicativo","mecanizada");

    this.storage.set('tabActiva',0);

    this.storage.set('envioMin',1);
    this.storage.set('envioMax',100);
    this.storage.set('ultEnvio',0);
    this.storage.set('checkpoligono',true);
    
  }
  public createDatabase() {
    let dbname = "";
    return this.storage.get('db').then((val) => {
      console.log(val);
      dbname = val;

      let promise = this.sqLite.create({
        name: dbname+'.db',//'dbcatalogos.db',
        location: 'default'
      });
      promise.then((db: SQLiteObject) => {
          this.bdService.setDatabase(db);
          this.bdService.createTables();
          this.bdService.onStart();
          console.log("Database creada");
      }).catch(e => console.log("Error : " + e));
      
    });
    

    
    
}

}