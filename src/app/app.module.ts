import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AppVersion } from '@ionic-native/app-version';
import { NFC, Ndef } from '@ionic-native/nfc';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { DatePicker } from '@ionic-native/date-picker';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Network } from '@ionic-native/network';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SettingsPage } from '../pages/settings/settings';
import { PrincipalPage } from '../pages/principal/principal';
import { EnviosPage } from '../pages/envios/envios';
import { SQLite } from '@ionic-native/sqlite';
import { IonicStorageModule } from '@ionic/storage';
import { CrearPage } from '../pages/crear/crear';
import { TripulacionPage } from '../pages/tripulacion/tripulacion';
import { MaquinariaPage } from '../pages/maquinaria/maquinaria';

import { QRCodeModule } from 'angularx-qrcode';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { LiberacionPage } from '../pages/liberacion/liberacion';
// import { PruebaProvider } from '../providers/prueba/prueba';
import { HttpClient,HttpClientModule } from '@angular/common/http';
import { HTTP } from '@ionic-native/http';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { DatabaseProvider } from '../providers/database/database';
import { RestProvider } from '../providers/rest/rest';
import { SincronizarPage } from '../pages/sincronizar/sincronizar';
import { IonfcPage } from '../pages/ionfc/ionfc';
import { ConsultarPage } from '../pages/consultar/consultar';
import { PopoverconsultarPage } from '../pages/popoverconsultar/popoverconsultar';
import { DetalleEnvioPage } from '../pages/detalle-envio/detalle-envio';
import { Uid } from '@ionic-native/uid';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { PrinterProvider } from '../providers/printer/printer';
import { CorreccionPage } from '../pages/correccion/correccion';
import { RozadoresPage } from '../pages/rozadores/rozadores';
import { CambioAppPage } from '../pages/cambio-app/cambio-app';
import { Vibration } from '@ionic-native/vibration';
import { Dialogs } from '@ionic-native/dialogs';
import { Media } from '@ionic-native/media';
import { FiltroEnviosPage } from '../pages/filtro-envios/filtro-envios';
import { EdicionPage } from '../pages/edicion/edicion';
import { CambioAppPageModule } from '../pages/cambio-app/cambio-app.module';
import { ConsultarPageModule } from '../pages/consultar/consultar.module';
import { PrincipalPageModule } from '../pages/principal/principal.module';
import { SettingsPageModule } from '../pages/settings/settings.module';
import { EnviosPageModule } from '../pages/envios/envios.module';
import { CrearPageModule } from '../pages/crear/crear.module';
import { TripulacionPageModule } from '../pages/tripulacion/tripulacion.module';
import { MaquinariaPageModule } from '../pages/maquinaria/maquinaria.module';
import { LiberacionPageModule } from '../pages/liberacion/liberacion.module';
import { SincronizarPageModule } from '../pages/sincronizar/sincronizar.module';
import { IonfcPageModule } from '../pages/ionfc/ionfc.module';
import { PopoverconsultarPageModule } from '../pages/popoverconsultar/popoverconsultar.module';
import { DetalleEnvioPageModule } from '../pages/detalle-envio/detalle-envio.module';
import { CorreccionPageModule } from '../pages/correccion/correccion.module';
import { RozadoresPageModule } from '../pages/rozadores/rozadores.module';
import { FiltroEnviosPageModule } from '../pages/filtro-envios/filtro-envios.module';
import { EdicionPageModule } from '../pages/edicion/edicion.module';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    // SettingsPage,
    // PrincipalPage,
    // EnviosPage,
    // CrearPage,
    // TripulacionPage,
    // MaquinariaPage,
    // LiberacionPage,
    // SincronizarPage,
    // IonfcPage,
    // // ConsultarPage,
    // PopoverconsultarPage,
    // DetalleEnvioPage,
    // CorreccionPage,
    // RozadoresPage,
    // // CambioAppPage,
    // FiltroEnviosPage,
    // EdicionPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{
      monthNames: ['Enero', 'Febreero', 'Marzo','Abril', 'Mayo', 'Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
      monthShortNames: ['Ene', 'Feb', 'Mar','Abr','May', 'Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
      dayNames: ['domingo', 'lunes','martes','miercoles','jueves','viernes','sabado' ],
      dayShortNames: ['dom', 'lun', 'mar', 'mie','jue','vie','sab'],
      
    }),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    SelectSearchableModule,
    QRCodeModule,
    CambioAppPageModule,
    ConsultarPageModule,

    
    SettingsPageModule,
    PrincipalPageModule,
    EnviosPageModule,
    CrearPageModule,
    TripulacionPageModule,
    MaquinariaPageModule,
    LiberacionPageModule,
    SincronizarPageModule,
    IonfcPageModule,
    // ConsultarPage,
    PopoverconsultarPageModule,
    DetalleEnvioPageModule,
    CorreccionPageModule,
    RozadoresPageModule,
    // CambioAppPage,
    FiltroEnviosPageModule,
    EdicionPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SettingsPage,
    PrincipalPage,
    EnviosPage,
    CrearPage,
    TripulacionPage,
    MaquinariaPage,
    LiberacionPage,
    SincronizarPage,
    IonfcPage,
    ConsultarPage,
    PopoverconsultarPage,
    DetalleEnvioPage,
    CorreccionPage,
    RozadoresPage,
    CambioAppPage,
    FiltroEnviosPage,
    EdicionPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    BarcodeScanner,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    // PruebaProvider,
    HttpClient,
    // Storage,
    HTTP,
    NativePageTransitions,
    DatabaseProvider,
    RestProvider,
    AppVersion,
    NFC,
    Ndef,
    Geolocation,
    Diagnostic,
    LocationAccuracy,
    DatePicker,
    BackgroundMode,
    Network,
    AndroidPermissions,
    Uid,
    BluetoothSerial,
    PrinterProvider,
    Vibration,
    Dialogs,
    Media
  ]
})
export class AppModule {}
